/**
 * LZ4F is a stand-alone API to create LZ4-compressed Frames
 *  in full conformance with specification v1.6.1 .
 *  This library rely upon memory management capabilities.
 *
 * License: BSD 2-Clause License
 */
/*
LZ4 auto-framing library
Copyright (C) 2011-2016, Yann Collet.

BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the
distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You can contact the author at :
- LZ4 homepage : http://www.lz4.org
- LZ4 source repository : https://github.com/lz4/lz4
*/
module lz4_lib.lz4frame;


/*-************************************
*  Dependency
**************************************/
private static import core.stdc.stdlib;
private static import core.stdc.string;
private static import lz4_lib.lz4common;
private static import lz4_lib.lz4;
private static import lz4_lib.lz4hc;
private static import lz4_lib.xxhash;

/*-************************************
*  Memory routines
**************************************/

/*-************************************
*  Debug
**************************************/

/*-************************************
*  Tuning parameters
**************************************/
/*
 * LZ4F_HEAPMODE :
 * Select how default compression functions will allocate memory for their hash table,
 * in memory stack (false:default, fastest), or in memory heap (true:requires malloc()).
 */
static if (!__traits(compiles, .LZ4F_HEAPMODE)) {
	enum bool LZ4F_HEAPMODE = false;
}

/*-************************************
 *  Frame compression types
 **************************************/
/*
 * The larger the block size, the (slightly) better the compression ratio,
 * though there are diminishing returns.
 * Larger blocks also increase memory usage on both compression and decompression sides.
 */
enum LZ4F_blockSizeID_t
{
	default_size = 0,
	max64KB = 4,
	max256KB = 5,
	max1MB = 6,
	max4MB = 7,
}

/*
 * Linked blocks sharply reduce inefficiencies when using small blocks,
 * they compress better.
 * However, some LZ4 decoders are only compatible with independent blocks
 */
enum LZ4F_blockMode_t
{
	blockLinked = 0,
	blockIndependent,
}

enum LZ4F_contentChecksum_t
{
	noContentChecksum = 0,
	contentChecksumEnabled,
}

enum LZ4F_blockChecksum_t
{
	noBlockChecksum = 0,
	blockChecksumEnabled,
}

enum LZ4F_frameType_t
{
	frame = 0,
	skippableFrame,
}

static if (__traits(compiles, LZ4F_ENABLE_OBSOLETE_ENUMS)) {
	alias blockSizeID_t = .LZ4F_blockSizeID_t;
	alias blockMode_t = .LZ4F_blockMode_t;
	alias frameType_t = .LZ4F_frameType_t;
	alias contentChecksum_t = .LZ4F_contentChecksum_t;
}

/*-****************************
*  Definitions
******************************/
/**
 * This number can be used to check for an incompatible API breaking change
 */
enum LZ4F_VERSION = 100;

/**
 * LZ4 Frame header size can vary from 7 to 19 bytes
 */
enum LZ4F_HEADER_SIZE_MAX = 19;

enum dStage_t
{
	getFrameHeader = 0,
	storeFrameHeader,
	dstage_init,
	getBlockHeader,
	storeBlockHeader,
	copyDirect,
	getBlockChecksum,
	getCBlock,
	storeCBlock,
	flushOut,
	getSuffix,
	storeSuffix,
	getSFrameSize,
	storeSFrameSize,
	skipSkippable
}

/**
 * enum list is exposed, to handle specific errors
 */
/* These declarations are not stable and may change in the future.
 * They are therefore only safe to depend on
 * when the caller is statically linked against the library.
 * To access their declarations, define LZ4F_STATIC_LINKING_ONLY.
 *
 * By default, these symbols aren't published into shared/dynamic libraries.
 * You can override this behavior and force them to be published
 * by defining LZ4F_PUBLISH_STATIC_FUNCTIONS.
 * Use at your own risk.
 */
public enum LZ4F_errorCodes
{
	NoError = 0,
	GENERIC,
	maxBlockSize_invalid,
	blockMode_invalid,
	contentChecksumFlag_invalid,
	compressionLevel_invalid,
	headerVersion_wrong,
	blockChecksum_invalid,
	reservedFlag_set,
	allocation_failed,
	srcSize_tooLarge,
	dstMaxSize_tooSmall,
	frameHeader_incomplete,
	frameType_unknown,
	frameSize_wrong,
	srcPtr_wrong,
	decompressionFailed,
	headerChecksum_invalid,
	contentChecksum_invalid,
	frameDecoding_alreadyStarted,
	maxCode,
	_LZ4F_dummy_error_enum_for_c89_never_used,
}

/*-************************************
 *  structures
 **************************************/
/**
 * LZ4F_frameInfo_t :
 *  makes it possible to set or read frame parameters.
 *  Structure must be first init to 0, using memset() or LZ4F_INIT_FRAMEINFO,
 *  setting all parameters to default.
 *  It's then possible to update selectively some parameters
 */
struct LZ4F_frameInfo_t
{
	/**
	 * max64KB, max256KB, max1MB, max4MB; 0 == default
	 */
	.LZ4F_blockSizeID_t blockSizeID;

	/**
	 * LZ4F_blockMode_t.blockLinked, LZ4F_blockMode_t.blockIndependent; 0 == default
	 */
	.LZ4F_blockMode_t blockMode;

	/**
	 * 1: frame terminated with 32-bit checksum of decompressed data; 0: disabled (default)
	 */
	.LZ4F_contentChecksum_t contentChecksumFlag;

	/**
	 * read-only field : LZ4F_frameType_t.frame or LZ4F_frameType_t.skippableFrame
	 */
	.LZ4F_frameType_t frameType;

	/**
	 * Size of uncompressed content ; 0 == unknown
	 */
	ulong contentSize;

	/**
	 * Dictionary ID, sent by compressor to help decoder select correct dictionary; 0 == no dictID provided
	 */
	uint dictID;

	/**
	 * 1: each block followed by a checksum of block's compressed data; 0: disabled (default)
	 */
	.LZ4F_blockChecksum_t blockChecksumFlag;

	version (none) {
		/* v1.8.3+ */
		pragma(inline, true)
		pure nothrow @safe @nogc
		public void INIT_FRAMEINFO()

			do
			{
				this.blockSizeID = .LZ4F_blockSizeID_t.default_size;
				this.blockMode = .LZ4F_blockMode_t.blockLinked;
				this.contentChecksumFlag = .LZ4F_contentChecksum_t.noContentChecksum;
				this.frameType = .LZ4F_frameType_t.frame;
				this.contentSize = 0;
				this.dictID = 0;
				this.blockChecksumFlag = .LZ4F_blockChecksum_t.noBlockChecksum;
			}
	} else {
		pragma(inline, true)
		pure nothrow @safe @nogc
		public void INIT_FRAMEINFO()

			do
			{
				this = this.init;
			}
	}
}

/**
 * LZ4F_preferences_t :
 *  makes it possible to supply advanced compression instructions to streaming interface.
 *  Structure must be first init to 0, using memset() or LZ4F_INIT_PREFERENCES,
 *  setting all parameters to default.
 *  All reserved fields must be set to zero.
 */
struct LZ4F_preferences_t
{
	.LZ4F_frameInfo_t frameInfo;

	/**
	 * 0: default (fast mode); values > LZ4HC_CLEVEL_MAX count as LZ4HC_CLEVEL_MAX; values < 0 trigger "fast acceleration"
	 */
	int compressionLevel;

	/**
	 * 1: always flush; reduces usage of internal buffers
	 */
	uint autoFlush;

	/**
	 * 1: parser favors decompression speed vs compression ratio. Only works for high compression modes (>= LZ4HC_CLEVEL_OPT_MIN) */  /* v1.8.2+
	 */
	uint favorDecSpeed;

	/**
	 * must be zero for forward compatibility
	 */
	uint[3] reserved;

	invariant
	{
		for (size_t i = 0; i < reserved.length; i++) {
			assert(reserved[i] == 0);
		}
	}

	/* v1.8.3+ */
	pragma(inline, true)
	@disable pure nothrow @safe @nogc
	public void INIT_PREFERENCES()
	{
		this.frameInfo.INIT_FRAMEINFO();
		this.compressionLevel = 0;
		this.autoFlush = 0;
		this.favorDecSpeed = 0;
		this.reserved[] = 0;
	}
}

struct LZ4F_CDict
{
	static import lz4_lib.lz4;
	static import lz4_lib.lz4hc;

	void* dictContent;
	lz4_lib.lz4.LZ4_stream_t fastCtx;
	lz4_lib.lz4hc.LZ4_streamHC_t HCCtx;

	/**
	 * LZ4_createCDict() :
	 *  When compressing multiple messages / blocks with the same dictionary, it's recommended to load it just once.
	 *  LZ4_createCDict() will create a digested dictionary, ready to start future compression operations without startup delay.
	 *  LZ4_CDict can be created once and shared by multiple threads concurrently, since its usage is read-only.
	 * `dictBuffer` can be released after LZ4_CDict creation, since its content is copied within CDict
	 */

	/**
	 * LZ4F_createCDict() :
	 *  When compressing multiple messages / blocks with the same dictionary, it's recommended to load it just once.
	 *  LZ4F_createCDict() will create a digested dictionary, ready to start future compression operations without startup delay.
	 *  LZ4F_CDict can be created once and shared by multiple threads concurrently, since its usage is read-only.
	 * `dictBuffer` can be released after LZ4F_CDict creation, since its content is copied within CDict
	 *
	 * Returns: digested dictionary for compression, or null if failed
	 */
	//LZ4FLIB_STATIC_API
	nothrow @nogc @system
	public this(const void* dictBuffer, size_t dictSize)

		in
		{
			assert(dictBuffer != null);
		}

		do
		{
			static import core.stdc.stdlib;
			static import core.stdc.string;
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;
			static import lz4_lib.lz4hc;

			const (char)* dictStart = cast(const (char)*)(dictBuffer);

			debug(4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `LZ4F_createCDict`);
			}

			if (dictSize > lz4_lib.lz4common.KB!(size_t, 64)) {
				dictStart += dictSize - lz4_lib.lz4common.KB!(size_t, 64);
				dictSize = lz4_lib.lz4common.KB!(size_t, 64);
			}

			this.dictContent = core.stdc.stdlib.malloc(dictSize);
			//this.fastCtx;
			//this.HCCtx = lz4_lib.lz4hc.LZ4_createStreamHC();

			if (!this.dictContent) {
				this.freeCDict();

				return;
			}

			core.stdc.string.memcpy(this.dictContent, dictStart, dictSize);
			this.fastCtx.loadDict(cast(const (char)*)this.dictContent, cast(int)(dictSize));
			this.HCCtx.setCompressionLevel(lz4_lib.lz4hc.LZ4HC_CLEVEL_DEFAULT);
			this.HCCtx.loadDictHC(cast(const (char)*)this.dictContent, cast(int)(dictSize));
		}

	//LZ4FLIB_STATIC_API
	nothrow @nogc @system
	public void freeCDict()

		in
		{
		}

		do
		{
			static import core.stdc.stdlib;

			core.stdc.stdlib.free(this.dictContent);
			this.dictContent = null;
			//this.fastCtx.freeStream();
			//this.HCCtx.freeStreamHC();
		}
}

struct LZ4F_cctx_t
{
	.LZ4F_preferences_t prefs;
	uint version_Number;
	uint cStage;
	const (.LZ4F_CDict)* cdict;
	size_t maxBlockSize;
	size_t maxBufferSize;
	ubyte* tmpBuff;
	ubyte* tmpIn;
	size_t tmpInSize;
	ulong totalInSize;
	lz4_lib.xxhash.XXH32_state_t xxh;
	void* lz4CtxPtr;

	/**
	 * sized for: 0 = none, 1 = lz4 ctx, 2 = lz4hc ctx
	 */
	ushort lz4CtxAlloc;

	/**
	 * in use as: 0 = none, 1 = lz4 ctx, 2 = lz4hc ctx
	 */
	ushort lz4CtxState;

	/**
	 * LZ4_compressFrame_usingCDict() :
	 *  Compress an entire srcBuffer into a valid LZ4 frame using a digested Dictionary.
	 *  cctx must point to a context created by LZ4F_createCompressionContext().
	 *  If cdict==null, compress without a dictionary.
	 *  dstBuffer MUST be >= LZ4F_compressFrameBound(srcSize, preferencesPtr).
	 *  If this condition is not respected, function will fail (@return an errorCode).
	 *  The LZ4F_preferences_t structure is optional : you may provide null as argument,
	 *  but it's not recommended, as it's the only way to provide dictID in the frame header.
	 *
	 * Returns: number of bytes written into dstBuffer. or an error code if it fails (can be tested using LZ4F_isError())
	 */
	/**
	 * compressFrame_usingCDict() :
	 *  Compress srcBuffer using a dictionary, in a single step.
	 *  cdict can be null, in which case, no dictionary is used.
	 *  dstBuffer MUST be >= LZ4F_compressFrameBound(srcSize, preferencesPtr).
	 *  The LZ4F_preferences_t structure is optional : you may provide null as argument,
	 *  however, it's the only way to provide a dictID, so it's not recommended.
	 *
	 * Returns: number of bytes written into dstBuffer, or an error code if it fails (can be tested using LZ4F_isError())
	 */
	//LZ4FLIB_STATIC_API
	@system
	public size_t compressFrame_usingCDict(void* dstBuffer, const size_t dstCapacity, const void* srcBuffer, const size_t srcSize, const .LZ4F_CDict* cdict, const .LZ4F_preferences_t* preferencesPtr)

		in
		{
			assert(dstBuffer != null);
		}

		do
		{
			.LZ4F_preferences_t prefs;
			.LZ4F_compressOptions_t options;
			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);
			const ubyte* dstEnd = dstStart + dstCapacity;

			if (preferencesPtr != null) {
				prefs = *preferencesPtr;
			} else {
				prefs = .LZ4F_preferences_t.init;
			}

			if (prefs.frameInfo.contentSize != 0) {
				/* auto-correct content size if selected (!=0) */
				prefs.frameInfo.contentSize = cast(ulong)(srcSize);
			}

			prefs.frameInfo.blockSizeID = .LZ4F_optimalBSID(prefs.frameInfo.blockSizeID, srcSize);
			prefs.autoFlush = 1;

			if (srcSize <= .LZ4F_getBlockSize(prefs.frameInfo.blockSizeID)) {
				/* only one block => no need for inter-block link */
				prefs.frameInfo.blockMode = .LZ4F_blockMode_t.blockIndependent;
			}

			//options = .LZ4F_compressOptions_t.init;
			options.stableSrc = 1;

			if (dstCapacity < .LZ4F_compressFrameBound(srcSize, &prefs)) { /* condition to guarantee success */
				return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
			}

			{
				/* write header */
				const size_t headerSize = this.compressBegin_usingCDict(dstBuffer, dstCapacity, cdict, &prefs);

				if (.LZ4F_isError(headerSize)) {
					return headerSize;
				}

				/* header size */
				dstPtr += headerSize;
			}

			{
				const size_t cSize = this.compressUpdate(dstPtr, dstEnd - dstPtr, srcBuffer, srcSize, &options);

				if (.LZ4F_isError(cSize)) {
					return cSize;
				}

				dstPtr += cSize;
			}

			{
				/* flush last block, and generate suffix */
				const size_t tailSize = this.compressEnd(dstPtr, dstEnd - dstPtr, &options);

				if (.LZ4F_isError(tailSize)) {
					return tailSize;
				}

				dstPtr += tailSize;
			}

			return (dstPtr - dstStart);
		}

	/**
	 * compressBegin_usingCDict() :
	 *  Inits streaming dictionary compression, and writes the frame header into dstBuffer.
	 *  dstCapacity must be >= LZ4F_HEADER_SIZE_MAX bytes.
	 * `prefsPtr` is optional : you may provide null as argument,
	 *  however, it's the only way to provide dictID in the frame header.
	 *
	 * Returns: number of bytes written into dstBuffer for the header, or an error code (which can be tested using LZ4F_isError())
	 */
	/**
	 * compressBegin_usingCDict() :
	 *  init streaming compression and writes frame header into dstBuffer.
	 *  dstBuffer must be >= LZ4F_HEADER_SIZE_MAX bytes.
	 *
	 * Returns: number of bytes written into dstBuffer for the header or an error code (can be tested using LZ4F_isError())
	 */
	//LZ4FLIB_STATIC_API
	nothrow @nogc @system
	public size_t compressBegin_usingCDict(void* dstBuffer, const size_t dstCapacity, const .LZ4F_CDict* cdict, const (.LZ4F_preferences_t)* preferencesPtr)

		in
		{
			assert(dstBuffer != null);
			assert(cdict != null);
		}

		do
		{
			static import core.stdc.stdlib;
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;
			static import lz4_lib.lz4hc;
			static import lz4_lib.xxhash;

			.LZ4F_preferences_t prefNull;
			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);
			ubyte* headerStart;

			if (dstCapacity < maxFHSize) {
				return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
			}

			//prefNull = .LZ4F_preferences_t.init;

			if (preferencesPtr == null) {
				preferencesPtr = &prefNull;
			}

			this.prefs = *preferencesPtr;

			/* Ctx Management */
			{
				const ushort ctxTypeID = (this.prefs.compressionLevel < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) ? (1) : (2);

				if (this.lz4CtxAlloc < ctxTypeID) {
					core.stdc.stdlib.free(this.lz4CtxPtr);

					if (this.prefs.compressionLevel < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
						this.lz4CtxPtr = cast(void*)(core.stdc.stdlib.calloc(1, lz4_lib.lz4.LZ4_stream_t.sizeof));
					} else {
						this.lz4CtxPtr = cast(void*)(core.stdc.stdlib.calloc(1, lz4_lib.lz4hc.LZ4_streamHC_t.sizeof));
					}

					if (this.lz4CtxPtr == null) {
						return .err0r(.LZ4F_errorCodes.allocation_failed);
					}

					this.lz4CtxAlloc = ctxTypeID;
					this.lz4CtxState = ctxTypeID;
				} else if (this.lz4CtxState != ctxTypeID) {
					/*
					 * otherwise, a sufficient buffer is allocated, but we need to
					 * reset it to the correct context type
					 */
					if (this.prefs.compressionLevel < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
						(*(cast(lz4_lib.lz4.LZ4_stream_t*)(this.lz4CtxPtr))).resetStream();
					} else {
						(*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(this.lz4CtxPtr))).resetStreamHC(this.prefs.compressionLevel);
					}

					this.lz4CtxState = ctxTypeID;
				}
			}

			/* Buffer Management */
			if (this.prefs.frameInfo.blockSizeID == 0) {
				this.prefs.frameInfo.blockSizeID = .LZ4F_BLOCKSIZEID_DEFAULT;
			}

			this.maxBlockSize = .LZ4F_getBlockSize(this.prefs.frameInfo.blockSizeID);

			{
				const size_t requiredBuffSize = ((*preferencesPtr).autoFlush) ? ((this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) * lz4_lib.lz4common.KB!(size_t, 64)) : (this.maxBlockSize + ((this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) * lz4_lib.lz4common.KB!(size_t, 128)));

				if (this.maxBufferSize < requiredBuffSize) {
					this.maxBufferSize = 0;
					core.stdc.stdlib.free(this.tmpBuff);
					this.tmpBuff = cast(ubyte*)(core.stdc.stdlib.calloc(1, requiredBuffSize));

					if (this.tmpBuff == null) {
						return .err0r(.LZ4F_errorCodes.allocation_failed);
					}

					this.maxBufferSize = requiredBuffSize;
				}
			}

			this.tmpIn = this.tmpBuff;
			this.tmpInSize = 0;
			this.xxh.reset(0);

			/* context init */
			this.cdict = cdict;

			if (this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
				/* frame init only for blockLinked : blockIndependent will be init at each block */
				.LZ4F_initStream(this.lz4CtxPtr, cdict, this.prefs.compressionLevel, .LZ4F_blockMode_t.blockLinked);
			}

			if ((*preferencesPtr).compressionLevel >= lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
				(*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(this.lz4CtxPtr))).favorDecompressionSpeed(cast(int)((*preferencesPtr).favorDecSpeed));
			}

			/* Magic Number */
			.writeLE32(dstPtr, .LZ4F_MAGICNUMBER);
			dstPtr += 4;
			headerStart = dstPtr;

			/* FLG Byte */
			*dstPtr++ = cast(ubyte)(((1 & ._2BITS) << 6) + ((this.prefs.frameInfo.blockMode & ._1BIT) << 5) + ((this.prefs.frameInfo.blockChecksumFlag & ._1BIT) << 4) + ((this.prefs.frameInfo.contentSize > 0) << 3) + ((this.prefs.frameInfo.contentChecksumFlag & ._1BIT) << 2) + (this.prefs.frameInfo.dictID > 0));
			/* BD Byte */
			*dstPtr++ = cast(ubyte)((this.prefs.frameInfo.blockSizeID & ._3BITS) << 4);

			/* Optional Frame content size field */
			if (this.prefs.frameInfo.contentSize) {
				.writeLE64(dstPtr, this.prefs.frameInfo.contentSize);
				dstPtr += 8;
				this.totalInSize = 0;
			}

			/* Optional dictionary ID field */
			if (this.prefs.frameInfo.dictID) {
				.writeLE32(dstPtr, this.prefs.frameInfo.dictID);
				dstPtr += 4;
			}

			/* Header CRC Byte */
			*dstPtr = .LZ4F_headerChecksum(headerStart, dstPtr - headerStart);
			dstPtr++;

			/* header written, now request input data block */
			this.cStage = 1;

			return (dstPtr - dstStart);
		}

	/**
	 * compressBegin() :
	 *  will write the frame header into dstBuffer.
	 *  dstBuffer must be >= LZ4F_HEADER_SIZE_MAX bytes.
	 * `prefsPtr` is optional : you can provide null as argument, all preferences will then be set to default.
	 *
	 * Returns: number of bytes written into dstBuffer for the header or an error code (which can be tested using LZ4F_isError())
	 */
	//LZ4FLIB_API
	nothrow @nogc
	public size_t compressBegin(void* dstBuffer, const size_t dstCapacity, const .LZ4F_preferences_t* preferencesPtr)

		in
		{
		}

		do
		{
			return this.compressBegin_usingCDict(dstBuffer, dstCapacity, null, preferencesPtr);
		}

	pure nothrow @nogc
	package int localSaveDict()

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;
			static import lz4_lib.lz4hc;

			if (this.prefs.compressionLevel < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
				return (*(cast(lz4_lib.lz4.LZ4_stream_t*)(this.lz4CtxPtr))).saveDict(cast(char*)(this.tmpBuff), lz4_lib.lz4common.KB!(int, 64));
			}

			return (*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(this.lz4CtxPtr))).saveDictHC(cast(char*)(this.tmpBuff), lz4_lib.lz4common.KB!(int, 64));
		}

	/**
	 * compressUpdate() :
	 *  compressUpdate() can be called repetitively to compress as much data as necessary.
	 *  Important rule: dstCapacity MUST be large enough to ensure operation success even in worst case situations.
	 *  This value is provided by LZ4F_compressBound().
	 *  If this condition is not respected, LZ4F_compress() will fail (result is an errorCode).
	 *  compressUpdate() doesn't guarantee error recovery.
	 *  When an error occurs, compression context must be freed or resized.
	 * `cOptPtr` is optional : null can be provided, in which case all options are set to default.
	 *
	 * Returns: number of bytes written into `dstBuffer` (it can be zero, meaning input data was just buffered). or an error code if it fails (which can be tested using LZ4F_isError())
	 */
	/**
	 * compressUpdate() :
	 *  compressUpdate() can be called repetitively to compress as much data as necessary.
	 *  dstBuffer MUST be >= LZ4F_compressBound(srcSize, preferencesPtr).
	 *  LZ4F_compressOptions_t structure is optional : you can provide null as argument.
	 *
	 * Returns: the number of bytes written into dstBuffer. It can be zero, meaning input data was just buffered. or an error code if it fails (which can be tested using LZ4F_isError())
	 */
	//LZ4FLIB_API
	@system
	public size_t compressUpdate(void* dstBuffer, const size_t dstCapacity, const void* srcBuffer, const size_t srcSize, const (.LZ4F_compressOptions_t)* compressOptionsPtr)

		in
		{
			assert(dstBuffer != null);
			assert(srcBuffer != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;
			static import lz4_lib.xxhash;

			.LZ4F_compressOptions_t cOptionsNull;
			const size_t blockSize = this.maxBlockSize;
			const (ubyte)* srcPtr = cast(const (ubyte)*)(srcBuffer);
			const ubyte* srcEnd = srcPtr + srcSize;
			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);
			.LZ4F_lastBlockStatus lastBlockCompressed = .LZ4F_lastBlockStatus.notDone;
			const .compressFunc_t compress = LZ4F_selectCompression(this.prefs.frameInfo.blockMode, this.prefs.compressionLevel);

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compressUpdate (srcSize=%u)`, srcSize);
			}

			if (this.cStage != 1) {
				return .err0r(.LZ4F_errorCodes.GENERIC);
			}

			if (dstCapacity < .LZ4F_compressBound_internal(srcSize, &(this.prefs), this.tmpInSize)) {
				return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
			}

			//cOptionsNull = .LZ4F_compressOptions_t.init;

			if (compressOptionsPtr == null) {
				compressOptionsPtr = &cOptionsNull;
			}

			/* complete tmp buffer */
			/* some data already within tmp buffer */
			if (this.tmpInSize > 0) {
				const size_t sizeToCopy = blockSize - this.tmpInSize;

				if (sizeToCopy > srcSize) {
					/* add src to tmpIn buffer */
					core.stdc.string.memcpy(this.tmpIn + this.tmpInSize, srcBuffer, srcSize);
					srcPtr = srcEnd;
					this.tmpInSize += srcSize;
					/* still needs some CRC */
				} else {
					/* complete tmpIn block and then compress it */
					lastBlockCompressed = .LZ4F_lastBlockStatus.fromTmpBuffer;
					core.stdc.string.memcpy(this.tmpIn + this.tmpInSize, srcBuffer, sizeToCopy);
					srcPtr += sizeToCopy;

					dstPtr += .LZ4F_makeBlock(dstPtr, this.tmpIn, blockSize, compress, this.lz4CtxPtr, this.prefs.compressionLevel, this.cdict, this.prefs.frameInfo.blockChecksumFlag);

					if (this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
						this.tmpIn += blockSize;
					}

					this.tmpInSize = 0;
				}
			}

			while (cast(size_t)(srcEnd - srcPtr) >= blockSize) {
				/* compress full blocks */
				lastBlockCompressed = .LZ4F_lastBlockStatus.fromSrcBuffer;
				dstPtr += .LZ4F_makeBlock(dstPtr, srcPtr, blockSize, compress, this.lz4CtxPtr, this.prefs.compressionLevel, this.cdict, this.prefs.frameInfo.blockChecksumFlag);
				srcPtr += blockSize;
			}

			if ((this.prefs.autoFlush) && (srcPtr < srcEnd)) {
				/* compress remaining input < blockSize */
				lastBlockCompressed = .LZ4F_lastBlockStatus.fromSrcBuffer;
				dstPtr += .LZ4F_makeBlock(dstPtr, srcPtr, srcEnd - srcPtr, compress, this.lz4CtxPtr, this.prefs.compressionLevel, this.cdict, this.prefs.frameInfo.blockChecksumFlag);
				srcPtr = srcEnd;
			}

			/* preserve dictionary if necessary */
			if ((this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) && (lastBlockCompressed == .LZ4F_lastBlockStatus.fromSrcBuffer)) {
				if ((*compressOptionsPtr).stableSrc) {
					this.tmpIn = this.tmpBuff;
				} else {
					const int realDictSize = this.localSaveDict();

					if (realDictSize == 0) {
						return .err0r(.LZ4F_errorCodes.GENERIC);
					}

					this.tmpIn = this.tmpBuff + realDictSize;
				}
			}

			/* keep tmpIn within limits */
			if ((this.tmpIn + blockSize) > (this.tmpBuff + this.maxBufferSize) && !(this.prefs.autoFlush)) {
				const int realDictSize = this.localSaveDict();
				this.tmpIn = this.tmpBuff + realDictSize;
			}

			/* some input data left, necessarily < blockSize */
			if (srcPtr < srcEnd) {
				/* fill tmp buffer */
				const size_t sizeToCopy = srcEnd - srcPtr;
				core.stdc.string.memcpy(this.tmpIn, srcPtr, sizeToCopy);
				this.tmpInSize = sizeToCopy;
			}

			if (this.prefs.frameInfo.contentChecksumFlag == .LZ4F_contentChecksum_t.contentChecksumEnabled) {
				this.xxh.update(srcBuffer, srcSize);
			}

			this.totalInSize += srcSize;

			return dstPtr - dstStart;
		}

	/**
	 * flush() :
	 *  When data must be generated and sent immediately, without waiting for a block to be completely filled,
	 *  it's possible to call LZ4_flush(). It will immediately compress any data buffered within cctx.
	 * `dstCapacity` must be large enough to ensure the operation will be successful.
	 * `cOptPtr` is optional : it's possible to provide null, all options will be set to default.
	 *  Note : LZ4F_flush() is guaranteed to be successful when dstCapacity >= LZ4F_compressBound(0, prefsPtr).
	 *
	 * Returns: nb of bytes written into dstBuffer (can be zero, when there is no data stored within cctx) or an error code if it fails (which can be tested using LZ4F_isError())
	 */
	/**
	 * flush() :
	 *  Should you need to create compressed data immediately, without waiting for a block to be filled,
	 *  you can call LZ4_flush(), which will immediately compress any remaining data stored within compressionContext.
	 *  The result of the function is the number of bytes written into dstBuffer
	 *  (it can be zero, this means there was no data left within compressionContext)
	 *  The function outputs an error code if it fails (can be tested using LZ4F_isError())
	 *  The LZ4F_compressOptions_t structure is optional : you can provide null as argument.
	 */
	//LZ4FLIB_API
	@system
	public size_t flush(void* dstBuffer, const size_t dstCapacity, const .LZ4F_compressOptions_t* compressOptionsPtr)

		in
		{
			assert(dstBuffer != null);
		}

		do
		{
			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);
			.compressFunc_t compress;

			if (this.tmpInSize == 0) {
				/* nothing to flush */
				return 0;
			}

			if (this.cStage != 1) {
				return .err0r(.LZ4F_errorCodes.GENERIC);
			}

			if (dstCapacity < (this.tmpInSize + 4)) {
				/* +4 : block header(4)  */
				return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
			}

			/* not yet useful */
			//(void)compressOptionsPtr;

			/* select compression function */
			compress = LZ4F_selectCompression(this.prefs.frameInfo.blockMode, this.prefs.compressionLevel);

			/* compress tmp buffer */
			dstPtr += .LZ4F_makeBlock(dstPtr, this.tmpIn, this.tmpInSize, compress, this.lz4CtxPtr, this.prefs.compressionLevel, this.cdict, this.prefs.frameInfo.blockChecksumFlag);

			if (this.prefs.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
				this.tmpIn += this.tmpInSize;
			}

			this.tmpInSize = 0;

			/* keep tmpIn within limits */
			if ((this.tmpIn + this.maxBlockSize) > (this.tmpBuff + this.maxBufferSize)) {  /* necessarily LZ4F_blockMode_t.blockLinked */
				int realDictSize = this.localSaveDict();
				this.tmpIn = this.tmpBuff + realDictSize;
			}

			return dstPtr - dstStart;
		}

	/**
	 * compressEnd() :
	 *  To properly finish an LZ4 frame, invoke compressEnd().
	 *  It will flush whatever data remained within `cctx` (like LZ4_flush())
	 *  and properly finalize the frame, with an endMark and a checksum.
	 * `cOptPtr` is optional : null can be provided, in which case all options will be set to default.
	 *  Note : LZ4F_compressEnd() is guaranteed to be successful when dstCapacity >= LZ4F_compressBound(0, prefsPtr).
	 *  A successful call to compressEnd() makes `cctx` available again for another compression task.
	 *
	 * Returns: nb of bytes written into dstBuffer, necessarily >= 4 (endMark), or an error code if it fails (which can be tested using LZ4F_isError())
	 */
	/**
	 * compressEnd() :
	 *  When you want to properly finish the compressed frame, just call LZ4F_compressEnd().
	 *  It will flush whatever data remained within compressionContext (like LZ4_flush())
	 *  but also properly finalize the frame, with an endMark and an (optional) checksum.
	 *  LZ4F_compressOptions_t structure is optional : you can provide NULL as argument.
	 * @return: the number of bytes written into dstBuffer (necessarily >= 4 (endMark size))
	 *       or an error code if it fails (can be tested using LZ4F_isError())
	 *  The context can then be used again to compress a new frame, starting with LZ4F_compressBegin().
	 */
	//LZ4FLIB_API
	@system
	public size_t compressEnd(void* dstBuffer, size_t dstCapacity, const .LZ4F_compressOptions_t* compressOptionsPtr)

		in
		{
			assert(dstBuffer != null);
		}

		do
		{
			static import lz4_lib.xxhash;

			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);

			const size_t flushSize = this.flush(dstBuffer, dstCapacity, compressOptionsPtr);

			if (.LZ4F_isError(flushSize)) {
				return flushSize;
			}

			dstPtr += flushSize;

			assert(flushSize <= dstCapacity);
			dstCapacity -= flushSize;

			if (dstCapacity < 4) {
				return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
			}

			.writeLE32(dstPtr, 0);

			/* endMark */
			dstPtr += 4;

			if (this.prefs.frameInfo.contentChecksumFlag == .LZ4F_contentChecksum_t.contentChecksumEnabled) {
				const uint xxh = this.xxh.digest_endian();
				.writeLE32(dstPtr, xxh);

				if (dstCapacity < 8) {
					return .err0r(.LZ4F_errorCodes.dstMaxSize_tooSmall);
				}

				/* content Checksum */
				dstPtr += 4;
			}

			/* state is now re-usable (with identical preferences) */
			this.cStage = 0;

			/* reuse HC context */
			this.maxBufferSize = 0;

			if (this.prefs.frameInfo.contentSize) {
				if (this.prefs.frameInfo.contentSize != this.totalInSize) {
					return .err0r(.LZ4F_errorCodes.frameSize_wrong);
				}
			}

			return dstPtr - dstStart;
		}
}

struct LZ4F_compressOptions_t
{
	/**
	 * 1 == src content will remain present on future calls to LZ4F_compress(); skip copying src content within tmp buffer
	 */
	uint stableSrc;

	uint[3] reserved;
}

struct LZ4F_dctx
{
	static import lz4_lib.xxhash;

	.LZ4F_frameInfo_t frameInfo;
	uint version_Number;
	.dStage_t dStage;
	ulong frameRemainingSize;
	size_t maxBlockSize;
	size_t maxBufferSize;
	ubyte* tmpIn;
	size_t tmpInSize;
	size_t tmpInTarget;
	ubyte* tmpOutBuffer;
	const (ubyte)* dict;
	size_t dictSize;
	ubyte* tmpOut;
	size_t tmpOutSize;
	size_t tmpOutStart;
	lz4_lib.xxhash.XXH32_state_t xxh;
	lz4_lib.xxhash.XXH32_state_t blockChecksum;
	ubyte[.LZ4F_HEADER_SIZE_MAX] header;

	/**
	 * resetDecompressionContext() : added in v1.8.0
	 *  In case of an error, the context is left in "undefined" state.
	 *  In which case, it's necessary to reset it, before re-using it.
	 *  This method can also be used to abruptly stop any unfinished decompression,
	 *  and start a new one using same context resources.
	 */
	/* always successful */
	//LZ4FLIB_API
	pure nothrow @safe @nogc
	public void resetDecompressionContext()

		in
		{
		}

		do
		{
			this.dStage = .dStage_t.getFrameHeader;
			this.dict = null;
			this.dictSize = 0;
		}

	/**
	 * decodeHeader() :
	 *  input   : `src` points at the **beginning of the frame**
	 *  output  : set internal values of this, such as
	 *            (*this).frameInfo and (*this).dStage.
	 *            Also allocates internal buffers.
	 *
	 * Returns: nb Bytes read from src (necessarily <= srcSize) or an error code (testable with LZ4F_isError())
	 */
	pure nothrow @nogc @system
	package size_t decodeHeader(const void* src, const size_t srcSize)

		in
		{
			assert(src != null);
		}

		do
		{
			static import core.stdc.string;

			uint blockMode;
			uint blockChecksumFlag;
			uint contentSizeFlag;
			uint contentChecksumFlag;
			uint dictIDFlag;
			uint blockSizeID;
			size_t frameHeaderSize;
			const (ubyte)* srcPtr = cast(const (ubyte)*)(src);

			/* need to decode header to get frameInfo */
			if (srcSize < .minFHSize) {
				/* minimal frame header size */
				return .err0r(.LZ4F_errorCodes.frameHeader_incomplete);
			}

			this.frameInfo.INIT_FRAMEINFO();

			/* special case : skippable frames */
			if ((.readLE32(srcPtr) & 0xFFFFFFF0u) == .LZ4F_MAGIC_SKIPPABLE_START) {
				this.frameInfo.frameType = .LZ4F_frameType_t.skippableFrame;

				if (src == cast(void*)(&this.header[0])) {
					this.tmpInSize = srcSize;
					this.tmpInTarget = 8;
					this.dStage = .dStage_t.storeSFrameSize;

					return srcSize;
				} else {
					this.dStage = .dStage_t.getSFrameSize;

					return 4;
				}
			}

			/* control magic number */
			if (.readLE32(srcPtr) != .LZ4F_MAGICNUMBER) {
				return .err0r(.LZ4F_errorCodes.frameType_unknown);
			}

			this.frameInfo.frameType = .LZ4F_frameType_t.frame;

			/* Flags */
			{
				const uint FLG = srcPtr[4];
				const uint version_Number = (FLG >> 6) & ._2BITS;
				blockChecksumFlag = (FLG >> 4) & ._1BIT;
				blockMode = (FLG >> 5) & ._1BIT;
				contentSizeFlag = (FLG >> 3) & ._1BIT;
				contentChecksumFlag = (FLG >> 2) & ._1BIT;
				dictIDFlag = FLG & ._1BIT;

				/* validate */
				if (((FLG >> 1)&._1BIT) != 0) {
					/* Reserved bit */
					return .err0r(.LZ4F_errorCodes.reservedFlag_set);
				}

				if (version_Number != 1) {
					/* Version Number, only supported value */
					return .err0r(.LZ4F_errorCodes.headerVersion_wrong);
				}
			}

			/* Frame Header Size */
			frameHeaderSize = .minFHSize + (contentSizeFlag * 8) + (dictIDFlag * 4);

			if (srcSize < frameHeaderSize) {
				/* not enough input to fully decode frame header */
				if (srcPtr != &this.header[0]) {
					core.stdc.string.memcpy(&this.header[0], srcPtr, srcSize);
				}

				this.tmpInSize = srcSize;
				this.tmpInTarget = frameHeaderSize;
				this.dStage = .dStage_t.storeFrameHeader;

				return srcSize;
			}

			{
				const uint BD = srcPtr[5];
				blockSizeID = (BD >> 4) & ._3BITS;

				/* validate */
				if (((BD >> 7)&._1BIT) != 0) {
					/* Reserved bit */
					return .err0r(.LZ4F_errorCodes.reservedFlag_set);
				}

				if (blockSizeID < 4) {
					/* 4-7 only supported values for the time being */
					return .err0r(.LZ4F_errorCodes.maxBlockSize_invalid);
				}

				if (((BD >> 0)&._4BITS) != 0) {
					/* Reserved bits */
					return .err0r(.LZ4F_errorCodes.reservedFlag_set);
				}
			}

			/* check header */
			{
				const ubyte HC = .LZ4F_headerChecksum(srcPtr + 4, frameHeaderSize - 5);

				if (HC != srcPtr[frameHeaderSize - 1]) {
					return .err0r(.LZ4F_errorCodes.headerChecksum_invalid);
				}
			}

			/* save */
			this.frameInfo.blockMode = cast(.LZ4F_blockMode_t)(blockMode);
			this.frameInfo.blockChecksumFlag = cast(.LZ4F_blockChecksum_t)(blockChecksumFlag);
			this.frameInfo.contentChecksumFlag = cast(.LZ4F_contentChecksum_t)(contentChecksumFlag);
			this.frameInfo.blockSizeID = cast(.LZ4F_blockSizeID_t)(blockSizeID);
			this.maxBlockSize = .LZ4F_getBlockSize(blockSizeID);

			if (contentSizeFlag) {
				this.frameRemainingSize = this.frameInfo.contentSize = .readLE64(srcPtr + 6);
			}

			if (dictIDFlag) {
				this.frameInfo.dictID = .readLE32(srcPtr + frameHeaderSize - 5);
			}

			this.dStage = .dStage_t.dstage_init;

			return frameHeaderSize;
		}

	/**
	 * getFrameInfo() :
	 *  This function extracts frame parameters (max blockSize, dictID, etc.).
	 *  Its usage is optional.
	 *  Extracted information is typically useful for allocation and dictionary.
	 *  This function works in 2 situations :
	 *   - At the beginning of a new frame, in which case
	 *     it will decode information from `srcBuffer`, starting the decoding process.
	 *     Input size must be large enough to successfully decode the entire frame header.
	 *     Frame header size is variable, but is guaranteed to be <= LZ4F_HEADER_SIZE_MAX bytes.
	 *     It's allowed to provide more input data than this minimum.
	 *   - After decoding has been started.
	 *     In which case, no input is read, frame parameters are extracted from this.
	 *   - If decoding has barely started, but not yet extracted information from header,
	 *     getFrameInfo() will fail.
	 *  The number of bytes consumed from srcBuffer will be updated within *srcSizePtr (necessarily <= original value).
	 *  Decompression must resume from (srcBuffer + *srcSizePtr).
	 *  note 1 : in case of error, this is not modified. Decoding operation can resume from beginning safely.
	 *  note 2 : frame parameters are *copied into* an already allocated LZ4F_frameInfo_t structure.
	 *
	 * Returns: an hint about how many srcSize bytes decompress() expects for next call, or an error code which can be tested using LZ4F_isError().
	 */
	/**
	 * getFrameInfo() :
	 *  This function extracts frame parameters (max blockSize, frame checksum, etc.).
	 *  Usage is optional. Objective is to provide relevant information for allocation purposes.
	 *  This function works in 2 situations :
	 *   - At the beginning of a new frame, in which case it will decode this information from `srcBuffer`, and start the decoding process.
	 *     Amount of input data provided must be large enough to successfully decode the frame header.
	 *     A header size is variable, but is guaranteed to be <= LZ4F_HEADER_SIZE_MAX bytes. It's possible to provide more input data than this minimum.
	 *   - After decoding has been started. In which case, no input is read, frame parameters are extracted from this.
	 *  The number of bytes consumed from srcBuffer will be updated within *srcSizePtr (necessarily <= original value).
	 *  Decompression must resume from (srcBuffer + *srcSizePtr).
	 *  note 1 : in case of error, this is not modified. Decoding operations can resume from where they stopped.
	 *  note 2 : frame parameters are *copied into* an already allocated LZ4F_frameInfo_t structure.
	 *
	 * Returns: an hint about how many srcSize bytes decompress() expects for next call, or an error code which can be tested using LZ4F_isError()
	 */
	//LZ4FLIB_API
	nothrow @nogc @system
	public size_t getFrameInfo(ref .LZ4F_frameInfo_t frameInfoPtr, const void* srcBuffer, ref size_t srcSizePtr)

		in
		{
		}

		do
		{
			static import core.stdc.string;

			/* assumption :  dstage_* header enum at beginning of range */
			if (this.dStage > .dStage_t.storeFrameHeader) {
				/* frameInfo already decoded */
				size_t o = 0;
				size_t i = 0;
				srcSizePtr = 0;
				frameInfoPtr = this.frameInfo;
				/* returns : recommended nb of bytes for decompress() */

				return this.decompress(null, o, null, i, null);
			} else {
				if (this.dStage == .dStage_t.storeFrameHeader) {
					/* frame decoding already started, in the middle of header => automatic fail */
					srcSizePtr = 0;

					return .err0r(.LZ4F_errorCodes.frameDecoding_alreadyStarted);
				} else {
					size_t decodeResult;
					const size_t hSize = .LZ4F_headerSize(srcBuffer, srcSizePtr);

					if (.LZ4F_isError(hSize)) {
						srcSizePtr = 0;

						return hSize;
					}

					if (srcSizePtr < hSize) {
						srcSizePtr = 0;

						return .err0r(.LZ4F_errorCodes.frameHeader_incomplete);
					}

					decodeResult = this.decodeHeader(srcBuffer, hSize);

					if (.LZ4F_isError(decodeResult)) {
						srcSizePtr = 0;
					} else {
						srcSizePtr = decodeResult;

						/* block header size */
						decodeResult = BHSize;
					}

					frameInfoPtr = this.frameInfo;

					return decodeResult;
				}
			}
		}

	/**
	 * updateDict() :
	 * only used for LZ4F_blockMode_t.blockLinked mode
	 */
	pure nothrow @nogc @system
	package void updateDict(const (ubyte)* dstPtr, const size_t dstSize, const (ubyte)* dstBufferStart, const uint withinTmp)

		in
		{
			assert(dstPtr != null);
			assert(dstBufferStart != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			if (this.dictSize == 0) {
				/* priority to dictionary continuity */
				this.dict = cast(const (ubyte)*)(dstPtr);
			}

			/* dictionary continuity, directly within dstBuffer */
			if (this.dict + this.dictSize == dstPtr) {
				this.dictSize += dstSize;

				return;
			}

			/* history in dstBuffer becomes large enough to become dictionary */
			if (dstPtr - dstBufferStart + dstSize >= lz4_lib.lz4common.KB!(size_t, 64)) {
				this.dict = cast(const (ubyte)*)(dstBufferStart);
				this.dictSize = dstPtr - dstBufferStart + dstSize;

				return;
			}

			/* if dstSize >= 64 KB, dictionary would be set into dstBuffer directly */
			assert(dstSize < lz4_lib.lz4common.KB!(size_t, 64));

			/* dstBuffer does not contain whole useful history (64 KB), so it must be saved within tmpOut */

			/* continue history within tmpOutBuffer */
			if ((withinTmp) && (this.dict == this.tmpOutBuffer)) {
				/* withinTmp expectation : content of [dstPtr,dstSize] is same as [dict+dictSize,dstSize], so we just extend it */
				assert(this.dict + this.dictSize == this.tmpOut + this.tmpOutStart);
				this.dictSize += dstSize;

				return;
			}

			if (withinTmp) { /* copy relevant dict portion in front of tmpOut within tmpOutBuffer */
				const size_t preserveSize = this.tmpOut - this.tmpOutBuffer;
				size_t copySize = lz4_lib.lz4common.KB!(size_t, 64) - this.tmpOutSize;
				const ubyte* oldDictEnd = this.dict + this.dictSize - this.tmpOutStart;

				if (this.tmpOutSize > lz4_lib.lz4common.KB!(size_t, 64)) {
					copySize = 0;
				}

				if (copySize > preserveSize) {
					copySize = preserveSize;
				}

				core.stdc.string.memcpy(this.tmpOutBuffer + preserveSize - copySize, oldDictEnd - copySize, copySize);

				this.dict = this.tmpOutBuffer;
				this.dictSize = preserveSize + this.tmpOutStart + dstSize;

				return;
			}

			/* copy dst into tmp to complete dict */
			if (this.dict == this.tmpOutBuffer) {
				/* tmp buffer not large enough */
				if (this.dictSize + dstSize > this.maxBufferSize) {
					const size_t preserveSize = lz4_lib.lz4common.KB!(size_t, 64) - dstSize;
					core.stdc.string.memcpy(this.tmpOutBuffer, this.dict + this.dictSize - preserveSize, preserveSize);
					this.dictSize = preserveSize;
				}

				core.stdc.string.memcpy(this.tmpOutBuffer + this.dictSize, dstPtr, dstSize);
				this.dictSize += dstSize;

				return;
			}

			/* join dict & dest into tmp */
			{
				size_t preserveSize = lz4_lib.lz4common.KB!(size_t, 64) - dstSize;

				if (preserveSize > this.dictSize) {
					preserveSize = this.dictSize;
				}

				core.stdc.string.memcpy(this.tmpOutBuffer, this.dict + this.dictSize - preserveSize, preserveSize);
				core.stdc.string.memcpy(this.tmpOutBuffer + preserveSize, dstPtr, dstSize);
				this.dict = this.tmpOutBuffer;
				this.dictSize = preserveSize + dstSize;
			}
		}

	/**
	 * decompress() :
	 *  Call this function repetitively to regenerate compressed data from `srcBuffer`.
	 *  The function will read up to *srcSizePtr bytes from srcBuffer,
	 *  and decompress data into dstBuffer, of capacity *dstSizePtr.
	 *
	 *  The nb of bytes consumed from srcBuffer will be written into *srcSizePtr (necessarily <= original value).
	 *  The nb of bytes decompressed into dstBuffer will be written into *dstSizePtr (necessarily <= original value).
	 *
	 *  The function does not necessarily read all input bytes, so always check value in *srcSizePtr.
	 *  Unconsumed source data must be presented again in subsequent invocations.
	 *
	 * `dstBuffer` can freely change between each consecutive function invocation.
	 * `dstBuffer` content will be overwritten.
	 *
	 *
	 * Returns: an hint of how many `srcSize` bytes decompress() expects for next call.
	 *  Schematically, it's the size of the current (or remaining) compressed block + header of next block.
	 *  Respecting the hint provides some small speed benefit, because it skips intermediate buffers.
	 *  This is just a hint though, it's always possible to provide any srcSize.
	 *
	 *  When a frame is fully decoded, @return will be 0 (no more data expected).
	 *  When provided with more bytes than necessary to decode a frame,
	 *  decompress() will stop reading exactly at end of current frame, and @return 0.
	 *
	 *  If decompression failed, @return is an error code, which can be tested using LZ4F_isError().
	 *  After a decompression error, the `this` context is not resumable.
	 *  Use resetDecompressionContext() to return to clean state.
	 *
	 *  After a frame is fully decoded, this can be used again to decompress another frame.
	 */
	/**
	 * decompress() :
	 *  Call this function repetitively to regenerate compressed data in srcBuffer.
	 *  The function will attempt to decode up to *srcSizePtr bytes from srcBuffer
	 *  into dstBuffer of capacity *dstSizePtr.
	 *
	 *  The number of bytes regenerated into dstBuffer will be provided within *dstSizePtr (necessarily <= original value).
	 *
	 *  The number of bytes effectively read from srcBuffer will be provided within *srcSizePtr (necessarily <= original value).
	 *  If number of bytes read is < number of bytes provided, then decompression operation is not complete.
	 *  Remaining data will have to be presented again in a subsequent invocation.
	 *
	 *  The function result is an hint of the better srcSize to use for next call to decompress.
	 *  Schematically, it's the size of the current (or remaining) compressed block + header of next block.
	 *  Respecting the hint provides a small boost to performance, since it allows less buffer shuffling.
	 *  Note that this is just a hint, and it's always possible to any srcSize value.
	 *  When a frame is fully decoded, @return will be 0.
	 *  If decompression failed, @return is an error code which can be tested using LZ4F_isError().
	 */
	//LZ4FLIB_API
	nothrow @nogc @system
	public size_t decompress(void* dstBuffer, ref size_t dstSizePtr, const void* srcBuffer, ref size_t srcSizePtr, const (.LZ4F_decompressOptions_t)* decompressOptionsPtr)

		in
		{
			assert(dstBuffer != null);
			assert(srcBuffer != null);
			//assert(this.maxBlockSize <= (size_t.max - 4));
		}

		do
		{
			static import core.stdc.stdlib;
			static import core.stdc.string;
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;
			static import lz4_lib.lz4hc;
			static import lz4_lib.xxhash;

			.LZ4F_decompressOptions_t optionsNull;
			const ubyte* srcStart = cast(const (ubyte)*)(srcBuffer);
			const ubyte* srcEnd = srcStart + srcSizePtr;
			const (ubyte)* srcPtr = srcStart;
			const ubyte* dstStart = cast(ubyte*)(dstBuffer);
			const ubyte* dstEnd = dstStart + dstSizePtr;
			ubyte* dstPtr = cast(ubyte*)(dstBuffer);
			const (ubyte)* selectedIn = null;
			uint doAnotherStage = 1;
			size_t nextSrcSizeHint = 1;

			//optionsNull = .LZ4F_decompressOptions_t.init;

			if (decompressOptionsPtr == null) {
				decompressOptionsPtr = &optionsNull;
			}

			srcSizePtr = 0;
			dstSizePtr = 0;

			/* behaves as a state machine */

			while (doAnotherStage) {
				switch (this.dStage) {
					case .dStage_t.getFrameHeader:
						if (cast(size_t)(srcEnd - srcPtr) >= maxFHSize) { /* enough to decode - shortcut */

							/* will update dStage appropriately */
							const size_t hSize = this.decodeHeader(srcPtr, srcEnd - srcPtr);

							if (.LZ4F_isError(hSize)) {
								return hSize;
							}

							srcPtr += hSize;
							break;
						}

						this.tmpInSize = 0;

						if (srcEnd - srcPtr == 0) {
							/* 0-size input */
							return .minFHSize;
						}

						/* minimum size to decode header */
						this.tmpInTarget = .minFHSize;

						this.dStage = .dStage_t.storeFrameHeader;
						goto case;

					case .dStage_t.storeFrameHeader:
						{
							const size_t sizeToCopy = lz4_lib.lz4common.MIN(this.tmpInTarget - this.tmpInSize, cast(size_t)(srcEnd - srcPtr));
							core.stdc.string.memcpy(&this.header[0] + this.tmpInSize, srcPtr, sizeToCopy);
							this.tmpInSize += sizeToCopy;
							srcPtr += sizeToCopy;
						}

						if (this.tmpInSize < this.tmpInTarget) {
							/* rest of header + nextBlockHeader */
							nextSrcSizeHint = (this.tmpInTarget - this.tmpInSize) + BHSize;

							/* not enough src data, ask for some more */
							doAnotherStage = 0;

							break;
						}

						{
							/* will update dStage appropriately */
							const size_t hSize = this.decodeHeader(&this.header[0], this.tmpInTarget);

							if (.LZ4F_isError(hSize)) {
								return hSize;
							}
						}

						break;

					case .dStage_t.dstage_init:
						if (this.frameInfo.contentChecksumFlag) {
							this.xxh.reset(0);
						}

						/* internal buffers allocation */
						{
							const size_t bufferNeeded = this.maxBlockSize + ((this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) * lz4_lib.lz4common.KB!(size_t, 128));

							/* tmp buffers too small */
							if (bufferNeeded > this.maxBufferSize) {

								/* ensure allocation will be re-attempted on next entry*/
								this.maxBufferSize = 0;

								core.stdc.stdlib.free(this.tmpIn);
								this.tmpIn = cast(ubyte*)(core.stdc.stdlib.malloc(this.maxBlockSize + 4 /* size for block checksum */));

								if (this.tmpIn == null) {
									return .err0r(.LZ4F_errorCodes.allocation_failed);
								}

								core.stdc.stdlib.free(this.tmpOutBuffer);
								this.tmpOutBuffer = cast(ubyte*)(core.stdc.stdlib.malloc(bufferNeeded));

								if (this.tmpOutBuffer == null) {
									return .err0r(.LZ4F_errorCodes.allocation_failed);
								}

								this.maxBufferSize = bufferNeeded;
							}
						}

						this.tmpInSize = 0;
						this.tmpInTarget = 0;
						this.tmpOut = this.tmpOutBuffer;
						this.tmpOutStart = 0;
						this.tmpOutSize = 0;

						this.dStage = .dStage_t.getBlockHeader;
						goto case;

					case .dStage_t.getBlockHeader:
						if (cast(size_t)(srcEnd - srcPtr) >= BHSize) {
							selectedIn = srcPtr;
							srcPtr += BHSize;
						} else {
							/* not enough input to read cBlockSize field */
							this.tmpInSize = 0;
							this.dStage = .dStage_t.storeBlockHeader;
						}

						if (this.dStage == .dStage_t.storeBlockHeader) {
							case .dStage_t.storeBlockHeader: {
									const size_t remainingInput = cast(size_t)(srcEnd - srcPtr);
									const size_t wantedData = BHSize - this.tmpInSize;
									const size_t sizeToCopy = lz4_lib.lz4common.MIN(wantedData, remainingInput);
									core.stdc.string.memcpy(this.tmpIn + this.tmpInSize, srcPtr, sizeToCopy);
									srcPtr += sizeToCopy;
									this.tmpInSize += sizeToCopy;

									/* not enough input for cBlockSize */
									if (this.tmpInSize < BHSize) {
										nextSrcSizeHint = BHSize - this.tmpInSize;
										doAnotherStage = 0;
										break;
								}

								selectedIn = this.tmpIn;
							}
						}

						/* decode block header */
						{
							const size_t nextCBlockSize = .readLE32(selectedIn) & 0x7FFFFFFFU;
							const size_t crcSize = this.frameInfo.blockChecksumFlag * 4;

							if (nextCBlockSize == 0) { /* frameEnd signal, no more block */
								this.dStage = .dStage_t.getSuffix;
								break;
							}

							if (nextCBlockSize > this.maxBlockSize) {
								return .err0r(.LZ4F_errorCodes.maxBlockSize_invalid);
							}

							if (.readLE32(selectedIn) & .LZ4F_BLOCKUNCOMPRESSED_FLAG) {
								/* next block is uncompressed */
								this.tmpInTarget = nextCBlockSize;

								if (this.frameInfo.blockChecksumFlag) {
									this.blockChecksum.reset(0);
								}

								this.dStage = .dStage_t.copyDirect;
								break;
							}

							/* next block is a compressed block */
							this.tmpInTarget = nextCBlockSize + crcSize;
							this.dStage = .dStage_t.getCBlock;

							if (dstPtr == dstEnd) {
								nextSrcSizeHint = nextCBlockSize + crcSize + BHSize;
								doAnotherStage = 0;
							}

							break;
						}

					case .dStage_t.copyDirect:
						{ /* uncompressed block */
							const size_t minBuffSize = lz4_lib.lz4common.MIN(cast(size_t)(srcEnd - srcPtr), cast(size_t)(dstEnd - dstPtr));
							const size_t sizeToCopy = lz4_lib.lz4common.MIN(this.tmpInTarget, minBuffSize);
							core.stdc.string.memcpy(dstPtr, srcPtr, sizeToCopy);

							if (this.frameInfo.blockChecksumFlag) {
								this.blockChecksum.update(srcPtr, sizeToCopy);
							}

							if (this.frameInfo.contentChecksumFlag) {
								this.xxh.update(srcPtr, sizeToCopy);
							}

							if (this.frameInfo.contentSize) {
								this.frameRemainingSize -= sizeToCopy;
							}

							/* history management (linked blocks only)*/
							if (this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
								this.updateDict(dstPtr, sizeToCopy, dstStart, 0);
							}

							srcPtr += sizeToCopy;
							dstPtr += sizeToCopy;

							/* all done */
							if (sizeToCopy == this.tmpInTarget) {
								if (this.frameInfo.blockChecksumFlag) {
									this.tmpInSize = 0;
									this.dStage = .dStage_t.getBlockChecksum;
								} else {
									/* new block */
									this.dStage = .dStage_t.getBlockHeader;
								}

								break;
							}

							/* need to copy more */
							this.tmpInTarget -= sizeToCopy;

							nextSrcSizeHint = this.tmpInTarget + this.frameInfo.blockChecksumFlag * 4 + BHSize;
							doAnotherStage = 0;
							break;
						}

					/* check block checksum for recently transferred uncompressed block */
					case .dStage_t.getBlockChecksum:
						{
							const (void)* crcSrc;

							if ((srcEnd - srcPtr >= 4) && (this.tmpInSize == 0)) {
								crcSrc = srcPtr;
								srcPtr += 4;
							} else {
								const size_t stillToCopy = 4 - this.tmpInSize;
								const size_t sizeToCopy = lz4_lib.lz4common.MIN(stillToCopy, cast(size_t)(srcEnd - srcPtr));
								core.stdc.string.memcpy(&this.header[0] + this.tmpInSize, srcPtr, sizeToCopy);
								this.tmpInSize += sizeToCopy;
								srcPtr += sizeToCopy;

								/* all input consumed */
								if (this.tmpInSize < 4) {
									doAnotherStage = 0;
									break;
								}

								crcSrc = &this.header[0];
							}

							{
								const uint readCRC = .readLE32(crcSrc);
								const uint calcCRC = this.blockChecksum.digest_endian();

								if (readCRC != calcCRC) {
									return .err0r(.LZ4F_errorCodes.blockChecksum_invalid);
								}
							}
						}

						/* new block */
						this.dStage = .dStage_t.getBlockHeader;

						break;

					case .dStage_t.getCBlock:
						if (cast(size_t)(srcEnd - srcPtr) < this.tmpInTarget) {
							this.tmpInSize = 0;
							this.dStage = .dStage_t.storeCBlock;
							break;
						}

						/* input large enough to read full block directly */
						selectedIn = srcPtr;
						srcPtr += this.tmpInTarget;

						/* jump over next block */
						static if (0) {
							case .dStage_t.storeCBlock:
								{
									const size_t wantedData = this.tmpInTarget - this.tmpInSize;
									const size_t inputLeft = cast(size_t)(srcEnd - srcPtr);
									const size_t sizeToCopy = lz4_lib.lz4common.MIN(wantedData, inputLeft);
									core.stdc.string.memcpy(this.tmpIn + this.tmpInSize, srcPtr, sizeToCopy);
									this.tmpInSize += sizeToCopy;
									srcPtr += sizeToCopy;

									if (this.tmpInSize < this.tmpInTarget) { /* need more input */
										nextSrcSizeHint = (this.tmpInTarget - this.tmpInSize)
											+ (this.frameInfo.blockChecksumFlag * 4)   /* size for block checksum */
											+ BHSize /* next header size */;

										doAnotherStage = 0;
										break;
									}

									selectedIn = this.tmpIn;
								}

							/* At this stage, input is large enough to decode a block */
							if (this.frameInfo.blockChecksumFlag) {
								this.tmpInTarget -= 4;

								/* selectedIn is defined at this stage (either srcPtr, or this.tmpIn) */
								assert(selectedIn != null);

								{
									const uint readBlockCrc = .readLE32(selectedIn + this.tmpInTarget);
									const uint calcBlockCrc = lz4_lib.xxhash.XXH32(selectedIn, this.tmpInTarget, 0);

									if (readBlockCrc != calcBlockCrc) {
										return .err0r(.LZ4F_errorCodes.blockChecksum_invalid);
									}
								}
							}

							if (cast(size_t)(dstEnd - dstPtr) >= this.maxBlockSize) {
								const (char)* dict = cast(const (char)*)(this.dict);
								size_t dictSize = this.dictSize;
								int decodedSize;

								if (dict && dictSize > lz4_lib.lz4common.GB!(size_t, 1)) {
									/* the dictSize param is an int, avoid truncation / sign issues */
									dict += dictSize - lz4_lib.lz4common.KB!(size_t, 64);
									dictSize = lz4_lib.lz4common.KB!(size_t, 64);
								}

								/* enough capacity in `dst` to decompress directly there */
								decodedSize = lz4_lib.lz4.LZ4_decompress_safe_usingDict(cast(const (char)*)(selectedIn), cast(char*)(dstPtr), cast(int)(this.tmpInTarget), cast(int)(this.maxBlockSize), dict, cast(int)(dictSize));

								if (decodedSize < 0) {
									/* decompression failed */
									return .err0r(.LZ4F_errorCodes.GENERIC);
								}

								if (this.frameInfo.contentChecksumFlag) {
									this.xxh.update(dstPtr, decodedSize);
								}

								if (this.frameInfo.contentSize) {
									this.frameRemainingSize -= decodedSize;
								}

								/* dictionary management */
								if (this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
									this.updateDict(dstPtr, decodedSize, dstStart, 0);
								}

								dstPtr += decodedSize;
								this.dStage = .dStage_t.getBlockHeader;
								break;
							}

							/* not enough place into dst : decode into tmpOut */
							/* ensure enough place for tmpOut */
							if (this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
								if (this.dict == this.tmpOutBuffer) {
									if (this.dictSize > lz4_lib.lz4common.KB!(size_t, 128)) {
										core.stdc.string.memcpy(this.tmpOutBuffer, this.dict + this.dictSize - lz4_lib.lz4common.KB!(size_t, 64), lz4_lib.lz4common.KB!(size_t, 64));
										this.dictSize = lz4_lib.lz4common.KB!(size_t, 64);
									}

									this.tmpOut = this.tmpOutBuffer + this.dictSize;
								} else {
									/* dict not within tmp */
									const size_t reservedDictSpace = lz4_lib.lz4common.MIN(this.dictSize, lz4_lib.lz4common.KB!(size_t, 64));
									this.tmpOut = this.tmpOutBuffer + reservedDictSpace;
								}
							}

							/* Decode block */
							{
								const (char)* dict = cast(const (char)*)(this.dict);
								size_t dictSize = this.dictSize;
								int decodedSize;

								if (dict && dictSize > lz4_lib.lz4common.GB!(size_t, 1)) {
									/* the dictSize param is an int, avoid truncation / sign issues */
									dict += dictSize - lz4_lib.lz4common.KB!(size_t, 64);
									dictSize = lz4_lib.lz4common.KB!(size_t, 64);
								}

								decodedSize = lz4_lib.lz4.LZ4_decompress_safe_usingDict(cast(const (char)*)(selectedIn), cast(char*)(this.tmpOut), cast(int)(this.tmpInTarget), cast(int)(this.maxBlockSize), dict, cast(int)(dictSize));

								if (decodedSize < 0) { /* decompression failed */
									return .err0r(.LZ4F_errorCodes.decompressionFailed);
								}

								if (this.frameInfo.contentChecksumFlag) {
									this.xxh.update(this.tmpOut, decodedSize);
								}

								if (this.frameInfo.contentSize) {
									this.frameRemainingSize -= decodedSize;
								}

								this.tmpOutSize = decodedSize;
								this.tmpOutStart = 0;
								this.dStage = .dStage_t.flushOut;
							}
						}

						goto case;

					case .dStage_t.flushOut:
						{ /* flush decoded data from tmpOut to dstBuffer */
							const size_t sizeToCopy = lz4_lib.lz4common.MIN(this.tmpOutSize - this.tmpOutStart, cast(size_t)(dstEnd - dstPtr));
							core.stdc.string.memcpy(dstPtr, this.tmpOut + this.tmpOutStart, sizeToCopy);

							/* dictionary management */
							if (this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) {
								this.updateDict(dstPtr, sizeToCopy, dstStart, 1 /*withinTmp*/);
							}

							this.tmpOutStart += sizeToCopy;
							dstPtr += sizeToCopy;

							if (this.tmpOutStart == this.tmpOutSize) { /* all flushed */

								/* get next block */
								this.dStage = .dStage_t.getBlockHeader;

								break;
							}

							/* could not flush everything : stop there, just request a block header */
							doAnotherStage = 0;
							nextSrcSizeHint = BHSize;
							break;
						}

					case .dStage_t.getSuffix:
						if (this.frameRemainingSize) {
							/* incorrect frame size decoded */
							return .err0r(.LZ4F_errorCodes.frameSize_wrong);
						}

						/* no checksum, frame is completed */
						if (!this.frameInfo.contentChecksumFlag) {
							nextSrcSizeHint = 0;
							this.resetDecompressionContext();
							doAnotherStage = 0;
							break;
						}

						/* not enough size for entire CRC */
						if ((srcEnd - srcPtr) < 4) {
							this.tmpInSize = 0;
							this.dStage = .dStage_t.storeSuffix;
						} else {
							selectedIn = srcPtr;
							srcPtr += 4;
						}

						if (this.dStage == .dStage_t.storeSuffix) {
							case .dStage_t.storeSuffix: {
								const size_t remainingInput = cast(size_t)(srcEnd - srcPtr);
								const size_t wantedData = 4 - this.tmpInSize;
								const size_t sizeToCopy = lz4_lib.lz4common.MIN(wantedData, remainingInput);
								core.stdc.string.memcpy(this.tmpIn + this.tmpInSize, srcPtr, sizeToCopy);
								srcPtr += sizeToCopy;
								this.tmpInSize += sizeToCopy;

								if (this.tmpInSize < 4) { /* not enough input to read complete suffix */
									nextSrcSizeHint = 4 - this.tmpInSize;
									doAnotherStage = 0;
									break;
								}

								selectedIn = this.tmpIn;
							}
						}

						/* case dstage_checkSuffix: */
						/* no direct entry, avoid initialization risks */
						{
							const uint readCRC = .readLE32(selectedIn);
							const uint resultCRC = this.xxh.digest_endian();

							if (readCRC != resultCRC) {
								return .err0r(.LZ4F_errorCodes.contentChecksum_invalid);
							}

							nextSrcSizeHint = 0;
							this.resetDecompressionContext();
							doAnotherStage = 0;
							break;
						}

					case .dStage_t.getSFrameSize:
						if ((srcEnd - srcPtr) >= 4) {
							selectedIn = srcPtr;
							srcPtr += 4;
						} else {
							/* not enough input to read cBlockSize field */
							this.tmpInSize = 4;
							this.tmpInTarget = 8;
							this.dStage = .dStage_t.storeSFrameSize;
						}

						if (this.dStage == .dStage_t.storeSFrameSize) {
							case .dStage_t.storeSFrameSize: {
								const size_t sizeToCopy = lz4_lib.lz4common.MIN(this.tmpInTarget - this.tmpInSize, cast(size_t)(srcEnd - srcPtr));
								core.stdc.string.memcpy(&this.header[0] + this.tmpInSize, srcPtr, sizeToCopy);
								srcPtr += sizeToCopy;
								this.tmpInSize += sizeToCopy;

								if (this.tmpInSize < this.tmpInTarget) {
									/* not enough input to get full sBlockSize; wait for more */
									nextSrcSizeHint = this.tmpInTarget - this.tmpInSize;
									doAnotherStage = 0;
									break;
								}

								selectedIn = &this.header[0] + 4;
							}
						}

						/* case dstage_decodeSFrameSize: */
						/* no direct entry */
						{
							const size_t SFrameSize = .readLE32(selectedIn);
							this.frameInfo.contentSize = SFrameSize;
							this.tmpInTarget = SFrameSize;
							this.dStage = .dStage_t.skipSkippable;
							break;
						}

					case .dStage_t.skipSkippable:
					{
						const size_t skipSize = lz4_lib.lz4common.MIN(this.tmpInTarget, cast(size_t)(srcEnd - srcPtr));
						srcPtr += skipSize;
						this.tmpInTarget -= skipSize;
						doAnotherStage = 0;
						nextSrcSizeHint = this.tmpInTarget;

						if (nextSrcSizeHint) {
							/* still more to skip */
							break;
						}

						/* frame fully skipped : prepare context for a new frame */
						this.resetDecompressionContext();
						break;
					}

					default:
						break;
				}
			}

			/* preserve history within tmp whenever necessary */
			static assert(cast(uint)(.dStage_t.dstage_init) == 2);

			if ((this.frameInfo.blockMode == .LZ4F_blockMode_t.blockLinked) && (this.dict != this.tmpOutBuffer) && (!(*decompressOptionsPtr).stableDst) && (cast(uint)(this.dStage) - 2 < cast(uint)(.dStage_t.getSuffix) - 2)) {
				if (this.dStage == .dStage_t.flushOut) {
					const size_t preserveSize = this.tmpOut - this.tmpOutBuffer;
					size_t copySize = lz4_lib.lz4common.KB!(size_t, 64) - this.tmpOutSize;
					const (ubyte)* oldDictEnd = this.dict + this.dictSize - this.tmpOutStart;

					if (this.tmpOutSize > lz4_lib.lz4common.KB!(size_t, 64)) {
						copySize = 0;
					}

					if (copySize > preserveSize) {
						copySize = preserveSize;
					}

					if (copySize > 0) {
						core.stdc.string.memcpy(this.tmpOutBuffer + preserveSize - copySize, oldDictEnd - copySize, copySize);
					}

					this.dict = this.tmpOutBuffer;
					this.dictSize = preserveSize + this.tmpOutStart;
				} else {
					const ubyte* oldDictEnd = this.dict + this.dictSize;
					const size_t newDictSize = lz4_lib.lz4common.MIN(this.dictSize, lz4_lib.lz4common.KB!(size_t, 64));

					if (newDictSize > 0) {
						core.stdc.string.memcpy(this.tmpOutBuffer, oldDictEnd - newDictSize, newDictSize);
					}

					this.dict = this.tmpOutBuffer;
					this.dictSize = newDictSize;
					this.tmpOut = this.tmpOutBuffer + newDictSize;
				}
			}

			srcSizePtr = (srcPtr - srcStart);
			dstSizePtr = (dstPtr - dstStart);

			return nextSrcSizeHint;
		}

	/**
	 * decompress_usingDict() :
	 *  Same as decompress(), using a predefined dictionary.
	 *  Dictionary is used "in place", without any preprocessing.
	 *  It must remain accessible throughout the entire frame decoding.
	 */
	//LZ4FLIB_STATIC_API
	nothrow @nogc @system
	public size_t decompress_usingDict(void* dstBuffer, ref size_t dstSizePtr, const void* srcBuffer, ref size_t srcSizePtr, const void* dict, const size_t dictSize, const (.LZ4F_decompressOptions_t)* decompressOptionsPtr)

		in
		{
			assert(dict != null);
		}

		do
		{
			if (this.dStage <= .dStage_t.dstage_init) {
				this.dict = cast(const (ubyte)*)(dict);
				this.dictSize = dictSize;
			}

			return this.decompress(dstBuffer, dstSizePtr, srcBuffer, srcSizePtr, decompressOptionsPtr);
		}
}

struct LZ4F_decompressOptions_t
{
	/**
	 * pledges that last 64KB decompressed data will remain available unmodified. This optimization skips storage operations in tmp buffers.
	 */
	uint stableDst;

	/**
	 * must be set to zero for forward compatibility
	 */
	uint[3] reserved;

	invariant
	{
		for (size_t i = 0; i < reserved.length; i++) {
			assert(reserved[i] == 0);
		}
	}
}

package alias LZ4F_CDict_s = .LZ4F_CDict;
package alias LZ4F_cctx_s = .LZ4F_cctx_t;
package alias LZ4F_dctx_s = .LZ4F_dctx;

/*-************************************
*  Basic Types
**************************************/
/**
 * unoptimized version; solves endianess & alignment issues
 */
pure nothrow @nogc
private uint readLE32(const void* src)

	in
	{
		assert(src != null);
	}

	do
	{
		const ubyte* srcPtr = cast(const (ubyte)*)(src);
		uint value32 = srcPtr[0];
		value32 += (srcPtr[1] << 8);
		value32 += (srcPtr[2] << 16);
		value32 += (cast(uint)(srcPtr[3])) << 24;

		return value32;
	}

pure nothrow @nogc
private void writeLE32(void* dst, const uint value32)

	in
	{
		assert(dst != null);
	}

	do
	{
		ubyte* dstPtr = cast(ubyte*)(dst);
		dstPtr[0] = cast(ubyte)(value32);
		dstPtr[1] = cast(ubyte)(value32 >> 8);
		dstPtr[2] = cast(ubyte)(value32 >> 16);
		dstPtr[3] = cast(ubyte)(value32 >> 24);
	}

pure nothrow @nogc
private ulong readLE64(const void* src)

	in
	{
		assert(src != null);
	}

	do
	{
		const ubyte* srcPtr = cast(const (ubyte)*)(src);
		ulong value64 = srcPtr[0];
		value64 += (cast(ulong)(srcPtr[1]) << 8);
		value64 += (cast(ulong)(srcPtr[2]) << 16);
		value64 += (cast(ulong)(srcPtr[3]) << 24);
		value64 += (cast(ulong)(srcPtr[4]) << 32);
		value64 += (cast(ulong)(srcPtr[5]) << 40);
		value64 += (cast(ulong)(srcPtr[6]) << 48);
		value64 += (cast(ulong)(srcPtr[7]) << 56);

		return value64;
	}

pure nothrow @nogc
private void writeLE64(void* dst, const ulong value64)

	in
	{
		assert(dst != null);
	}

	do
	{
		ubyte* dstPtr = cast(ubyte*)(dst);
		dstPtr[0] = cast(ubyte)(value64);
		dstPtr[1] = cast(ubyte)(value64 >> 8);
		dstPtr[2] = cast(ubyte)(value64 >> 16);
		dstPtr[3] = cast(ubyte)(value64 >> 24);
		dstPtr[4] = cast(ubyte)(value64 >> 32);
		dstPtr[5] = cast(ubyte)(value64 >> 40);
		dstPtr[6] = cast(ubyte)(value64 >> 48);
		dstPtr[7] = cast(ubyte)(value64 >> 56);
	}

/*-************************************
*  Constants
**************************************/
enum _1BIT = 0x01;
enum _2BITS = 0x03;
enum _3BITS = 0x07;
enum _4BITS = 0x0F;
enum _8BITS = 0xFF;

enum LZ4F_MAGIC_SKIPPABLE_START = 0x184D2A50u;
enum LZ4F_MAGICNUMBER = 0x184D2204u;
enum LZ4F_BLOCKUNCOMPRESSED_FLAG = 0x80000000u;
enum LZ4F_BLOCKSIZEID_DEFAULT = .LZ4F_blockSizeID_t.max64KB;

enum size_t minFHSize = 7;

/**
 * 19
 */
enum size_t maxFHSize = .LZ4F_HEADER_SIZE_MAX;

enum size_t BHSize = 4;

/*-************************************
*  Structures and local types
**************************************/

/*-************************************
*  Error management
**************************************/
static immutable string[] LZ4F_errorStrings =
[
	`NoError`,
	`ERROR: GENERIC`,
	`ERROR: maxBlockSize_invalid`,
	`ERROR: blockMode_invalid`,
	`ERROR: contentChecksumFlag_invalid`,
	`ERROR: compressionLevel_invalid`,
	`ERROR: headerVersion_wrong`,
	`ERROR: blockChecksum_invalid`,
	`ERROR: reservedFlag_set`,
	`ERROR: allocation_failed`,
	`ERROR: srcSize_tooLarge`,
	`ERROR: dstMaxSize_tooSmall`,
	`ERROR: frameHeader_incomplete`,
	`ERROR: frameType_unknown`,
	`ERROR: frameSize_wrong`,
	`ERROR: srcPtr_wrong`,
	`ERROR: decompressionFailed`,
	`ERROR: headerChecksum_invalid`,
	`ERROR: contentChecksum_invalid`,
	`ERROR: frameDecoding_alreadyStarted`,
	`ERROR: maxCode`,
	`ERROR: _LZ4F_dummy_error_enum_for_c89_never_used`
];

static assert(.LZ4F_errorCodes.max == (.LZ4F_errorStrings.length - 1));

/**
 * tells when a function result is an error code
 */
//LZ4FLIB_API
pure nothrow @safe @nogc
public uint LZ4F_isError(const size_t code)

	in
	{
	}

	do
	{
		return (code > cast(size_t)(-.LZ4F_errorCodes.maxCode));
	}

/**
 * return error code string; for debugging
 */
//LZ4FLIB_API
pure nothrow @safe @nogc
public string LZ4F_getErrorName(const size_t code)

	in
	{
	}

	do
	{
		enum string codeError = `Unspecified error code`;

		if (.LZ4F_isError(code)) {
			return .LZ4F_errorStrings[-(cast(int)(code))];
		}

		return codeError;
	}

//LZ4FLIB_STATIC_API
pure nothrow @safe @nogc
public .LZ4F_errorCodes LZ4F_getErrorCode(const size_t functionResult)

	in
	{
	}

	do
	{
		if (!.LZ4F_isError(functionResult)) {
			return .LZ4F_errorCodes.NoError;
		}

		return cast(.LZ4F_errorCodes)(-(cast(ptrdiff_t)(functionResult)));
	}

pure nothrow @safe @nogc
package size_t err0r(const .LZ4F_errorCodes code)

	in
	{
	}

	do
	{
		/* A compilation error here means ptrdiff_t.sizeof is not large enough */
		static assert(ptrdiff_t.sizeof >= size_t.sizeof);

		return cast(size_t)(-(cast(ptrdiff_t)(code)));
	}

//LZ4FLIB_API
pure nothrow @safe @nogc
public uint LZ4F_getVersion()

	in
	{
	}

	do
	{
		return .LZ4F_VERSION;
	}

//LZ4FLIB_API
pure nothrow @safe @nogc
public int LZ4F_compressionLevel_max()

	in
	{
	}

	do
	{
		static import lz4_lib.lz4hc;

		return lz4_lib.lz4hc.LZ4HC_CLEVEL_MAX;
	}

/*-************************************
*  Private functions
**************************************/
private static immutable size_t[4] blockSizes =
[
	lz4_lib.lz4common.KB!(size_t, 64),
	lz4_lib.lz4common.KB!(size_t, 256),
	lz4_lib.lz4common.MB!(size_t, 1),
	lz4_lib.lz4common.MB!(size_t, 4),
];

//LZ4FLIB_STATIC_API
pure nothrow @safe @nogc
package size_t LZ4F_getBlockSize(uint blockSizeID)

	in
	{
	}

	do
	{
		if (blockSizeID == 0) {
			blockSizeID = .LZ4F_BLOCKSIZEID_DEFAULT;
		}

		if ((blockSizeID > 4) || (blockSizeID > 7)) {
			return .err0r(.LZ4F_errorCodes.maxBlockSize_invalid);
		}

		blockSizeID -= 4;

		return .blockSizes[blockSizeID];
	}

pure nothrow @nogc
package ubyte LZ4F_headerChecksum(const void* header, const size_t length)

	in
	{
	}

	do
	{
		static import lz4_lib.xxhash;

		const uint xxh = lz4_lib.xxhash.XXH32(header, length, 0);

		return cast(ubyte)(xxh >> 8);
	}

/*-************************************
*  Simple-pass compression functions
**************************************/
pure nothrow @safe @nogc
package .LZ4F_blockSizeID_t LZ4F_optimalBSID(const .LZ4F_blockSizeID_t requestedBSID, const size_t srcSize)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4common;

		.LZ4F_blockSizeID_t proposedBSID = .LZ4F_blockSizeID_t.max64KB;
		size_t maxBlockSize = lz4_lib.lz4common.KB!(size_t, 64);

		while (requestedBSID > proposedBSID) {
			if (srcSize <= maxBlockSize) {
				return proposedBSID;
			}

			proposedBSID = cast(.LZ4F_blockSizeID_t)(cast(int)(proposedBSID) + 1);
			maxBlockSize <<= 2;
		}

		return requestedBSID;
	}

/**
 * LZ4F_compressBound_internal() :
 *  Provides dstCapacity given a srcSize to guarantee operation success in worst case situations.
 *  prefsPtr is optional : if null is provided, preferences will be set to cover worst case scenario.
 * @return is always the same for a srcSize and prefsPtr, so it can be relied upon to size reusable buffers.
 *  When srcSize==0, LZ4F_compressBound() provides an upper bound for flush() and compressEnd() operations.
 */
pure nothrow @nogc @system
package size_t LZ4F_compressBound_internal(const size_t srcSize, const .LZ4F_preferences_t* preferencesPtr, const size_t alreadyBuffered)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4common;

		.LZ4F_preferences_t prefsNull;
		//prefsNull = .LZ4F_preferences_t.init;

		/* worst case */
		prefsNull.frameInfo.contentChecksumFlag = .LZ4F_contentChecksum_t.contentChecksumEnabled;

		{
			const .LZ4F_preferences_t* prefsPtr = (preferencesPtr == null) ? (&prefsNull) : (preferencesPtr);
			const uint flush = (*prefsPtr).autoFlush | (srcSize == 0);
			const .LZ4F_blockSizeID_t blockID = (*prefsPtr).frameInfo.blockSizeID;
			const size_t blockSize = .LZ4F_getBlockSize(blockID);
			const size_t maxBuffered = blockSize - 1;
			const size_t bufferedSize = lz4_lib.lz4common.MIN(alreadyBuffered, maxBuffered);
			const size_t maxSrcSize = srcSize + bufferedSize;
			const uint nbFullBlocks = cast(uint)(maxSrcSize / blockSize);
			const size_t partialBlockSize = maxSrcSize & (blockSize - 1);
			const size_t lastBlockSize = (flush) ? (partialBlockSize) : (0);
			const uint nbBlocks = nbFullBlocks + (lastBlockSize > 0);

			const size_t blockHeaderSize = 4;
			const size_t blockCRCSize = 4 * (*prefsPtr).frameInfo.blockChecksumFlag;
			const size_t frameEnd = 4 + ((*prefsPtr).frameInfo.contentChecksumFlag * 4);

			return ((blockHeaderSize + blockCRCSize) * nbBlocks) + (blockSize * nbFullBlocks) + lastBlockSize + frameEnd;
		}
	}

/**
 * LZ4F_compressFrameBound() :
 *  Returns the maximum possible compressed size with LZ4F_compressFrame() given srcSize and preferences.
 * `preferencesPtr` is optional. It can be replaced by null, in which case, the function will assume default preferences.
 *  Note : this result is only usable with LZ4F_compressFrame().
 *         It may also be used with compressUpdate() _if no flush() operation_ is performed.
 */
//LZ4FLIB_API
pure nothrow @nogc
public size_t LZ4F_compressFrameBound(const size_t srcSize, const .LZ4F_preferences_t* preferencesPtr)

	in
	{
	}

	do
	{
		.LZ4F_preferences_t prefs;

		/* max header size, including optional fields */
		const size_t headerSize = maxFHSize;

		if (preferencesPtr != null) {
			prefs = *preferencesPtr;
		} else {
			prefs = .LZ4F_preferences_t.init;
		}

		prefs.autoFlush = 1;

		return headerSize + .LZ4F_compressBound_internal(srcSize, &prefs, 0);
	}

/**
 * LZ4F_compressFrame() :
 *  Compress an entire srcBuffer into a valid LZ4 frame.
 *  dstBuffer MUST be >= LZ4F_compressFrameBound(srcSize, preferencesPtr).
 *  The LZ4F_preferences_t structure is optional : you can provide null as argument. All preferences will be set to default.
 *
 * Returns: number of bytes written into dstBuffer. or an error code if it fails (can be tested using LZ4F_isError())
 */
static if (.LZ4F_HEAPMODE) {
	//LZ4FLIB_API
	@system
	public size_t LZ4F_compressFrame(void* dstBuffer, const size_t dstCapacity, const void* srcBuffer, const size_t srcSize, const .LZ4F_preferences_t* preferencesPtr)

		in
		{
		}

		do
		{
			size_t result;

			.LZ4F_cctx_t* cctxPtr;
			result = .LZ4F_createCompressionContext(&cctxPtr, .LZ4F_VERSION);

			if (.LZ4F_isError(result)) {
				return result;
			}

			result = (*cctxPtr).compressFrame_usingCDict(dstBuffer, dstCapacity, srcBuffer, srcSize, null, preferencesPtr);

			.LZ4F_freeCompressionContext(cctxPtr);

			return result;
		}
} else {
	//LZ4FLIB_API
	@system
	public size_t LZ4F_compressFrame(void* dstBuffer, const size_t dstCapacity, const void* srcBuffer, const size_t srcSize, const .LZ4F_preferences_t* preferencesPtr)

		in
		{
		}

		do
		{
			static import core.stdc.stdlib;
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;
			static import lz4_lib.lz4hc;

			size_t result;

			.LZ4F_cctx_t cctx;
			lz4_lib.lz4.LZ4_stream_t lz4ctx;

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `LZ4F_compressFrame`);
			}

			//cctx = .LZ4F_cctx_t.init;
			cctx.version_Number = .LZ4F_VERSION;

			/* mess with real buffer size to prevent dynamic allocation; works only because autoflush==1 & stableSrc==1 */
			cctx.maxBufferSize = lz4_lib.lz4common.MB!(size_t, 5);

			if (preferencesPtr == null || (*preferencesPtr).compressionLevel < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
				lz4ctx.resetStream();
				cctx.lz4CtxPtr = &lz4ctx;
				cctx.lz4CtxAlloc = 1;
				cctx.lz4CtxState = 1;
			}

			result = cctx.compressFrame_usingCDict(dstBuffer, dstCapacity, srcBuffer, srcSize, null, preferencesPtr);

			if (preferencesPtr != null && (*preferencesPtr).compressionLevel >= lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
				core.stdc.stdlib.free(cctx.lz4CtxPtr);
				cctx.lz4CtxPtr = null;
			}

			return result;
		}
}

/*-***************************************************
*   Dictionary compression
*****************************************************/

/*-*********************************
*  Advanced compression functions
***********************************/

/**
 * LZ4F_createCompressionContext() :
 * The first thing to do is to create a compressionContext object, which will be used in all compression operations.
 * This is achieved using LZ4F_createCompressionContext(), which takes as argument a version.
 * The version provided MUST be LZ4F_VERSION. It is intended to track potential version mismatch, notably when using DLL.
 * The function will provide a pointer to a fully allocated LZ4F_cctx_t object.
 * If @return != zero, there was an error during context creation.
 * Object can release its memory using LZ4F_freeCompressionContext();
 */
//LZ4FLIB_API
nothrow @nogc @system
public size_t LZ4F_createCompressionContext(ref .LZ4F_cctx_t* LZ4F_compressionContextPtr, const uint version_Number)

	in
	{
	}

	do
	{
		static import core.stdc.stdlib;

		.LZ4F_cctx_t* cctxPtr = cast(.LZ4F_cctx_t*)(core.stdc.stdlib.calloc(1, .LZ4F_cctx_t.sizeof));

		if (cctxPtr == null) {
			return .err0r(.LZ4F_errorCodes.allocation_failed);
		}

		(*cctxPtr).version_Number = version_Number;

		/* Next stage : init stream */
		(*cctxPtr).cStage = 0;

		LZ4F_compressionContextPtr = cctxPtr;

		return .LZ4F_errorCodes.NoError;
	}

//LZ4FLIB_API
nothrow @nogc @system
public size_t LZ4F_freeCompressionContext(.LZ4F_cctx_t* LZ4F_compressionContext)

	in
	{
	}

	do
	{
		static import core.stdc.stdlib;

		/* support free on null */
		if (LZ4F_compressionContext != null) {
			.LZ4F_cctx_t* cctxPtr = cast(.LZ4F_cctx_t*)(LZ4F_compressionContext);

			/* works because LZ4_streamHC_t and LZ4_stream_t are simple POD types */
			core.stdc.stdlib.free((*cctxPtr).lz4CtxPtr);
			(*cctxPtr).lz4CtxPtr = null;

			core.stdc.stdlib.free((*cctxPtr).tmpBuff);
			(*cctxPtr).tmpBuff = null;
			core.stdc.stdlib.free(LZ4F_compressionContext);
			LZ4F_compressionContext = null;
		}

		return .LZ4F_errorCodes.NoError;
	}

/**
 * This function prepares the internal LZ4(HC) stream for a new compression,
 * resetting the context and attaching the dictionary, if there is one.
 *
 * It needs to be called at the beginning of each independent compression
 * stream (i.e., at the beginning of a frame in blockLinked mode, or at the
 * beginning of each block in blockIndependent mode).
 */
pure nothrow @nogc
package void LZ4F_initStream(void* ctx, const .LZ4F_CDict* cdict, const int level, const .LZ4F_blockMode_t blockMode)

	in
	{
		assert(ctx != null);
	}

	do
	{
		static import lz4_lib.lz4;
		static import lz4_lib.lz4hc;

		if (level < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
			if (cdict != null || blockMode == .LZ4F_blockMode_t.blockLinked) {
				/*
				 * In these cases, we will call compress_fast_continue(),
				 * which needs an already reset context. Otherwise, we'll call a
				 * one-shot API. The non-continued APIs internally perform their own
				 * resets at the beginning of their calls, where they know what
				 * tableType they need the context to be in. So in that case this
				 * would be misguided / wasted work.
				 */
				(*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).resetStream_fast();
			}

			if (cdict != null) {
				(*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).attach_dictionary((*cdict).fastCtx);
			} else {
				(*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).attach_dictionary(null);
			}
		} else {
			(*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(ctx))).resetStreamHC_fast(level);

			if (cdict != null) {
				(*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(ctx))).attach_HC_dictionary((*cdict).HCCtx);
			} else {
				(*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(ctx))).attach_HC_dictionary(null);
			}
		}
	}

/**
 * LZ4F_compressBound() :
 *  Provides minimum dstCapacity required to guarantee success of
 *  LZ4F_compressUpdate(), given a srcSize and preferences, for a worst case scenario.
 *  When srcSize==0, LZ4F_compressBound() provides an upper bound for LZ4F_flush() and LZ4F_compressEnd() instead.
 *  Note that the result is only valid for a single invocation of LZ4F_compressUpdate().
 *  When invoking LZ4F_compressUpdate() multiple times,
 *  if the output buffer is gradually filled up instead of emptied and re-used from its start,
 *  one must check if there is enough remaining capacity before each invocation, using LZ4F_compressBound().
 * @return is always the same for a srcSize and prefsPtr.
 *  prefsPtr is optional : when NULL is provided, preferences will be set to cover worst case scenario.
 *  tech details :
 * @return includes the possibility that internal buffer might already be filled by up to (blockSize-1) bytes.
 *  It also includes frame footer (ending + checksum), since it might be generated by LZ4F_compressEnd().
 * @return doesn't include frame header, as it was already generated by LZ4F_compressBegin().
 */
/**
 * LZ4F_compressBound() :
 * @return minimum capacity of dstBuffer for a given srcSize to handle worst case scenario.
 *  LZ4F_preferences_t structure is optional : if null, preferences will be set to cover worst case scenario.
 *  This function cannot fail.
 */
//LZ4FLIB_API
pure nothrow @nogc
public size_t LZ4F_compressBound(const size_t srcSize, const .LZ4F_preferences_t* preferencesPtr)

	in
	{
	}

	do
	{
		return .LZ4F_compressBound_internal(srcSize, preferencesPtr, size_t.max);
	}

package alias compressFunc_t = int function(void* ctx, const (char)* src, char* dst, const int srcSize, const int dstSize, const int level, const .LZ4F_CDict* cdict);

/**
 * LZ4F_makeBlock():
 *  compress a single block, add header and checksum
 *  assumption : dst buffer capacity is >= srcSize
 */
@system
package size_t LZ4F_makeBlock(void* dst, const void* src, const size_t srcSize, const .compressFunc_t compress, void* lz4ctx, const int level, const .LZ4F_CDict* cdict, const .LZ4F_blockChecksum_t crcFlag)

	in
	{
		assert(dst != null);
		assert(src != null);
		assert(lz4ctx != null);
	}

	do
	{
		static import core.stdc.string;
		static import lz4_lib.xxhash;

		ubyte* cSizePtr = cast(ubyte*)(dst);
		uint cSize = cast(uint)(compress(lz4ctx, cast(const (char)*)(src), cast(char*)(cSizePtr + 4), cast(int)(srcSize), cast(int)(srcSize - 1), level, cdict));
		.writeLE32(cSizePtr, cSize);

		/* compression failed */
		if (cSize == 0) {
			cSize = cast(uint)(srcSize);
			.writeLE32(cSizePtr, cSize | .LZ4F_BLOCKUNCOMPRESSED_FLAG);
			core.stdc.string.memcpy(cSizePtr + 4, src, srcSize);
		}

		if (crcFlag) {
			/* checksum of compressed data */
			const uint crc32 = lz4_lib.xxhash.XXH32(cSizePtr + 4, cSize, 0);

			.writeLE32(cSizePtr + 4 + cSize, crc32);
		}

		return 4 + cSize + (cast(uint)(crcFlag)) * 4;
	}

pure nothrow @nogc
package int LZ4F_compressBlock(void* ctx, const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int level, const .LZ4F_CDict* cdict)

	in
	{
		assert(ctx != null);
	}

	do
	{
		static import lz4_lib.lz4;

		const int acceleration = (level < 0) ? (-level + 1) : (1);
		.LZ4F_initStream(ctx, cdict, level, .LZ4F_blockMode_t.blockIndependent);

		if (cdict) {
			return (*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).compress_fast_continue(src, dst, srcSize, dstCapacity, acceleration);
		} else {
			return (*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).compress_fast_extState_fastReset(src, dst, srcSize, dstCapacity, acceleration);
		}
	}

pure nothrow @nogc
package int LZ4F_compressBlock_continue(void* ctx, const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int level, const .LZ4F_CDict* cdict)

	in
	{
		assert(ctx != null);
	}

	do
	{
		static import lz4_lib.lz4;

		const int acceleration = (level < 0) ? (-level + 1) : (1);

		/* init once at beginning of frame */
		//(void)cdict;

		return (*(cast(lz4_lib.lz4.LZ4_stream_t*)(ctx))).compress_fast_continue(src, dst, srcSize, dstCapacity, acceleration);
	}

pure nothrow @nogc
package int LZ4F_compressBlockHC(void* ctx, const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int level, const .LZ4F_CDict* cdict)

	in
	{
		assert(ctx != null);
	}

	do
	{
		static import lz4_lib.lz4hc;

		.LZ4F_initStream(ctx, cdict, level, .LZ4F_blockMode_t.blockIndependent);

		if (cdict) {
			return (*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(ctx))).compress_HC_continue(src, dst, srcSize, dstCapacity);
		}

		return lz4_lib.lz4hc.LZ4_compress_HC_extStateHC_fastReset(cast(void*)(ctx), src, dst, srcSize, dstCapacity, level);
	}

pure nothrow @nogc
package int LZ4F_compressBlockHC_continue(void* ctx, const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int level, const .LZ4F_CDict* cdict)

	in
	{
		assert(ctx != null);
	}

	do
	{
		static import lz4_lib.lz4hc;

		//(void)level;

		/* init once at beginning of frame */
		//(void)cdict;

		return (*(cast(lz4_lib.lz4hc.LZ4_streamHC_t*)(ctx))).compress_HC_continue(src, dst, srcSize, dstCapacity);
	}

@system
package .compressFunc_t LZ4F_selectCompression(.LZ4F_blockMode_t blockMode, const int level)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4hc;

		if (level < lz4_lib.lz4hc.LZ4HC_CLEVEL_MIN) {
			if (blockMode == .LZ4F_blockMode_t.blockIndependent) {
				return &(.LZ4F_compressBlock);
			}

			return &(.LZ4F_compressBlock_continue);
		}

		if (blockMode == .LZ4F_blockMode_t.blockIndependent) {
			return &(.LZ4F_compressBlockHC);
		}

		return &(.LZ4F_compressBlockHC_continue);
	}

enum LZ4F_lastBlockStatus
{
	notDone,
	fromTmpBuffer,
	fromSrcBuffer,
}

/*-***************************************************
*   Frame Decompression
*****************************************************/
/**
 * LZ4F_createDecompressionContext() :
 *  Create an LZ4F_dctx object, to track all decompression operations.
 *  The version provided MUST be LZ4F_VERSION.
 *  The function provides a pointer to an allocated and initialized LZ4F_dctx object.
 *  The result is an errorCode, which can be tested using LZ4F_isError().
 *  dctx memory can be released using LZ4F_freeDecompressionContext();
 *  Result of LZ4F_freeDecompressionContext() indicates current state of decompressionContext when being released.
 *  That is, it should be == 0 if decompression has been completed fully and correctly.
 */
/**
 * LZ4F_createDecompressionContext() :
 *  Create a decompressionContext object, which will track all decompression operations.
 *  Provides a pointer to a fully allocated and initialized LZ4F_decompressionContext object.
 *  Object can later be released using LZ4F_freeDecompressionContext().
 *
 * Returns: if != 0, there was an error during context creation.
 */
//LZ4FLIB_API
nothrow @nogc @system
public size_t LZ4F_createDecompressionContext(ref .LZ4F_dctx* LZ4F_decompressionContextPtr, const uint version_Number)

	in
	{
	}

	do
	{
		static import core.stdc.stdlib;

		.LZ4F_dctx* dctx = cast(.LZ4F_dctx*)(core.stdc.stdlib.calloc(1, .LZ4F_dctx.sizeof));

		if (dctx == null) {
			return .err0r(.LZ4F_errorCodes.GENERIC);
		}

		(*dctx).version_Number = version_Number;
		LZ4F_decompressionContextPtr = dctx;

		return .LZ4F_errorCodes.NoError;
	}

//LZ4FLIB_API
nothrow @nogc @system
public size_t LZ4F_freeDecompressionContext(.LZ4F_dctx* dctx)

	in
	{
	}

	do
	{
		static import core.stdc.stdlib;

		size_t result = .LZ4F_errorCodes.NoError;

		/* can accept null input, like free() */
		if (dctx != null) {
			result = cast(size_t)((*dctx).dStage);
			core.stdc.stdlib.free((*dctx).tmpIn);
			core.stdc.stdlib.free((*dctx).tmpOutBuffer);
			core.stdc.stdlib.free(dctx);
			(*dctx).tmpIn = null;
			(*dctx).tmpOutBuffer = null;
			dctx = null;
		}

		return result;
	}

/*==---   Streaming Decompression operations   ---==*/

/**
 * LZ4F_headerSize() :
 *
 * Returns: size of frame header or an error code, which can be tested using LZ4F_isError()
 */
pure nothrow @nogc
package size_t LZ4F_headerSize(const void* src, const size_t srcSize)

	in
	{
		assert(src != null);
	}

	do
	{
		/* minimal srcSize to determine header size */
		if (srcSize < 5) {
			return .err0r(.LZ4F_errorCodes.frameHeader_incomplete);
		}

		/* special case : skippable frames */
		if ((.readLE32(src) & 0xFFFFFFF0u) == .LZ4F_MAGIC_SKIPPABLE_START) {
			return 8;
		}

		/* control magic number */
		if (.readLE32(src) != .LZ4F_MAGICNUMBER) {
			return .err0r(.LZ4F_errorCodes.frameType_unknown);
		}

		/* Frame Header Size */
		{
			const ubyte FLG = (cast(const (ubyte)*)(src))[4];
			const uint contentSizeFlag = (FLG >> 3) & ._1BIT;
			const uint dictIDFlag = FLG & ._1BIT;

			return .minFHSize + (contentSizeFlag * 8) + (dictIDFlag * 4);
		}
	}
