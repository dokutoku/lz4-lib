/**
 *
 *
 * License: BSD 2-Clause License
 */
/*
    LZ4 HC - High Compression Mode of LZ4
    Copyright (C) 2011-2017, Yann Collet.

    BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following disclaimer
    in the documentation and/or other materials provided with the
    distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    You can contact the author at :
       - LZ4 source repository : https://github.com/lz4/lz4
       - LZ4 public forum : https://groups.google.com/forum/#!forum/lz4c
*/
module lz4_lib.lz4hc;


/*-************************************
*  Dependency
**************************************/
private static import core.stdc.stdlib;
private static import core.stdc.string;
private static import lz4_lib.lz4common;
private static import lz4_lib.lz4;

/*-************************************
*  Useful constants
**************************************/

enum LZ4HC_CLEVEL_MIN = 3;
enum LZ4HC_CLEVEL_DEFAULT = 9;
enum LZ4HC_CLEVEL_OPT_MIN = 10;
enum LZ4HC_CLEVEL_MAX = 12;


/*===   Enums   ===*/
enum dictCtx_directive
{
	noDictCtx,
	usingDictCtxHc,
}

enum limitedOutput_directive
{
	noLimit = 0,
	limitedOutput = 1,
	limitedDestSize = 2,
}

/*-************************************
*  Constants
**************************************/
enum int OPTIMAL_ML = (lz4_lib.lz4.ML_MASK - 1) + lz4_lib.lz4.MINMATCH;
enum LZ4_OPT_NUM = 1 << 12;


/*-******************************************************************
 * PRIVATE DEFINITIONS :
 * Do not use these definitions directly.
 * They are merely exposed to allow static allocation of `LZ4_streamHC_t`.
 * Declare an `LZ4_streamHC_t` directly, rather than any type below.
 * Even then, only do so in the context of static linking, as definitions may change between versions.
 ********************************************************************/
/*
 * These functions compress data in successive blocks of any size, using previous blocks as dictionary.
 * One key assumption is that previous blocks (up to 64 KB) remain read-accessible while compressing next blocks.
 * There is an exception for ring buffers, which can be smaller than 64 KB.
 * Ring buffers scenario is automatically detected and handled by compress_HC_continue().

 * Before starting compression, state must be properly initialized, using resetStreamHC().
 * A first "fictional block" can then be designated as initial dictionary, using loadDictHC() (Optional).

 * Then, use compress_HC_continue() to compress each successive block.
 * Previous memory blocks (including initial dictionary when present) must remain accessible and unmodified during compression.
 * 'dst' buffer should be sized to handle worst case scenarios (see LZ4_compressBound()), to ensure operation success.
 * Because in case of failure, the API does not guarantee context recovery, and context will have to be reset.
 * If `dst` buffer budget cannot be >= LZ4_compressBound(), consider using compress_HC_continue_destSize() instead.

 * If, for any reason, previous data block can't be preserved unmodified in memory for next compression block,
 * you can save it to a more stable memory space, using saveDictHC().
 * Return value of saveDictHC() is the size of dictionary effectively saved into 'safeBuffer'.
 */
enum LZ4HC_DICTIONARY_LOGSIZE = 16;
enum LZ4HC_MAXD = 1 << .LZ4HC_DICTIONARY_LOGSIZE;
enum LZ4HC_MAXD_MASK = .LZ4HC_MAXD - 1;

enum LZ4HC_HASH_LOG = 15;
enum LZ4HC_HASHTABLESIZE = 1 << .LZ4HC_HASH_LOG;
enum LZ4HC_HASH_MASK = .LZ4HC_HASHTABLESIZE - 1;

package struct LZ4HC_CCtx_internal
{
	uint[.LZ4HC_HASHTABLESIZE] hashTable;
	ushort[.LZ4HC_MAXD] chainTable;

	/**
	 * next block here to continue on current prefix
	 */
	const (ubyte)* end = cast(const (ubyte)*)(ptrdiff_t.max);

	/**
	 * All index relative to this position
	 */
	const (ubyte)* base = null;

	/**
	 * alternate base for extDict
	 */
	const (ubyte)* dictBase = null;

	/**
	 * below that point, need extDict
	 */
	uint dictLimit;

	/**
	 * below that point, no more dict
	 */
	uint lowLimit;

	/**
	 * index from which to continue dictionary update
	 */
	uint nextToUpdate;

	short compressionLevel = .LZ4HC_CLEVEL_DEFAULT;

	/**
	 * favor decompression speed if this flag set, otherwise, favor compression ratio
	 */
	short favorDecSpeed = 0;

	/**
	 * stream has to be fully reset if this flag is set
	 */
	short dirty;

	const (.LZ4HC_CCtx_internal)* dictCtx = null;

	invariant
	{
	}

package:
	pure nothrow @safe @nogc
	void clearTables()

		in
		{
		}

		do
		{
			//init
			this.hashTable[] = 0;
			this.chainTable[] = 0xFF;
		}

	pure nothrow @nogc
	void init(const ubyte* start)

		in
		{
			assert(start != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			size_t startingOffset = this.end - this.base;

			if (startingOffset > lz4_lib.lz4common.GB!(size_t, 1)) {
				this.clearTables();
				startingOffset = 0;
			}

			startingOffset += lz4_lib.lz4common.KB!(size_t, 64);
			this.nextToUpdate = cast(uint)(startingOffset);
			this.base = start - startingOffset;
			this.end = start;
			this.dictBase = start - startingOffset;
			this.dictLimit = cast(uint)(startingOffset);
			this.lowLimit = cast(uint)(startingOffset);
		}

	/**
	 * Update chains up to ip (excluded)
	 */
	pragma(inline, true)
	pure nothrow @nogc
	void Insert(const (ubyte)* ip)

		in
		{
			assert(ip != null);
		}

		do
		{
			static import lz4_lib.lz4;

			const ubyte* base = this.base;
			const uint target = cast(uint)(ip - base);
			uint idx = this.nextToUpdate;

			while (idx < target) {
				const uint h = .LZ4HC_hashPtr(base + idx);
				size_t delta = idx - this.hashTable[h];

				if (delta > lz4_lib.lz4.MAX_DISTANCE) {
					delta = lz4_lib.lz4.MAX_DISTANCE;
				}

				this.chainTable[idx] = cast(ushort)(delta);
				this.hashTable[h] = idx;
				idx++;
			}

			this.nextToUpdate = target;
		}

	pragma(inline, true)
	pure nothrow @nogc
	int InsertAndGetWiderMatch(const ubyte* ip, const ubyte* iLowLimit, const ubyte* iHighLimit, int longest, const (ubyte)** matchpos, const (ubyte)** startpos, const int maxNbAttempts, const int patternAnalysis, const int chainSwap, const .dictCtx_directive dict, const .HCfavor_e favorDecSpeed)

		in
		{
			assert(ip != null);
			assert(iLowLimit != null);
			assert(iHighLimit != null);
			assert(matchpos != null);
			assert(startpos != null);
		}

		do
		{
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;

			const .LZ4HC_CCtx_internal* dictCtx = this.dictCtx;
			const ubyte* base = this.base;
			const uint dictLimit = this.dictLimit;
			const ubyte* lowPrefixPtr = base + dictLimit;
			const uint ipIndex = cast(uint)(ip - base);
			const uint lowestMatchIndex = (this.lowLimit + lz4_lib.lz4common.KB!(uint, 64) > ipIndex) ? (this.lowLimit) : (ipIndex - lz4_lib.lz4.MAX_DISTANCE);
			const ubyte* dictBase = this.dictBase;
			const int lookBackLength = cast(int)(ip - iLowLimit);
			int nbAttempts = maxNbAttempts;
			int matchChainPos = 0;
			const uint pattern = lz4_lib.lz4common.read32(ip);
			uint matchIndex;
			.repeat_state_e repeat = .repeat_state_e.untested;
			size_t srcPatternLength = 0;

			debug (7) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `InsertAndGetWiderMatch`);
			}

			/* First Match */
			this.Insert(ip);
			matchIndex = this.hashTable[.LZ4HC_hashPtr(ip)];

			debug(7) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `First match at index %u / %u (lowestMatchIndex)`, matchIndex, lowestMatchIndex);
			}

			while ((matchIndex >= lowestMatchIndex) && (nbAttempts)) {
				int matchLength = 0;
				nbAttempts--;
				assert(matchIndex < ipIndex);

				if (favorDecSpeed && (ipIndex - matchIndex < 8)) {
					/* do nothing */
				} else if (matchIndex >= dictLimit) {   /* within current Prefix */
					const ubyte* matchPtr = base + matchIndex;
					assert(matchPtr >= lowPrefixPtr);
					assert(matchPtr < ip);
					assert(longest >= 1);

					if (lz4_lib.lz4common.read16(iLowLimit + longest - 1) == lz4_lib.lz4common.read16(matchPtr - lookBackLength + longest - 1)) {
						if (lz4_lib.lz4common.read32(matchPtr) == pattern) {
							const int back = (lookBackLength) ? (.LZ4HC_countBack(ip, matchPtr, iLowLimit, lowPrefixPtr)) : (0);
							matchLength = cast(int)(lz4_lib.lz4.MINMATCH + lz4_lib.lz4.LZ4_count(ip + lz4_lib.lz4.MINMATCH, matchPtr + lz4_lib.lz4.MINMATCH, iHighLimit));
							matchLength -= back;

							if (matchLength > longest) {
								longest = matchLength;
								*matchpos = matchPtr + back;
								*startpos = ip + back;
							}
						}
					}
				} else {   /* lowestMatchIndex <= matchIndex < dictLimit */
					const ubyte* matchPtr = dictBase + matchIndex;

					if (lz4_lib.lz4common.read32(matchPtr) == pattern) {
						const ubyte* dictStart = dictBase + this.lowLimit;
						int back = 0;
						const (ubyte)* vLimit = ip + (dictLimit - matchIndex);

						if (vLimit > iHighLimit) {
							vLimit = iHighLimit;
						}

						matchLength = cast(int)(lz4_lib.lz4.LZ4_count(ip + lz4_lib.lz4.MINMATCH, matchPtr + lz4_lib.lz4.MINMATCH, vLimit) + lz4_lib.lz4.MINMATCH);

						if ((ip + matchLength == vLimit) && (vLimit < iHighLimit)) {
							matchLength += lz4_lib.lz4.LZ4_count(ip + matchLength, lowPrefixPtr, iHighLimit);
						}

						back = (lookBackLength) ? (.LZ4HC_countBack(ip, matchPtr, iLowLimit, dictStart)) : (0);
						matchLength -= back;

						if (matchLength > longest) {
							longest = matchLength;

							/* virtual pos, relative to ip, to retrieve offset */
							*matchpos = base + matchIndex + back;

							*startpos = ip + back;
						}
					}
				}

				if (chainSwap && matchLength == longest) {  /* better match => select a better chain */

					/* search forward only */
					assert(lookBackLength == 0);

					if (matchIndex + longest <= ipIndex) {
						uint distanceToNextMatch = 1;
						int pos;

						for (pos = 0; pos <= longest - lz4_lib.lz4.MINMATCH; pos++) {
							const uint candidateDist = .DELTANEXTU16(this.chainTable, matchIndex + pos);

							if (candidateDist > distanceToNextMatch) {
								distanceToNextMatch = candidateDist;
								matchChainPos = pos;
							}
						}

						if (distanceToNextMatch > 1) {
							if (distanceToNextMatch > matchIndex) {
								/* avoid overflow */
								break;
							}

							matchIndex -= distanceToNextMatch;
							continue;
						}
					}
				}

				{
					const uint distNextMatch = .DELTANEXTU16(this.chainTable, matchIndex);

					if (patternAnalysis && distNextMatch == 1 && matchChainPos == 0) {
						const uint matchCandidateIdx = matchIndex - 1;

						/* may be a repeated pattern */
						if (repeat == .repeat_state_e.untested) {
							if (((pattern & 0xFFFF) == (pattern >> 16)) & ((pattern & 0xFF) == (pattern >> 24))) {
								repeat = .repeat_state_e.confirmed;
								srcPatternLength = .LZ4HC_countPattern(ip + pattern.sizeof, iHighLimit, pattern) + pattern.sizeof;
							} else {
								repeat = .repeat_state_e.not;
							}
						}

						if ((repeat == .repeat_state_e.confirmed) && (matchCandidateIdx >= dictLimit)) {   /* same segment only */
							const ubyte* matchPtr = base + matchCandidateIdx;

							if (lz4_lib.lz4common.read32(matchPtr) == pattern) {  /* good candidate */
								const size_t forwardPatternLength = .LZ4HC_countPattern(matchPtr + pattern.sizeof, iHighLimit, pattern) + pattern.sizeof;
								const ubyte* lowestMatchPtr = (lowPrefixPtr + lz4_lib.lz4.MAX_DISTANCE >= ip) ? (lowPrefixPtr) : (ip - lz4_lib.lz4.MAX_DISTANCE);
								const size_t backLength = .LZ4HC_reverseCountPattern(matchPtr, lowestMatchPtr, pattern);
								const size_t currentSegmentLength = backLength + forwardPatternLength;

								if ((currentSegmentLength >= srcPatternLength) && (forwardPatternLength <= srcPatternLength)) {
									/* best position, full pattern, might be followed by more match */
									matchIndex = matchCandidateIdx + cast(uint)(forwardPatternLength) - cast(uint)(srcPatternLength);
								} else {
									/* farthest position in current segment, will find a match of length currentSegmentLength + maybe some back */
									matchIndex = matchCandidateIdx - cast(uint)(backLength);

									if (lookBackLength == 0) { /* no back possible */
										const size_t maxML = lz4_lib.lz4common.MIN(currentSegmentLength, srcPatternLength);

										if (cast(size_t)(longest) < maxML) {
											assert((base + matchIndex) < ip);

											if ((ip - (base+matchIndex)) > lz4_lib.lz4.MAX_DISTANCE) {
												break;
											}

											assert(maxML < lz4_lib.lz4common.GB!(size_t, 2));
											longest = cast(int)(maxML);

											/* virtual pos, relative to ip, to retrieve offset */
											*matchpos = base + matchIndex;

											*startpos = ip;
										}

										{
											const uint distToNextPattern = .DELTANEXTU16(this.chainTable, matchIndex);

											if (distToNextPattern > matchIndex) {
												/* avoid overflow */
												break;
											}

											matchIndex -= distToNextPattern;
										}
									}
								}

								continue;
							}
						}
					}
				}   /* PA optimization */

				/* follow current chain */
				matchIndex -= .DELTANEXTU16(this.chainTable, matchIndex + matchChainPos);
			}  /* while ((matchIndex>=lowestMatchIndex) && (nbAttempts)) */

			if (dict == .dictCtx_directive.usingDictCtxHc && nbAttempts && ipIndex - lowestMatchIndex < lz4_lib.lz4.MAX_DISTANCE) {
				const size_t dictEndOffset = (*dictCtx).end - (*dictCtx).base;
				uint dictMatchIndex = (*dictCtx).hashTable[.LZ4HC_hashPtr(ip)];
				assert(dictEndOffset <= lz4_lib.lz4common.GB!(size_t, 1));
				matchIndex = dictMatchIndex + lowestMatchIndex - cast(uint)(dictEndOffset);

				while (ipIndex - matchIndex <= lz4_lib.lz4.MAX_DISTANCE && nbAttempts--) {
					const ubyte* matchPtr = (*dictCtx).base + dictMatchIndex;

					if (lz4_lib.lz4common.read32(matchPtr) == pattern) {
						int mlt;
						int back = 0;
						const (ubyte)* vLimit = ip + (dictEndOffset - dictMatchIndex);

						if (vLimit > iHighLimit) {
							vLimit = iHighLimit;
						}

						mlt = cast(int)(lz4_lib.lz4.LZ4_count(ip + lz4_lib.lz4.MINMATCH, matchPtr + lz4_lib.lz4.MINMATCH, vLimit) + lz4_lib.lz4.MINMATCH);
						back = (lookBackLength) ? (.LZ4HC_countBack(ip, matchPtr, iLowLimit, (*dictCtx).base + (*dictCtx).dictLimit)) : (0);
						mlt -= back;

						if (mlt > longest) {
							longest = mlt;
							*matchpos = base + matchIndex + back;
							*startpos = ip + back;
						}
					}

					{
						const uint nextOffset = .DELTANEXTU16((*dictCtx).chainTable, dictMatchIndex);
						dictMatchIndex -= nextOffset;
						matchIndex -= nextOffset;
					}
				}
			}

			return longest;
		}

	pragma(inline, true)
	pure nothrow @nogc
	int InsertAndFindBestMatch(const ubyte* ip, const ubyte* iLimit, const (ubyte)** matchpos, const int maxNbAttempts, const int patternAnalysis, const .dictCtx_directive dict)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4;

			const (ubyte)* uselessPtr = ip;
			/*
			 * note : InsertAndGetWiderMatch() is able to modify the starting position of a match (*startpos),
			 * but this won't be the case here, as we define iLowLimit==ip,
			 * so InsertAndGetWiderMatch() won't be allowed to search past ip
			 */

			return this.InsertAndGetWiderMatch(ip, ip, iLimit, lz4_lib.lz4.MINMATCH - 1, matchpos, &uselessPtr, maxNbAttempts, patternAnalysis, 0 /*chainSwap*/, dict, .HCfavor_e.favorCompressionRatio);
		}

	pragma(inline, true)
	pure nothrow @nogc @system
	int compress_hashChain(const char* source, char* dest, ref int srcSizePtr, const int maxOutputSize, uint maxNbAttempts, const .limitedOutput_directive limit, const .dictCtx_directive dict)

		in
		{
			assert(source != null);
			assert(dest != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4;

			const int inputSize = srcSizePtr;

			/* levels 9+ */
			const int patternAnalysis = (maxNbAttempts > 128);

			const (ubyte)* ip = cast(const (ubyte)*)(source);
			const (ubyte)* anchor = ip;
			const ubyte* iend = ip + inputSize;
			const ubyte* mflimit = iend - lz4_lib.lz4.MFLIMIT;
			const ubyte* matchlimit = (iend - lz4_lib.lz4.LASTLITERALS);

			ubyte* optr = cast(ubyte*)(dest);
			ubyte* op = cast(ubyte*)(dest);
			ubyte* oend = op + maxOutputSize;

			int ml0;
			int ml;
			int ml2;
			int ml3;
			const (ubyte)* start0 = null;
			const (ubyte)* ref0 = null;
			const (ubyte)* ref_p = null;
			const (ubyte)* start2 = null;
			const (ubyte)* ref2 = null;
			const (ubyte)* start3 = null;
			const (ubyte)* ref3 = null;

			/* init */
			srcSizePtr = 0;

			if (limit == .limitedOutput_directive.limitedDestSize) {
				/* Hack for support LZ4 format restriction */
				oend -= lz4_lib.lz4.LASTLITERALS;
			}

			if (inputSize < lz4_lib.lz4.LZ4_minLength) {
				/* Input too small, no compression (all literals) */
				goto _last_literals;
			}

			/* Main Loop */
			while (ip <= mflimit) {
				ml = this.InsertAndFindBestMatch(ip, matchlimit, &ref_p, maxNbAttempts, patternAnalysis, dict);

				if (ml < lz4_lib.lz4.MINMATCH) {
					ip++;
					continue;
				}

				/* saved, in case we would skip too much */
				start0 = ip;
				ref0 = ref_p;
				ml0 = ml;

		_Search2:

				if (ip + ml <= mflimit) {
					ml2 = this.InsertAndGetWiderMatch(ip + ml - 2, ip + 0, matchlimit, ml, &ref2, &start2, maxNbAttempts, patternAnalysis, 0, dict, .HCfavor_e.favorCompressionRatio);
				} else {
					ml2 = ml;
				}

				if (ml2 == ml) { /* No better match => encode ML1 */
					optr = op;

					if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref_p, limit, oend)) {
						goto _dest_overflow;
					}

					continue;
				}

				if (start0 < ip) {   /* first match was skipped at least once */
					if (start2 < ip + ml0) {  /* squeezing ML1 between ML0(original ML1) and ML2 */
						ip = start0;
						ref_p = ref0;

						/* restore initial ML1 */
						ml = ml0;
					}
				}

				/* Here, start0==ip */
				if ((start2 - ip) < 3) {  /* First Match too small : removed */
					ml = ml2;
					ip = start2;
					ref_p = ref2;
					goto _Search2;
				}

		_Search3:

				/*
				 * At this stage, we have :
				 *  ml2 > ml1, and
				 *  ip1+3 <= ip2 (usually < ip1+ml1)
				 */
				if ((start2 - ip) < .OPTIMAL_ML) {
					int correction;
					int new_ml = ml;

					if (new_ml > .OPTIMAL_ML) {
						new_ml = .OPTIMAL_ML;
					}

					if (ip + new_ml > start2 + ml2 - lz4_lib.lz4.MINMATCH) {
						new_ml = cast(int)(start2 - ip) + ml2 - lz4_lib.lz4.MINMATCH;
					}

					correction = new_ml - cast(int)(start2 - ip);

					if (correction > 0) {
						start2 += correction;
						ref2 += correction;
						ml2 -= correction;
					}
				}

				/* Now, we have start2 = ip+new_ml, with new_ml = min(ml, OPTIMAL_ML=18) */

				if (start2 + ml2 <= mflimit) {
					ml3 = this.InsertAndGetWiderMatch(start2 + ml2 - 3, start2, matchlimit, ml2, &ref3, &start3, maxNbAttempts, patternAnalysis, 0, dict, .HCfavor_e.favorCompressionRatio);
				} else {
					ml3 = ml2;
				}

				if (ml3 == ml2) {  /* No better match => encode ML1 and ML2 */
					/* ip & ref_p are known; Now for ml */
					if (start2 < ip + ml) {
						ml = cast(int)(start2 - ip);
					}

					/* Now, encode 2 sequences */
					optr = op;

					if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref_p, limit, oend)) {
						goto _dest_overflow;
					}

					ip = start2;
					optr = op;

					if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml2, ref2, limit, oend)) {
						goto _dest_overflow;
					}

					continue;
				}

				if (start3 < ip + ml + 3) { /* Not enough space for match 2 : remove it */
					if (start3 >= (ip + ml)) { /* can write Seq1 immediately ==> Seq2 is removed, so Seq3 becomes Seq1 */
						if (start2 < ip + ml) {
							int correction = cast(int)(ip + ml - start2);
							start2 += correction;
							ref2 += correction;
							ml2 -= correction;

							if (ml2 < lz4_lib.lz4.MINMATCH) {
								start2 = start3;
								ref2 = ref3;
								ml2 = ml3;
							}
						}

						optr = op;

						if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref_p, limit, oend)) {
							goto _dest_overflow;
						}

						ip = start3;
						ref_p = ref3;
						ml = ml3;

						start0 = start2;
						ref0 = ref2;
						ml0 = ml2;
						goto _Search2;
					}

					start2 = start3;
					ref2 = ref3;
					ml2 = ml3;
					goto _Search3;
				}

				/*
				 * OK, now we have 3 ascending matches;
				 * let's write the first one ML1.
				 * ip & ref_p are known; Now decide ml.
				 */
				if (start2 < ip + ml) {
					if ((start2 - ip) < .OPTIMAL_ML) {
						int correction;

						if (ml > .OPTIMAL_ML) {
							ml = .OPTIMAL_ML;
						}

						if (ip + ml > start2 + ml2 - lz4_lib.lz4.MINMATCH) {
							ml = cast(int)(start2 - ip) + ml2 - lz4_lib.lz4.MINMATCH;
						}

						correction = ml - cast(int)(start2 - ip);

						if (correction > 0) {
							start2 += correction;
							ref2 += correction;
							ml2 -= correction;
						}
					} else {
						ml = cast(int)(start2 - ip);
					}
				}

				optr = op;

				if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ref_p, limit, oend)) {
					goto _dest_overflow;
				}

				/* ML2 becomes ML1 */
				ip = start2;
				ref_p = ref2;
				ml = ml2;

				/* ML3 becomes ML2 */
				start2 = start3;
				ref2 = ref3;
				ml2 = ml3;

				/* let's find a new ML3 */
				goto _Search3;
			}

		_last_literals:
			/* Encode Last Literals */
			{
				/* literals */
				size_t lastRunSize = cast(size_t)(iend - anchor);

				size_t litLength = (lastRunSize + 255 - lz4_lib.lz4.RUN_MASK) / 255;
				const size_t totalSize = 1 + litLength + lastRunSize;

				if (limit == .limitedOutput_directive.limitedDestSize) {
					/* restore correct value */
					oend += lz4_lib.lz4.LASTLITERALS;
				}

				if (limit && (op + totalSize > oend)) {
					if (limit == .limitedOutput_directive.limitedOutput) {
						/* Check output limit */
						return 0;
					}

					/* adapt lastRunSize to fill 'dest' */
					lastRunSize = cast(size_t)(oend - op) - 1;
					litLength = (lastRunSize + 255 - lz4_lib.lz4.RUN_MASK) / 255;
					lastRunSize -= litLength;
				}

				ip = anchor + lastRunSize;

				if (lastRunSize >= lz4_lib.lz4.RUN_MASK) {
					size_t accumulator = lastRunSize - lz4_lib.lz4.RUN_MASK;
					*op++ = (lz4_lib.lz4.RUN_MASK << lz4_lib.lz4.ML_BITS);

					for (; accumulator >= 255 ; accumulator -= 255) {
						*op++ = 255;
					}

					*op++ = cast(ubyte)(accumulator);
				} else {
					*op++ = cast(ubyte)(lastRunSize << lz4_lib.lz4.ML_BITS);
				}

				core.stdc.string.memcpy(op, anchor, lastRunSize);
				op += lastRunSize;
			}

			/* End */
			srcSizePtr = cast(int)((cast(const (char)*)(ip)) - source);

			return cast(int)((cast(char*)(op)) - dest);

		_dest_overflow:

			if (limit == .limitedOutput_directive.limitedDestSize) {
				/* restore correct out pointer */
				op = optr;

				goto _last_literals;
			}

			return 0;
		}

	pragma(inline, true)
	pure nothrow @nogc
	int compress_generic_internal(const char* src, char* dst, ref int srcSizePtr, const int dstCapacity, int cLevel, const .limitedOutput_directive limit, const .dictCtx_directive dict)

		in
		{
			assert(src != null);
		}

		do
		{
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_generic(%X, %X, %d)`, &this, src, srcSizePtr);
			}

			if (limit == .limitedOutput_directive.limitedDestSize && dstCapacity < 1) {
				/* Impossible to store anything */
				return 0;
			}

			if (cast(uint)(srcSizePtr) > cast(uint)(lz4_lib.lz4.LZ4_MAX_INPUT_SIZE)) {
				/* Unsupported input size (too large or negative) */
				return 0;
			}

			this.end += srcSizePtr;

			if (cLevel < 1) {
				/* note : convention is different from lz4frame, maybe something to review */
				cLevel = .LZ4HC_CLEVEL_DEFAULT;
			}

			cLevel = lz4_lib.lz4common.MIN(.LZ4HC_CLEVEL_MAX, cLevel);

			{
				const .cParams_t cParam = .clTable[cLevel];
				const .HCfavor_e favor = (this.favorDecSpeed) ? (.HCfavor_e.favorDecompressionSpeed) : (.HCfavor_e.favorCompressionRatio);
				int result;

				if (cParam.strat == .lz4hc_strat_e.lz4hc) {
					result = this.compress_hashChain(src, dst, srcSizePtr, dstCapacity, cParam.nbSearches, limit, dict);
				} else {
					assert(cParam.strat == .lz4hc_strat_e.lz4opt);
					result = this.compress_optimal(src, dst, srcSizePtr, dstCapacity, cParam.nbSearches, cParam.targetLength, limit, (cLevel == .LZ4HC_CLEVEL_MAX), dict, favor);
				}

				if (result <= 0) {
					this.dirty = 1;
				}

				return result;
			}
		}

	pure nothrow @nogc
	int compress_generic_noDictCtx(const char* src, char* dst, ref int srcSizePtr, const int dstCapacity, const int cLevel, const .limitedOutput_directive limit)

		in
		{
			assert(this.dictCtx == null);
		}

		do
		{
			return this.compress_generic_internal(src, dst, srcSizePtr, dstCapacity, cLevel, limit, .dictCtx_directive.noDictCtx);
		}

	pure nothrow @nogc @system
	int compress_generic_dictCtx(const char* src, char* dst, ref int srcSizePtr, const int dstCapacity, const int cLevel, const .limitedOutput_directive limit)

		in
		{
			assert(this.dictCtx != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			const size_t position = this.end - this.base - this.lowLimit;

			if (position >= lz4_lib.lz4common.KB!(size_t, 64)) {
				this.dictCtx = null;

				return this.compress_generic_noDictCtx(src, dst, srcSizePtr, dstCapacity, cLevel, limit);
			} else if (position == 0 && srcSizePtr > lz4_lib.lz4common.KB!(int, 4)) {
				core.stdc.string.memcpy(&this, this.dictCtx, .LZ4HC_CCtx_internal.sizeof);
				this.setExternalDict(cast(const (ubyte)*)(src));
				this.compressionLevel = cast(short)(cLevel);

				return this.compress_generic_noDictCtx(src, dst, srcSizePtr, dstCapacity, cLevel, limit);
			} else {
				return this.compress_generic_internal(src, dst, srcSizePtr, dstCapacity, cLevel, limit, .dictCtx_directive.usingDictCtxHc);
			}
		}

	pure nothrow @nogc
	int compress_generic(const char* src, char* dst, ref int srcSizePtr, const int dstCapacity, const int cLevel, const .limitedOutput_directive limit)

		in
		{
		}

		do
		{
			if (this.dictCtx == null) {
				return this.compress_generic_noDictCtx(src, dst, srcSizePtr, dstCapacity, cLevel, limit);
			} else {
				return this.compress_generic_dictCtx(src, dst, srcSizePtr, dstCapacity, cLevel, limit);
			}
		}

	pure nothrow @nogc
	void setExternalDict(const (ubyte)* newBlock)

		in
		{
			assert(newBlock != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `setExternalDict(%X, %X)`, &this, newBlock);
			}

			if (this.end >= this.base + this.dictLimit + 4) {
				/* Referencing remaining dictionary content */
				this.Insert(this.end - 3);
			}

			/* Only one memory segment for extDict, so any previous extDict is lost at this stage */
			this.lowLimit = this.dictLimit;
			this.dictLimit = cast(uint)(this.end - this.base);
			this.dictBase = this.base;
			this.base = newBlock - this.dictLimit;
			this.end = newBlock;

			/* match referencing will resume from there */
			this.nextToUpdate = this.dictLimit;
		}

	pragma(inline, true)
	pure nothrow @nogc
	.LZ4HC_match_t FindLongerMatch(const (ubyte)* ip, const ubyte* iHighLimit, const int minLen, const int nbSearches, const .dictCtx_directive dict, const .HCfavor_e favorDecSpeed)

		in
		{
			assert(ip != null);
		}

		do
		{
			.LZ4HC_match_t match = {0, 0};
			const (ubyte)* matchPtr = null;
			/*
			 * note : InsertAndGetWiderMatch() is able to modify the starting position of a match (*startpos),
			 * but this won't be the case here, as we define iLowLimit==ip,
			 * so InsertAndGetWiderMatch() won't be allowed to search past ip
			 */
			int matchLength = this.InsertAndGetWiderMatch(ip, ip, iHighLimit, minLen, &matchPtr, &ip, nbSearches, 1 /*patternAnalysis*/, 1 /*chainSwap*/, dict, favorDecSpeed);

			if (matchLength <= minLen) {
				return match;
			}

			if (favorDecSpeed) {
				if ((matchLength > 18) & (matchLength <= 36)) {
					/* favor shortcut */
					matchLength = 18;
				}
			}

			match.len = matchLength;
			match.off = cast(int)(ip - matchPtr);

			return match;
		}

	pure nothrow @nogc @system
	int compress_optimal(const char* source, char* dst, ref int srcSizePtr, const int dstCapacity, const int nbSearches, size_t sufficient_len, const .limitedOutput_directive limit, const int fullUpdate, const .dictCtx_directive dict, const .HCfavor_e favorDecSpeed)

		in
		{
			assert(source != null);
			assert(dst != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;
			static import lz4_lib.lz4;

			enum TRAILING_LITERALS = 3;

			/* ~64 KB, which is a bit large for stack... */
			.LZ4HC_optimal_t[.LZ4_OPT_NUM + TRAILING_LITERALS] opt;

			const (ubyte)* ip = cast(const (ubyte)*)(source);
			const (ubyte)* anchor = ip;
			const ubyte* iend = ip + srcSizePtr;
			const ubyte* mflimit = iend - lz4_lib.lz4.MFLIMIT;
			const ubyte* matchlimit = iend - lz4_lib.lz4.LASTLITERALS;
			ubyte* op = cast(ubyte*)(dst);
			ubyte* opSaved = cast(ubyte*)(dst);
			ubyte* oend = op + dstCapacity;

			/* init */
			debug (5) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_optimal`);
			}

			srcSizePtr = 0;

			if (limit == .limitedOutput_directive.limitedDestSize) {
				/* Hack for support LZ4 format restriction */
				oend -= lz4_lib.lz4.LASTLITERALS;
			}

			if (sufficient_len >= .LZ4_OPT_NUM) {
				sufficient_len = .LZ4_OPT_NUM - 1;
			}

			/* Main Loop */
			assert(ip - anchor < lz4_lib.lz4.LZ4_MAX_INPUT_SIZE);

			while (ip <= mflimit) {
				const int llen = cast(int)(ip - anchor);
				int best_mlen;
				int best_off;
				int cur;
				int last_match_pos = 0;

				const .LZ4HC_match_t firstMatch = this.FindLongerMatch(ip, matchlimit, lz4_lib.lz4.MINMATCH - 1, nbSearches, dict, favorDecSpeed);

				if (firstMatch.len == 0) {
					ip++;
					continue;
				}

				if (cast(size_t)(firstMatch.len) > sufficient_len) {
					/* good enough solution : immediate encoding */
					const int firstML = firstMatch.len;
					const ubyte* matchPos = ip - firstMatch.off;
					opSaved = op;

					if (.LZ4HC_encodeSequence(&ip, &op, &anchor, firstML, matchPos, limit, oend)) { /* updates ip, op and anchor */
						goto _dest_overflow;
					}

					continue;
				}

				/* set prices for first positions (literals) */
				{
					int rPos;

					for (rPos = 0 ; rPos < lz4_lib.lz4.MINMATCH ; rPos++) {
						const int cost = .LZ4HC_literalsPrice(llen + rPos);
						opt[rPos].mlen = 1;
						opt[rPos].off = 0;
						opt[rPos].litlen = llen + rPos;
						opt[rPos].price = cost;

						debug(7) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (litlen=%d) -- initial setup`, rPos, cost, opt[rPos].litlen);
						}
					}
				}
				/* set prices using initial match */
				{
					int mlen = lz4_lib.lz4.MINMATCH;

					/* necessarily < sufficient_len < LZ4_OPT_NUM */
					const int matchML = firstMatch.len;

					const int offset = firstMatch.off;
					assert(matchML < .LZ4_OPT_NUM);

					for ( ; mlen <= matchML; mlen++) {
						const int cost = .LZ4HC_sequencePrice(llen, mlen);
						opt[mlen].mlen = mlen;
						opt[mlen].off = offset;
						opt[mlen].litlen = llen;
						opt[mlen].price = cost;

						debug(7) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (matchlen=%d) -- initial setup`, mlen, cost, mlen);
						}
					}
				}

				last_match_pos = firstMatch.len;

				{
					int addLit;

					for (addLit = 1; addLit <= TRAILING_LITERALS; addLit++) {
						/* literal */
						opt[last_match_pos + addLit].mlen = 1;

						opt[last_match_pos + addLit].off = 0;
						opt[last_match_pos + addLit].litlen = addLit;
						opt[last_match_pos + addLit].price = opt[last_match_pos].price + .LZ4HC_literalsPrice(addLit);

						debug(7) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (litlen=%d) -- initial setup`, last_match_pos + addLit, opt[last_match_pos + addLit].price, addLit);
						}
					}
				}

				/* check further positions */
				for (cur = 1; cur < last_match_pos; cur++) {
					const ubyte* curPtr = ip + cur;
					.LZ4HC_match_t newMatch;

					if (curPtr > mflimit) {
						break;
					}

					debug (7) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%u[%u] vs [%u]%u`, cur, opt[cur].price, opt[cur + 1].price, cur + 1);
					}

					if (fullUpdate) {
						/* not useful to search here if next position has same (or lower) cost */
						if ((opt[cur + 1].price <= opt[cur].price) && (opt[cur + lz4_lib.lz4.MINMATCH].price < opt[cur].price + 3)) {
							continue;
						}
					} else {
						/* not useful to search here if next position has same (or lower) cost */
						if (opt[cur + 1].price <= opt[cur].price) {
							continue;
						}
					}

					debug (7) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `search at rPos:%u`, cur);
					}

					if (fullUpdate) {
						newMatch = this.FindLongerMatch(curPtr, matchlimit, lz4_lib.lz4.MINMATCH - 1, nbSearches, dict, favorDecSpeed);
					} else {
						/* only test matches of minimum length; slightly faster, but misses a few bytes */
						newMatch = this.FindLongerMatch(curPtr, matchlimit, last_match_pos - cur, nbSearches, dict, favorDecSpeed);
					}

					if (!newMatch.len) {
						continue;
					}

					if ((cast(size_t)(newMatch.len) > sufficient_len) || (newMatch.len + cur >= .LZ4_OPT_NUM)) {
						/* immediate encoding */
						best_mlen = newMatch.len;
						best_off = newMatch.off;
						last_match_pos = cur + 1;
						goto encode;
					}

					/* before match : set price with literals at beginning */
					{
						const int baseLitlen = opt[cur].litlen;
						int litlen;

						for (litlen = 1; litlen < lz4_lib.lz4.MINMATCH; litlen++) {
							const int price = opt[cur].price - .LZ4HC_literalsPrice(baseLitlen) + .LZ4HC_literalsPrice(baseLitlen + litlen);
							const int pos = cur + litlen;

							if (price < opt[pos].price) {
								/* literal */
								opt[pos].mlen = 1;

								opt[pos].off = 0;
								opt[pos].litlen = baseLitlen + litlen;
								opt[pos].price = price;

								debug(7) {
									lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (litlen=%d)`, pos, price, opt[pos].litlen);
								}
							}
						}
					}

					/* set prices using match at position = cur */
					{
						const int matchML = newMatch.len;
						int ml = lz4_lib.lz4.MINMATCH;

						assert(cur + newMatch.len < .LZ4_OPT_NUM);

						for ( ; ml <= matchML ; ml++) {
							const int pos = cur + ml;
							const int offset = newMatch.off;
							int price;
							int ll;

							debug(7) {
								lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `testing price rPos %d (last_match_pos=%d)`, pos, last_match_pos);
							}

							if (opt[cur].mlen == 1) {
								ll = opt[cur].litlen;
								price = ((cur > ll) ? (opt[cur - ll].price) : (0)) + .LZ4HC_sequencePrice(ll, ml);
							} else {
								ll = 0;
								price = opt[cur].price + .LZ4HC_sequencePrice(0, ml);
							}

							assert(cast(uint)(favorDecSpeed) <= 1);

							if (pos > last_match_pos + TRAILING_LITERALS || price <= opt[pos].price - cast(int)(favorDecSpeed)) {
								debug (7) {
									lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (matchlen=%d)`, pos, price, ml);
								}

								assert(pos < .LZ4_OPT_NUM);

								if ((ml == matchML) && (last_match_pos < pos)) {
									last_match_pos = pos;
								}

								opt[pos].mlen = ml;
								opt[pos].off = offset;
								opt[pos].litlen = ll;
								opt[pos].price = price;
							}
						}
					}
					/* complete following positions with literals */
					{
						int addLit;

						for (addLit = 1; addLit <= TRAILING_LITERALS; addLit++) {
							/* literal */
							opt[last_match_pos + addLit].mlen = 1;

							opt[last_match_pos + addLit].off = 0;
							opt[last_match_pos + addLit].litlen = addLit;
							opt[last_match_pos + addLit].price = opt[last_match_pos].price + .LZ4HC_literalsPrice(addLit);

							debug(7) {
								lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `rPos:%3d => price:%3d (litlen=%d)`, last_match_pos + addLit, opt[last_match_pos + addLit].price, addLit);
							}
						}
					}
				}  /* for (cur = 1; cur <= last_match_pos; cur++) */

				best_mlen = opt[last_match_pos].mlen;
				best_off = opt[last_match_pos].off;
				cur = last_match_pos - best_mlen;

		encode: /* cur, last_match_pos, best_mlen, best_off must be set */
				assert(cur < .LZ4_OPT_NUM);

				/* == 1 when only one candidate */
				assert(last_match_pos >= 1);

				debug (6) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `reverse traversal, looking for shortest path (last_match_pos=%d)`, last_match_pos);
				}

				{
					int candidate_pos = cur;
					int selected_matchLength = best_mlen;
					int selected_offset = best_off;

					while (1) {  /* from end to beginning */

						/* can be 1, means literal */
						const int next_matchLength = opt[candidate_pos].mlen;

						const int next_offset = opt[candidate_pos].off;

						debug(7) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `pos %d: sequence length %d`, candidate_pos, selected_matchLength);
						}

						opt[candidate_pos].mlen = selected_matchLength;
						opt[candidate_pos].off = selected_offset;
						selected_matchLength = next_matchLength;
						selected_offset = next_offset;

						if (next_matchLength > candidate_pos) {
							/* last match elected, first match to encode */
							break;
						}

						/* can be 1, means literal */
						assert(next_matchLength > 0);

						candidate_pos -= next_matchLength;
					}
				}

				/* encode all recorded sequences in order */
				{
					/* relative position (to ip) */
					int rPos = 0;

					while (rPos < last_match_pos) {
						const int ml = opt[rPos].mlen;
						const int offset = opt[rPos].off;

						if (ml == 1) {
							/* literal; note: can end up with several literals, in which case, skip them */
							ip++;

							rPos++;
							continue;
						}

						rPos += ml;
						assert(ml >= lz4_lib.lz4.MINMATCH);
						assert((offset >= 1) && (offset <= lz4_lib.lz4.MAX_DISTANCE));
						opSaved = op;

						if (.LZ4HC_encodeSequence(&ip, &op, &anchor, ml, ip - offset, limit, oend)) { /* updates ip, op and anchor */
							goto _dest_overflow;
						}
					}
				}
			}  /* while (ip <= mflimit) */

		_last_literals:
			/* Encode Last Literals */
			{
				/* literals */
				size_t lastRunSize = cast(size_t)(iend - anchor);

				size_t litLength = (lastRunSize + 255 - lz4_lib.lz4.RUN_MASK) / 255;
				const size_t totalSize = 1 + litLength + lastRunSize;

				if (limit == .limitedOutput_directive.limitedDestSize) {
					/* restore correct value */
					oend += lz4_lib.lz4.LASTLITERALS;
				}

				if (limit && (op + totalSize > oend)) {
					if (limit == .limitedOutput_directive.limitedOutput) {
						/* Check output limit */
						return 0;
					}

					/* adapt lastRunSize to fill 'dst' */
					lastRunSize = cast(size_t)(oend - op) - 1;
					litLength = (lastRunSize + 255 - lz4_lib.lz4.RUN_MASK) / 255;
					lastRunSize -= litLength;
				}

				ip = anchor + lastRunSize;

				if (lastRunSize >= lz4_lib.lz4.RUN_MASK) {
					size_t accumulator = lastRunSize - lz4_lib.lz4.RUN_MASK;
					*op++ = (lz4_lib.lz4.RUN_MASK << lz4_lib.lz4.ML_BITS);

					for (; accumulator >= 255 ; accumulator -= 255) {
						*op++ = 255;
					}

					*op++ = cast(ubyte)(accumulator);
				} else {
					*op++ = cast(ubyte)(lastRunSize << lz4_lib.lz4.ML_BITS);
				}

				core.stdc.string.memcpy(op, anchor, lastRunSize);
				op += lastRunSize;
			}

			/* End */
			srcSizePtr = cast(int)((cast(const (char)*)(ip)) - source);

			return cast(int)(cast(char*)(op) - dst);

		_dest_overflow:

			if (limit == .limitedOutput_directive.limitedDestSize) {
				/* restore correct out pointer */
				op = opSaved;

				goto _last_literals;
			}

			return 0;
		}
}

/*
 * do not use these definitions directly.
 * allocate an LZ4_streamHC_t instead.
 */
/**
 * 262200 or 262256
 */
package enum LZ4_STREAMHCSIZE = (4 * .LZ4HC_HASHTABLESIZE) + (2 * .LZ4HC_MAXD) + 56 + (((void*).sizeof == 16) ? (56) : (0));
package enum LZ4_STREAMHCSIZE_SIZET = .LZ4_STREAMHCSIZE / size_t.sizeof;

/*
 * LZ4_streamHC_t :
 * This structure allows static allocation of LZ4 HC streaming state.
 * State must be initialized using LZ4_resetStreamHC() before first use.
 *
 * Static allocation shall only be used in combination with static linking.
 * When invoking LZ4 from a DLL, use create/free functions instead, which are API and ABI stable.
 */
union LZ4_streamHC_t
{
public:
	size_t[.LZ4_STREAMHCSIZE_SIZET] table;

package:
	.LZ4HC_CCtx_internal internal_donotuse;

	/**
	 * compress_HC_destSize() : v1.8.0 (experimental)
	 *  Will try to compress as much data from `src` as possible
	 *  that can fit into `targetDstSize` budget.
	 *  Result is provided in 2 parts :
	 * `srcSizePtr` : value will be updated to indicate how much bytes were read from `src`
	 *
	 * Returns: the number of bytes written into 'dst' or 0 if compression fails.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	int compress_HC_destSize(const (char)* source, char* dest, ref int srcSizePtr, const int targetDestSize, const int cLevel)

		in
		{
			assert(source != null);
			assert(dest != null);
		}

		do
		{
			this.resetStreamHC(cLevel);
			this.internal_donotuse.init(cast(const (ubyte)*)(source));

			return this.internal_donotuse.compress_generic(source, dest, srcSizePtr, targetDestSize, cLevel, .limitedOutput_directive.limitedDestSize);
		}

	/**
	 * initialization
	 */
	//LZ4LIB_API
	pure nothrow @nogc
	public void resetStreamHC(const int compressionLevel)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			/* if compilation fails here, LZ4_STREAMHCSIZE must be increased */
			static assert(.LZ4HC_CCtx_internal.sizeof <= size_t.sizeof * .LZ4_STREAMHCSIZE_SIZET);

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `resetStreamHC(%X, %d)`, &this, compressionLevel);
			}

			this.internal_donotuse.end = cast(const (ubyte)*)(ptrdiff_t.max);
			this.internal_donotuse.base = null;
			this.internal_donotuse.dictCtx = null;
			this.internal_donotuse.favorDecSpeed = 0;
			this.internal_donotuse.dirty = 0;
			this.setCompressionLevel(compressionLevel);
		}

	/**
	 * resetStreamHC_fast() :
	 *  When an LZ4_streamHC_t is known to be in a internally coherent state,
	 *  it can often be prepared for a new compression with almost no work, only
	 *  sometimes falling back to the full, expensive reset that is always required
	 *  when the stream is in an indeterminate state (i.e., the reset performed by
	 *  resetStreamHC()).
	 *
	 *  LZ4_streamHCs are guaranteed to be in a valid state when:
	 *  - returned from LZ4_createStreamHC()
	 *  - reset by resetStreamHC()
	 *  - memset(stream, 0, LZ4_streamHC_t.sizeof)
	 *  - the stream was in a valid state and was reset by resetStreamHC_fast()
	 *  - the stream was in a valid state and was then used in any compression call
	 *    that returned success
	 *  - the stream was in an indeterminate state and was used in a compression
	 *    call that fully reset the state (LZ4_compress_HC_extStateHC()) and that
	 *    returned success
	 *
	 *  Note:
	 *  A stream that was last used in a compression call that returned an error
	 *  may be passed to this function. However, it will be fully reset, which will
	 *  clear any existing history and settings from the context.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	void resetStreamHC_fast(const int compressionLevel)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `resetStreamHC_fast(%X, %d)`, &this, compressionLevel);
			}

			if (this.internal_donotuse.dirty) {
				this.resetStreamHC(compressionLevel);
			} else {
				this.internal_donotuse.end -= cast(size_t)(this.internal_donotuse.base);
				this.internal_donotuse.base = null;
				this.internal_donotuse.dictCtx = null;
				this.setCompressionLevel(compressionLevel);
			}
		}

	/**
	 * setCompressionLevel() : v1.8.0 (experimental)
	 *  It's possible to change compression level between 2 invocations of compress_HC_continue*()
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	void setCompressionLevel(int compressionLevel)

		in
		{
		}

		do
		{
			if (compressionLevel < 1) {
				compressionLevel = .LZ4HC_CLEVEL_DEFAULT;
			}

			if (compressionLevel > .LZ4HC_CLEVEL_MAX) {
				compressionLevel = .LZ4HC_CLEVEL_MAX;
			}

			this.internal_donotuse.compressionLevel = cast(short)(compressionLevel);
		}

	/**
	 * favorDecompressionSpeed() : v1.8.2 (experimental)
	 *  Parser will select decisions favoring decompression over compression ratio.
	 *  Only work at highest compression settings (level >= LZ4HC_CLEVEL_OPT_MIN)
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	void favorDecompressionSpeed(const int favor)

		in
		{
		}

		do
		{
			this.internal_donotuse.favorDecSpeed = (favor != 0);
		}

	//LZ4LIB_API
	pure nothrow @nogc
	public int loadDictHC(const (char)* dictionary, int dictSize)

		in
		{
			assert(dictionary != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			debug(4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `loadDictHC(%X, %X, %d)`, &this, dictionary, dictSize);
			}

			if (dictSize > lz4_lib.lz4common.KB!(int, 64)) {
				dictionary += dictSize - lz4_lib.lz4common.KB!(uint, 64);
				dictSize = lz4_lib.lz4common.KB!(int, 64);
			}

			this.resetStreamHC(this.internal_donotuse.compressionLevel);
			this.internal_donotuse.init(cast(const (ubyte)*)(dictionary));
			this.internal_donotuse.end = cast(const (ubyte)*)(dictionary) + dictSize;

			if (dictSize >= 4) {
				this.internal_donotuse.Insert(this.internal_donotuse.end - 3);
			}

			return dictSize;
		}

	/**
	 * attach_HC_dictionary() :
	 *  This is an experimental API that allows for the efficient use of a
	 *  static dictionary many times.
	 *
	 *  Rather than re-loading the dictionary buffer into a working context before
	 *  each compression, or copying a pre-loaded dictionary's LZ4_streamHC_t into a
	 *  working LZ4_streamHC_t, this function introduces a no-copy setup mechanism,
	 *  in which the working stream references the dictionary stream in-place.
	 *
	 *  Several assumptions are made about the state of the dictionary stream.
	 *  Currently, only streams which have been prepared by loadDictHC() should
	 *  be expected to work.
	 *
	 *  Alternatively, the provided dictionary stream pointer may be null, in which
	 *  case any existing dictionary stream is unset.
	 *
	 *  A dictionary should only be attached to a stream without any history (i.e.,
	 *  a stream that has just been reset).
	 *
	 *  The dictionary will remain attached to the working stream only for the
	 *  current stream session. Calls to resetStreamHC(_fast) will remove the
	 *  dictionary context association from the working stream. The dictionary
	 *  stream (and source buffer) must remain in-place / accessible / unchanged
	 *  through the lifetime of the stream session.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public void attach_HC_dictionary(const .LZ4_streamHC_t* dictionaryStream)

		in
		{
		}

		do
		{
			this.internal_donotuse.dictCtx = (dictionaryStream != null) ? (&((*dictionaryStream).internal_donotuse)) : (null);
		}

	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public void attach_HC_dictionary(const ref .LZ4_streamHC_t dictionaryStream)

		in
		{
		}

		do
		{
			this.internal_donotuse.dictCtx = (&dictionaryStream.internal_donotuse);
		}

	/**
	 * compression
	 */
	pure nothrow @nogc
	package int compressHC_continue_generic(const (char)* src, char* dst, ref int srcSizePtr, const int dstCapacity, const .limitedOutput_directive limit)

		in
		{
			assert(src != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			debug(4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compressHC_continue_generic(%X, %X, %d)`, &this, src, srcSizePtr);
			}

			/* auto-init if forgotten */
			if (this.internal_donotuse.base == null) {
				this.internal_donotuse.init(cast(const (ubyte)*)(src));
			}

			/* Check overflow */
			if (cast(size_t)(this.internal_donotuse.end - this.internal_donotuse.base) > lz4_lib.lz4common.GB!(uint, 2)) {
				size_t dictSize = cast(size_t)(this.internal_donotuse.end - this.internal_donotuse.base) - this.internal_donotuse.dictLimit;

				if (dictSize > lz4_lib.lz4common.KB!(size_t, 64)) {
					dictSize = lz4_lib.lz4common.KB!(size_t, 64);
				}

				this.loadDictHC(cast(const (char)*)(this.internal_donotuse.end) - dictSize, cast(int)(dictSize));
			}

			/* Check if blocks follow each other */
			if (cast(const (ubyte)*)(src) != this.internal_donotuse.end) {
				this.internal_donotuse.setExternalDict(cast(const (ubyte)*)(src));
			}

			/* Check overlapping input/dictionary space */
			{
				const (ubyte)* sourceEnd = cast(const (ubyte)*)(src) + srcSizePtr;
				const ubyte* dictBegin = this.internal_donotuse.dictBase + this.internal_donotuse.lowLimit;
				const ubyte* dictEnd = this.internal_donotuse.dictBase + this.internal_donotuse.dictLimit;

				if ((sourceEnd > dictBegin) && (cast(const (ubyte)*)(src) < dictEnd)) {
					if (sourceEnd > dictEnd) {
						sourceEnd = dictEnd;
					}

					this.internal_donotuse.lowLimit = cast(uint)(sourceEnd - this.internal_donotuse.dictBase);

					if (this.internal_donotuse.dictLimit - this.internal_donotuse.lowLimit < 4) {
						this.internal_donotuse.lowLimit = this.internal_donotuse.dictLimit;
					}
				}
			}

			return this.internal_donotuse.compress_generic(src, dst, srcSizePtr, dstCapacity, this.internal_donotuse.compressionLevel, limit);
		}

	//LZ4LIB_API
	pure nothrow @nogc
	public int compress_HC_continue(const (char)* src, char* dst, int srcSize, const int dstCapacity)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4;

			if (dstCapacity < lz4_lib.lz4.LZ4_compressBound(srcSize)) {
				return this.compressHC_continue_generic(src, dst, srcSize, dstCapacity, .limitedOutput_directive.limitedOutput);
			} else {
				return this.compressHC_continue_generic(src, dst, srcSize, dstCapacity, .limitedOutput_directive.noLimit);
			}
		}

	/**
	 * compress_HC_continue_destSize() : v1.8.0 (experimental)
	 *  Similar as compress_HC_continue(),
	 *  but will read a variable nb of bytes from `src`
	 *  to fit into `targetDstSize` budget.
	 *  Result is provided in 2 parts :
	 * `srcSizePtr` : value will be updated to indicate how much bytes were read from `src`.
	 *
	 * Returns: the number of bytes written into 'dst' or 0 if compression fails.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	int compress_HC_continue_destSize(const (char)* src, char* dst, ref int srcSizePtr, const int targetDestSize)

		in
		{
		}

		do
		{
			return this.compressHC_continue_generic(src, dst, srcSizePtr, targetDestSize, .limitedOutput_directive.limitedDestSize);
		}

	/* dictionary saving */

	//LZ4LIB_API
	pure nothrow @nogc @system
	public int saveDictHC(char* safeBuffer, int dictSize)

		in
		{
			assert(safeBuffer != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			const int prefixSize = cast(int)(this.internal_donotuse.end - (this.internal_donotuse.base + this.internal_donotuse.dictLimit));

			debug(4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `saveDictHC(%X, %X, %d)`, &this, safeBuffer, dictSize);
			}

			if (dictSize > lz4_lib.lz4common.KB!(int, 64)) {
				dictSize = lz4_lib.lz4common.KB!(int, 64);
			}

			if (dictSize < 4) {
				dictSize = 0;
			}

			if (dictSize > prefixSize) {
				dictSize = prefixSize;
			}

			core.stdc.string.memmove(safeBuffer, this.internal_donotuse.end - dictSize, dictSize);

			{
				const uint endIndex = cast(uint)(this.internal_donotuse.end - this.internal_donotuse.base);
				this.internal_donotuse.end = cast(const (ubyte)*)(safeBuffer) + dictSize;
				this.internal_donotuse.base = this.internal_donotuse.end - endIndex;
				this.internal_donotuse.dictLimit = endIndex - dictSize;
				this.internal_donotuse.lowLimit = endIndex - dictSize;

				if (this.internal_donotuse.nextToUpdate < this.internal_donotuse.dictLimit) {
					this.internal_donotuse.nextToUpdate = this.internal_donotuse.dictLimit;
				}
			}

			return dictSize;
		}
}

package alias LZ4_streamHC_u = .LZ4_streamHC_t;

/*-*************************************
*  Tuning Parameter
***************************************/
/**
 * HEAPMODE :
 *  Select how default compression function will allocate workplace memory,
 *  in stack (false:fastest), or in heap (true:requires malloc()).
 *  Since workplace is rather large, heap mode is recommended.
 */
static if (!__traits(compiles, LZ4HC_HEAPMODE)) {
	enum bool LZ4HC_HEAPMODE = true;
}

/*===   Macros   ===*/
pragma(inline, true)
pure nothrow @safe @nogc
private uint HASH_FUNCTION(const uint i)

	in
	{
	}

	do
	{
		return (i * 2654435761u) >> ((lz4_lib.lz4.MINMATCH * 8) - .LZ4HC_HASH_LOG);
	}

pragma(inline, true)
pure nothrow @safe @nogc
private ushort DELTANEXTU16(const ref ushort[.LZ4HC_MAXD] table, const int pos)

	in
	{
	}

	do
	{
		/* faster */
		return table[cast(ushort)(pos)];
	}

pure nothrow @nogc
package uint LZ4HC_hashPtr(const void* ptr)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4common;

		return .HASH_FUNCTION(lz4_lib.lz4common.read32(ptr));
	}

/*-*************************************
*  HC Compression
**************************************/

/**
 * LZ4HC_countBack() :
 *
 * Returns: negative value, nb of common bytes before ip/match
 */
pragma(inline, true)
pure nothrow @nogc
int LZ4HC_countBack(const ubyte* ip, const ubyte* match, const ubyte* iMin, const ubyte* mMin)

	in
	{
		assert(ip != null);
		assert(match != null);
		assert(iMin != null);
		assert(mMin != null);
		assert(ip >= iMin);
		assert(cast(size_t)(ip - iMin) < (1u << 31));
		assert(match >= mMin);
		assert(cast(size_t)(match - mMin) < (1u << 31));
	}

	do
	{
		static import lz4_lib.lz4common;

		int back = 0;
		const int min = cast(int)(lz4_lib.lz4common.MAX(iMin - ip, mMin - match));
		assert(min <= 0);

		while ((back > min) && (ip[back - 1] == match[back - 1])) {
			back--;
		}

		return back;
	}

/**
 * LZ4HC_countPattern() :
 * pattern32 must be a sample of repetitive pattern of length 1, 2 or 4 (but not 3!)
 */
pure nothrow @nogc
package uint LZ4HC_countPattern(const (ubyte)* ip, const ubyte* iEnd, const uint pattern32)

	in
	{
		assert(ip != null);
		assert(iEnd != null);
	}

	do
	{
		static import lz4_lib.lz4common;
		static import lz4_lib.lz4;

		const ubyte* iStart = ip;

		static if (size_t.sizeof == 8) {
			const size_t pattern = cast(ulong)(pattern32) + ((cast(ulong)(pattern32)) << 32);
		} else {
			static assert(size_t.sizeof == 4);
			const size_t pattern = pattern32;
		}

		while (lz4_lib.lz4common.likely(ip < iEnd - (pattern.sizeof - 1))) {
			const size_t diff = lz4_lib.lz4common.read_ARCH(ip) ^ pattern;

			if (!diff) {
				ip += pattern.sizeof;
				continue;
			}

			ip += lz4_lib.lz4.NbCommonBytes(diff);

			return cast(uint)(ip - iStart);
		}

		version (LittleEndian) {
			size_t patternByte = pattern;

			while ((ip < iEnd) && (*ip == cast(ubyte)(patternByte))) {
				ip++;
				patternByte >>= 8;
			}
		} else {
			uint bitOffset = (pattern.sizeof * 8) - 8;

			while (ip < iEnd) {
				const ubyte patternByte = cast(ubyte)(pattern >> bitOffset);

				if (*ip != patternByte) {
					break;
				}

				ip++;
				bitOffset -= 8;
			}
		}

		return cast(uint)(ip - iStart);
	}

/**
 * LZ4HC_reverseCountPattern() :
 * pattern must be a sample of repetitive pattern of length 1, 2 or 4 (but not 3!)
 * read using natural platform endianess
 */
pure nothrow @nogc
package uint LZ4HC_reverseCountPattern(const (ubyte)* ip, const ubyte* iLow, const uint pattern)

	in
	{
		assert(ip != null);
		assert(iLow != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		const ubyte* iStart = ip;

		while (lz4_lib.lz4common.likely(ip >= iLow + 4)) {
			if (lz4_lib.lz4common.read32(ip - 4) != pattern) {
				break;
			}

			ip -= 4;
		}

		{
			/* works for any endianess */
			const (ubyte)* bytePtr = cast(const (ubyte)*)(&pattern) + 3;

			while (lz4_lib.lz4common.likely(ip > iLow)) {
				if (ip[-1] != *bytePtr) {
					break;
				}

				ip--;
				bytePtr--;
			}
		}

		return cast(uint)(iStart - ip);
	}

enum repeat_state_e
{
	untested,
	not,
	confirmed,
}

enum HCfavor_e
{
	favorCompressionRatio = 0,
	favorDecompressionSpeed,
}

/**
 * LZ4HC_encodeSequence() :
 *
 * Returns: 0 if ok, 1 if buffer issue detected
 */
pragma(inline, true)
pure nothrow @nogc
int LZ4HC_encodeSequence(const (ubyte)** ip, ubyte** op, const (ubyte)** anchor, const int matchLength, const ubyte* match, .limitedOutput_directive limit, ubyte* oend)

	in
	{
		assert(ip != null);
		assert(op != null);
		assert(anchor != null);
		assert(match != null);
		assert(oend != null);
	}

	do
	{
		static import lz4_lib.lz4common;
		static import lz4_lib.lz4;

		size_t length;
		ubyte* token = (*op)++;

		debug (6) {
			static const (ubyte)* start = null;
			static uint totalCost = 0;
			const uint pos = (start == null) ? (0) : (cast(uint)(*anchor - start));
			const uint ll = cast(uint)(*ip - *anchor);
			const uint llAdd = (ll >= 15) ? ((ll - 15) / 255) + 1 : 0;
			const uint mlAdd = (matchLength >= 19) ? (((matchLength - 19) / 255) + 1) : (0);
			const uint cost = 1 + llAdd + ll + 2 + mlAdd;

			if (start == null) {
				/* only works for single segment */
				start = *anchor;
			}

			/* g_debuglog_enable = (pos >= 2228) & (pos <= 2262); */
			debug (6) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `pos:%7u -- literals:%3u, match:%4d, offset:%5u, cost:%3u + %u`, pos, cast(uint)(*ip - *anchor), matchLength, cast(uint)(*ip - match), cost, totalCost);
			}

			totalCost += cost;
		}

		/* Encode Literal length */
		length = cast(size_t)(*ip - *anchor);

		if ((limit) && ((*op + (length / 255) + length + (2 + 1 + lz4_lib.lz4.LASTLITERALS)) > oend)) {
			/* Check output limit */
			return 1;
		}

		if (length >= lz4_lib.lz4.RUN_MASK) {
			size_t len = length - lz4_lib.lz4.RUN_MASK;
			*token = (lz4_lib.lz4.RUN_MASK << lz4_lib.lz4.ML_BITS);

			for (; len >= 255 ; len -= 255) {
				*(*op)++ = 255;
			}

			*(*op)++ = cast(ubyte)(len);
		} else {
			*token = cast(ubyte)(length << lz4_lib.lz4.ML_BITS);
		}

		/* Copy Literals */
		lz4_lib.lz4common.wildCopy(*op, *anchor, (*op) + length);
		*op += length;

		/* Encode Offset */
		/* note : consider providing offset as a value, rather than as a pointer difference */
		assert((*ip - match) <= lz4_lib.lz4.MAX_DISTANCE);

		lz4_lib.lz4common.writeLE16(*op, cast(ushort)(*ip - match));
		*op += 2;

		/* Encode MatchLength */
		assert(matchLength >= lz4_lib.lz4.MINMATCH);
		length = cast(size_t)(matchLength - lz4_lib.lz4.MINMATCH);

		if ((limit) && (*op + (length / 255) + (1 + lz4_lib.lz4.LASTLITERALS) > oend)) {
			/* Check output limit */
			return 1;
		}

		if (length >= lz4_lib.lz4.ML_MASK) {
			*token += lz4_lib.lz4.ML_MASK;
			length -= lz4_lib.lz4.ML_MASK;

			for (; length >= 510 ; length -= 510) {
				*(*op)++ = 255;
				*(*op)++ = 255;
			}

			if (length >= 255) {
				length -= 255;
				*(*op)++ = 255;
			}

			*(*op)++ = cast(ubyte)(length);
		} else {
			*token += cast(ubyte)(length);
		}

		/* Prepare next loop */
		*ip += matchLength;
		*anchor = *ip;

		return 0;
	}

private enum lz4hc_strat_e
{
	lz4hc,
	lz4opt,
}

private struct cParams_t
{
	.lz4hc_strat_e strat;
	uint nbSearches;
	uint targetLength;
}

private static immutable .cParams_t[.LZ4HC_CLEVEL_MAX + 1] clTable =
[
	/* 0, unused */
	{.lz4hc_strat_e.lz4hc, 2, 16},

	/* 1, unused */
	{.lz4hc_strat_e.lz4hc, 2, 16},

	/* 2, unused */
	{.lz4hc_strat_e.lz4hc, 2, 16},

	/* 3 */
	{.lz4hc_strat_e.lz4hc, 4, 16},

	/* 4 */
	{.lz4hc_strat_e.lz4hc, 8, 16},

	/* 5 */
	{.lz4hc_strat_e.lz4hc, 16, 16},

	/* 6 */
	{.lz4hc_strat_e.lz4hc, 32, 16},

	/* 7 */
	{.lz4hc_strat_e.lz4hc, 64, 16},

	/* 8 */
	{.lz4hc_strat_e.lz4hc, 128, 16},

	/* 9 */
	{.lz4hc_strat_e.lz4hc, 256, 16},

	/*10==LZ4HC_CLEVEL_OPT_MIN*/
	{.lz4hc_strat_e.lz4opt, 96, 64},

	/*11 */
	{.lz4hc_strat_e.lz4opt, 512, 128},

	/* 12==LZ4HC_CLEVEL_MAX */
	{.lz4hc_strat_e.lz4opt, 16384, .LZ4_OPT_NUM},
];

//LZ4LIB_API
pure nothrow @safe @nogc
public size_t LZ4_sizeofStateHC()

	in
	{
	}

	do
	{
		return .LZ4_streamHC_t.sizeof;
	}

/**
 * LZ4_compress_HC_extStateHC_fastReset() :
 *  A variant of LZ4_compress_HC_extStateHC().
 *
 *  Using this variant avoids an expensive initialization step. It is only safe
 *  to call if the state buffer is known to be correctly initialized already
 *  (see above comment on resetStreamHC_fast() for a definition of
 *  "correctly initialized"). From a high level, the difference is that this
 *  function initializes the provided state with a call to
 *  resetStreamHC_fast() while LZ4_compress_HC_extStateHC() starts with a
 *  call to resetStreamHC().
 */
//LZ4LIB_STATIC_API
pure nothrow @nogc
int LZ4_compress_HC_extStateHC_fastReset(void* state, const (char)* src, char* dst, int srcSize, const int dstCapacity, const int compressionLevel)

	in
	{
		assert(state != null);
		assert(src != null);
	}

	do
	{
		static import lz4_lib.lz4;

		.LZ4HC_CCtx_internal* ctx = &((*(cast(.LZ4_streamHC_t*)(state))).internal_donotuse);

		if ((cast(size_t)(state) & (((void*).sizeof) -1)) != 0) {
			/* Error : state is not aligned for pointers (32 or 64 bits) */
			return 0;
		}

		(*(cast(.LZ4_streamHC_t*)(state))).resetStreamHC_fast(compressionLevel);
		(*ctx).init(cast(const (ubyte)*)(src));

		if (dstCapacity < lz4_lib.lz4.LZ4_compressBound(srcSize)) {
			return (*ctx).compress_generic(src, dst, srcSize, dstCapacity, compressionLevel, .limitedOutput_directive.limitedOutput);
		} else {
			return (*ctx).compress_generic(src, dst, srcSize, dstCapacity, compressionLevel, .limitedOutput_directive.noLimit);
		}
	}

/**
 * LZ4_compress_HC_extStateHC() :
 *  Same as LZ4_compress_HC(), but using an externally allocated memory segment for `state`.
 * `state` size is provided by LZ4_sizeofStateHC().
 *  Memory segment must be aligned on 8-bytes boundaries (which a normal malloc() should do properly).
 */
//LZ4LIB_API
pure nothrow @nogc
public int LZ4_compress_HC_extStateHC(void* state, const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int compressionLevel)

	in
	{
		assert(state != null);
	}

	do
	{
		if ((cast(size_t)(state) & (((void*).sizeof) -1)) != 0) {
			/* Error : state is not aligned for pointers (32 or 64 bits) */
			return 0;
		}

		(*(cast(.LZ4_streamHC_t*)(state))).resetStreamHC(compressionLevel);

		return .LZ4_compress_HC_extStateHC_fastReset(state, src, dst, srcSize, dstCapacity, compressionLevel);
	}

/**
 * LZ4_compress_HC() :
 *  Compress data from `src` into `dst`, using the more powerful but slower "HC" algorithm.
 * `dst` must be already allocated.
 *  Compression is guaranteed to succeed if `dstCapacity >= LZ4_compressBound(srcSize)` (see "lz4.h")
 *  Max supported `srcSize` value is LZ4_MAX_INPUT_SIZE (see "lz4.h")
 * `compressionLevel` : any value between 1 and LZ4HC_CLEVEL_MAX will work.
 *                      Values > LZ4HC_CLEVEL_MAX behave the same as LZ4HC_CLEVEL_MAX.
 *
 * Returns: the number of bytes written into 'dst' or 0 if compression fails.
 */
static if (LZ4HC_HEAPMODE) {
	//LZ4LIB_API
	nothrow @nogc @system
	public int LZ4_compress_HC(const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int compressionLevel)

		in
		{
		}

		do
		{
			static import core.stdc.stdlib;

			.LZ4_streamHC_t* statePtr = cast(.LZ4_streamHC_t*)(core.stdc.stdlib.malloc(.LZ4_streamHC_t.sizeof));
			const int cSize = .LZ4_compress_HC_extStateHC(statePtr, src, dst, srcSize, dstCapacity, compressionLevel);
			core.stdc.stdlib.free(statePtr);
			statePtr = null;

			return cSize;
		}
} else {
	//LZ4LIB_API
	pure nothrow @nogc
	public int LZ4_compress_HC(const (char)* src, char* dst, const int srcSize, const int dstCapacity, const int compressionLevel)

		in
		{
		}

		do
		{
			.LZ4_streamHC_t state;
			const int cSize = .LZ4_compress_HC_extStateHC(&state, src, dst, srcSize, dstCapacity, compressionLevel);

			return cSize;
		}
}

/*-*************************************
*  Streaming Functions
**************************************/

/* ================================================
 * LZ4 Optimal parser (levels 10-12)
 * ===============================================*/
struct LZ4HC_optimal_t
{
	int price;
	int off;
	int mlen;
	int litlen;

	invariant
	{
	}
}

/**
 * price in bytes
 */
pragma(inline, true)
pure nothrow @safe @nogc
int LZ4HC_literalsPrice(const int litlen)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4;

		int price = litlen;

		if (litlen >= cast(int)(lz4_lib.lz4.RUN_MASK)) {
			price += 1 + (litlen - lz4_lib.lz4.RUN_MASK) / 255;
		}

		return price;
	}

/**
 * requires mlen >= MINMATCH
 */
pragma(inline, true)
pure nothrow @safe @nogc
int LZ4HC_sequencePrice(const int litlen, const int mlen)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4;

		/* token + 16-bit offset */
		int price = 1 + 2 ;

		price += .LZ4HC_literalsPrice(litlen);

		if (mlen >= cast(int)(lz4_lib.lz4.ML_MASK + lz4_lib.lz4.MINMATCH)) {
			price += 1 + (mlen - (lz4_lib.lz4.ML_MASK + lz4_lib.lz4.MINMATCH)) / 255;
		}

		return price;
	}

struct LZ4HC_match_t
{
	int off;
	int len;

	invariant
	{
	}
}
