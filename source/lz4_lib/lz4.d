/**
 * Introduction
 *
 * LZ4 is lossless compression algorithm, providing compression speed at 500 MB/s per core,
 * scalable with multi-cores CPU. It features an extremely fast decoder, with speed in
 * multiple GB/s per core, typically reaching RAM speed limits on multi-core systems.
 *
 * The LZ4 compression library provides in-memory compression and decompression functions.
 * Compression can be done in:
 *   - a single step (described as Simple Functions)
 *   - a single step, reusing a context (described in Advanced Functions)
 *   - unbounded multiple steps (described as Streaming compression)
 *
 * lz4.h provides block compression functions. It gives full buffer control to user.
 * Decompressing an lz4-compressed block also requires metadata (such as compressed size).
 * Each application is free to encode such metadata in whichever way it wants.
 *
 * An additional format, called LZ4 frame specification (doc/lz4_Frame_format.md),
 * take care of encoding standard metadata alongside LZ4-compressed blocks.
 * Frame format is required for interoperability.
 * It is delivered through a companion API, declared in lz4frame.h.
 *
 * License: BSD 2-Clause License
 */
/*
   LZ4 - Fast LZ compression algorithm
   Copyright (C) 2011-present, Yann Collet.

   BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are
   met:

       * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following disclaimer
   in the documentation and/or other materials provided with the
   distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   You can contact the author at :
    - LZ4 homepage : http://www.lz4.org
    - LZ4 source repository : https://github.com/lz4/lz4
*/
module lz4_lib.lz4;


/*-************************************
*  Dependency
**************************************/
private static import lz4_lib.lz4common;
private static import core.stdc.stdlib;
private static import core.stdc.string;
private static import std.algorithm;
private static import std.format;

/*-***************************************************************
*  Version
*****************************************************************/

/**
 * for breaking interface changes
 */
enum LZ4_VERSION_MAJOR = 1;

/**
 * for new (non-breaking) interface capabilities
 */
enum LZ4_VERSION_MINOR = 8;

/**
 * for tweaks, bug-fixes, or development
 */
enum LZ4_VERSION_RELEASE = 3;

public enum LZ4_VERSION_NUMBER = (.LZ4_VERSION_MAJOR * 100 * 100) + (.LZ4_VERSION_MINOR * 100) + .LZ4_VERSION_RELEASE;
public enum string LZ4_VERSION_STRING = `1.8.3`;
static assert(std.algorithm.cmp(std.format.format!(`%d.%d.%d`)(.LZ4_VERSION_MAJOR, .LZ4_VERSION_MINOR, .LZ4_VERSION_RELEASE), .LZ4_VERSION_STRING) == 0);

/*-************************************
*  Tuning parameters
**************************************/
/**
 * LZ4_MEMORY_USAGE :
 * Memory usage formula : N->2^N Bytes (examples : 10 -> 1KB; 12 -> 4KB ; 16 -> 64KB; 20 -> 1MB; etc.)
 * Increasing memory usage improves compression ratio.
 * Reduced memory usage may improve speed, thanks to better cache locality.
 * Default value is 14, for 16KB, which nicely fits into Intel x86 L1 cache
 */
static if (!__traits(compiles, LZ4_MEMORY_USAGE)) {
	enum LZ4_MEMORY_USAGE = 14;
}

/**
 * LZ4_HEAPMODE :
 * Select how default compression functions will allocate memory for their hash table,
 * in memory stack (false:default, fastest), or in memory heap (true:requires malloc()).
 */
static if (!__traits(compiles, .LZ4_HEAPMODE)) {
	enum bool LZ4_HEAPMODE = false;
}

/**
 * ACCELERATION_DEFAULT :
 * Select "acceleration" for LZ4_compress_fast() when parameter value <= 0
 */
enum ACCELERATION_DEFAULT = 1;

/*-************************************
*  CPU Feature Detection
**************************************/
/**
 * LZ4_FORCE_SW_BITCOUNT
 * Define this parameter if your target system or compiler does not support hardware bit count
 */
/* Visual Studio for WinCE doesn't support Hardware bit count */
//#if defined(_MSC_VER) && defined(_WIN32_WCE)
//	enum bool LZ4_FORCE_SW_BITCOUNT = true;
//#endif

/*-************************************
*  Compiler Options
**************************************/

/*-************************************
*  Memory routines
**************************************/

/*-************************************
*  Basic Types
**************************************/

/*-************************************************************
 *  PRIVATE DEFINITIONS
 **************************************************************
 * Do not use these definitions directly.
 * They are only exposed to allow static allocation of `LZ4_stream_t` and `LZ4_streamDecode_t`.
 * Accessing members will expose code to API and/or ABI break in future versions of the library.
 **************************************************************/
package enum LZ4_HASHLOG = .LZ4_MEMORY_USAGE - 2;
package enum LZ4_HASHTABLESIZE = 1 << .LZ4_MEMORY_USAGE;

/**
 * required as macro for static allocation
 */
package enum LZ4_HASH_SIZE_U32 = 1 << .LZ4_HASHLOG;

package struct LZ4_stream_t_internal
{
	uint[.LZ4_HASH_SIZE_U32] hashTable;
	uint currentOffset;
	ushort dirty;
	ushort tableType;
	const (ubyte)* dictionary;
	.LZ4_stream_t_internal* dictCtx;
	uint dictSize;

	pragma(inline, true)
	pure nothrow @safe @nogc
	void prepareTable(const int inputSize, const .tableType_t tableType)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			/* If compression failed during the previous step, then the context
			 * is marked as dirty, therefore, it has to be fully reset.
			 */
			if (this.dirty) {
				debug (5) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `LZ4_prepareTable: Full reset for %p`, cctx);
				}

				this = this.init;

				return;
			}

			/*
			 * If the table hasn't been used, it's guaranteed to be zeroed out, and is
			 * therefore safe to use no matter what mode we're in. Otherwise, we figure
			 * out if it's safe to leave as is or whether it needs to be reset.
			 */
			if (this.tableType != .tableType_t.clearedTable) {
				if (this.tableType != tableType || (tableType == .tableType_t.byU16 && this.currentOffset + inputSize >= 0xFFFFU) || (tableType == .tableType_t.byU32 && this.currentOffset > lz4_lib.lz4common.GB!(uint, 1)) || tableType == .tableType_t.byPtr || inputSize >= lz4_lib.lz4common.KB!(int, 4)) {
					debug (4) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `prepareTable: Resetting table in %X`, &this);
					}

					//init
					this.hashTable[] = 0;

					this.currentOffset = 0;
					this.tableType = tableType_t.clearedTable;
				} else {
					debug (4) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `prepareTable: Re-use hash table (no reset)`);
					}
				}
			}

			/*
			 * Adding a gap, so all previous entries are > MAX_DISTANCE back, is faster
			 * than compressing without a gap. However, compressing with
			 * currentOffset == 0 is faster still, so we preserve that case.
			 */
			if (this.currentOffset != 0 && tableType == .tableType_t.byU32) {
				debug (5) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `prepareTable: adding 64KB to currentOffset`);
				}

				this.currentOffset += lz4_lib.lz4common.KB!(uint, 64);
			}

			/* Finally, clear history */
			this.dictCtx = null;
			this.dictionary = null;
			this.dictSize = 0;
		}

	/**
	 * compress_generic() :
	 * inlined, to ensure branches are decided at compilation time
	 *
	 * ToDo: inputConsumed
	 */
	pragma(inline, true)
	pure nothrow @nogc @system
	int compress_generic(const char* source, const char* dest, const int inputSize, int* inputConsumed, const int maxOutputSize, const .limitedOutput_directive outputLimited, const .tableType_t tableType, const .dict_directive dictDirective, const .dictIssue_directive dictIssue, const uint acceleration)

		in
		{
			assert(source != null);
			assert(dest != null);
		}

		out(result)
		{
			assert(result > 0);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			int result;
			const (ubyte)* ip = cast(const (ubyte)*)(source);

			const uint startIndex = this.currentOffset;
			const (ubyte)* base = cast(const (ubyte)*)(source) - startIndex;
			const (ubyte)* lowLimit;

			const .LZ4_stream_t_internal* dictCtx = cast(const .LZ4_stream_t_internal*)(this.dictCtx);
			const ubyte* dictionary = dictDirective == (.dict_directive.usingDictCtx) ? ((*dictCtx).dictionary) : (this.dictionary);
			const uint dictSize = dictDirective == (.dict_directive.usingDictCtx) ? ((*dictCtx).dictSize) : (this.dictSize);

			/* make indexes in dictCtx comparable with index in current context */
			const uint dictDelta = (dictDirective == .dict_directive.usingDictCtx) ? (startIndex - (*dictCtx).currentOffset) : (0);

			const int maybe_extMem = (dictDirective == .dict_directive.usingExtDict) || (dictDirective == .dict_directive.usingDictCtx);

			/* used when dictDirective == dictIssue_directive.dictSmall */
			const uint prefixIdxLimit = startIndex - dictSize;

			const ubyte* dictEnd = dictionary + dictSize;
			const (ubyte)* anchor = cast(const (ubyte)*)(source);
			const ubyte* iend = ip + inputSize;
			const ubyte* mflimitPlusOne = iend - .MFLIMIT + 1;
			const ubyte* matchlimit = iend - .LASTLITERALS;

			/*
			 * the dictCtx currentOffset is indexed on the start of the dictionary,
			 * while a dictionary in the current context precedes the currentOffset
			 */
			const (ubyte)* dictBase = (dictDirective == .dict_directive.usingDictCtx) ? (dictionary + dictSize - (*dictCtx).currentOffset) : (dictionary + dictSize - startIndex);

			ubyte* op = cast(ubyte*)(dest);
			const ubyte* olimit = op + maxOutputSize;

			uint offset = 0;
			uint forwardH;

			debug (5) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_generic: srcSize=%d, tableType=%u`, inputSize, tableType);
			}

			/*
			 * If init conditions are not met, we don't have to mark stream
			 * as having dirty context, since no action was taken yet
			 */
			if (outputLimited == .limitedOutput_directive.fillOutput && maxOutputSize < 1) {
				/* Impossible to store anything */
				goto _failure;
			}

			if (cast(uint)(inputSize) > cast(uint)(.LZ4_MAX_INPUT_SIZE)) {
				/* Unsupported inputSize, too large (or negative) */
				goto _failure;
			}

			if ((tableType == .tableType_t.byU16) && (inputSize >= .LZ4_64Klimit)) {
				/* Size too large (not within 64K limit) */
				goto _failure;
			}

			if (tableType == .tableType_t.byPtr) {
				/* only supported use case with tableType_t.byPtr */
				assert(dictDirective == .dict_directive.noDict);
			}

			assert(acceleration >= 1);

			lowLimit = cast(const (ubyte)*)(source) - ((dictDirective == .dict_directive.withPrefix64k) ? (dictSize) : (0));

			/* Update context state */
			if (dictDirective == .dict_directive.usingDictCtx) {
				/* Subsequent linked blocks can't use the dictionary. */
				/* Instead, they use the block we just compressed. */
				this.dictCtx = null;
				this.dictSize = cast(uint)(inputSize);
			} else {
				this.dictSize += cast(uint)(inputSize);
			}

			this.currentOffset += cast(uint)(inputSize);
			this.tableType = cast(ushort)(tableType);

			if (inputSize < .LZ4_minLength) {
				/* Input too small, no compression (all literals) */
				goto _last_literals;
			}

			/* First Byte */
			.LZ4_putPosition(ip, &this.hashTable, tableType, base);
			ip++;
			forwardH = .LZ4_hashPosition(ip, tableType);

			/* Main Loop */
			for (; ;) {
				const (ubyte)* match;
				ubyte* token;

				/* Find a match */
				if (tableType == .tableType_t.byPtr) {
					const (ubyte)* forwardIp = ip;
					uint step = 1;
					uint searchMatchNb = acceleration << .LZ4_skipTrigger;

					do {
						const uint h = forwardH;
						ip = forwardIp;
						forwardIp += step;
						step = (searchMatchNb++ >> .LZ4_skipTrigger);

						if (lz4_lib.lz4common.unlikely(forwardIp > mflimitPlusOne)) {
							goto _last_literals;
						}

						assert(ip < mflimitPlusOne);

						match = .LZ4_getPositionOnHash(h, &this.hashTable, tableType, base);
						forwardH = .LZ4_hashPosition(forwardIp, tableType);
						.LZ4_putPositionOnHash(ip, h, &this.hashTable, tableType, base);
					} while ((match + .MAX_DISTANCE < ip) || (lz4_lib.lz4common.read32(match) != lz4_lib.lz4common.read32(ip)));
				} else {   /* tableType_t.byU32, tableType_t.byU16 */

					const (ubyte)* forwardIp = ip;
					uint step = 1;
					uint searchMatchNb = acceleration << .LZ4_skipTrigger;

					do {
						const uint h = forwardH;
						const uint current = cast(uint)(forwardIp - base);
						uint matchIndex = .LZ4_getIndexOnHash(h, &this.hashTable, tableType);
						assert(matchIndex <= current);
						assert(forwardIp - base < cast(ptrdiff_t)(lz4_lib.lz4common.GB!(uint, 2) - 1));
						ip = forwardIp;
						forwardIp += step;
						step = (searchMatchNb++ >> .LZ4_skipTrigger);

						if (lz4_lib.lz4common.unlikely(forwardIp > mflimitPlusOne)) {
							goto _last_literals;
						}

						assert(ip < mflimitPlusOne);

						if (dictDirective == .dict_directive.usingDictCtx) {
							if (matchIndex < startIndex) {
								/* there was no match, try the dictionary */
								assert(tableType == .tableType_t.byU32);
								matchIndex = .LZ4_getIndexOnHash(h, &((*dictCtx).hashTable), .tableType_t.byU32);
								match = dictBase + matchIndex;

								/* make dictCtx index comparable with current context */
								matchIndex += dictDelta;

								lowLimit = dictionary;
							} else {
								match = base + matchIndex;
								lowLimit = cast(const (ubyte)*)(source);
							}
						} else if (dictDirective == .dict_directive.usingExtDict) {
							if (matchIndex < startIndex) {
								debug (7) {
									lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `extDict candidate: matchIndex=%5u  <  startIndex=%5u`, matchIndex, startIndex);
								}

								assert(startIndex - matchIndex >= .MINMATCH);
								match = dictBase + matchIndex;
								lowLimit = dictionary;
							} else {
								match = base + matchIndex;
								lowLimit = cast(const (ubyte)*)(source);
							}
						} else {   /* single continuous memory segment */
							match = base + matchIndex;
						}

						forwardH = .LZ4_hashPosition(forwardIp, tableType);
						.LZ4_putIndexOnHash(current, h, &this.hashTable, tableType);

						if ((dictIssue == .dictIssue_directive.dictSmall) && (matchIndex < prefixIdxLimit)) {
							/* match outside of valid area */
							continue;
						}

						assert(matchIndex < current);

						if ((tableType != .tableType_t.byU16) && (matchIndex + .MAX_DISTANCE < current)) {
							/* too far */
							continue;
						}

						if (tableType == .tableType_t.byU16) {
							/* too_far presumed impossible with tableType_t.byU16 */
							assert((current - matchIndex) <= .MAX_DISTANCE);
						}

						if (lz4_lib.lz4common.read32(match) == lz4_lib.lz4common.read32(ip)) {
							if (maybe_extMem) {
								offset = current - matchIndex;
							}

							/* match found */
							break;
						}
					} while (1);
				}

				/* Catch up */
				while (((ip > anchor) & (match > lowLimit)) && (lz4_lib.lz4common.unlikely(ip[-1] == match[-1]))) {
					ip--;
					match--;
				}

				/* Encode Literals */
				{
					const uint litLength = cast(uint)(ip - anchor);
					token = op++;

					if ((outputLimited == .limitedOutput_directive.limitedOutput) &&  /* Check output buffer overflow */
							(lz4_lib.lz4common.unlikely(op + litLength + (2 + 1 + .LASTLITERALS) + (litLength / 255) > olimit))) {
						goto _failure;
					}

					if ((outputLimited == .limitedOutput_directive.fillOutput) && (lz4_lib.lz4common.unlikely(op + (litLength + 240) / 255 /* litlen */ + litLength /* literals */ + 2 /* offset */ + 1 /* token */ + .MFLIMIT - .MINMATCH /* min last literals so last match is <= end - MFLIMIT */ > olimit))) {
						op--;
						goto _last_literals;
					}

					if (litLength >= .RUN_MASK) {
						int len = cast(int)(litLength) - .RUN_MASK;
						*token = (.RUN_MASK << .ML_BITS);

						for (; len >= 255 ; len -= 255) {
							*op++ = 255;
						}

						*op++ = cast(ubyte)(len);
					} else {
						*token = cast(ubyte)(litLength << .ML_BITS);
					}

					/* Copy Literals */
					lz4_lib.lz4common.wildCopy(op, anchor, op + litLength);
					op += litLength;

					debug(6) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `seq.start:%d, literals=%u, match.start:%d`, cast(int)(anchor - cast(const (ubyte)*)(source)), litLength, cast(int)(ip - cast(const (ubyte)*)(source)));
					}
				}

		_next_match:
				/*
				 * at this stage, the following variables must be correctly set :
				 * - ip : at start of LZ operation
				 * - match : at start of previous pattern occurence; can be within current prefix, or within extDict
				 * - offset : if maybe_ext_memSegment==1 (constant)
				 * - lowLimit : must be == dictionary to mean "match is within extDict"; must be == source otherwise
				 * - token and *token : position to write 4-bits for match length; higher 4-bits for literal length supposed already written
				 */

				if ((outputLimited == .limitedOutput_directive.fillOutput) && (op + 2 /* offset */ + 1 /* token */ + .MFLIMIT - .MINMATCH /* min last literals so last match is <= end - MFLIMIT */ > olimit)) {
					/* the match was too close to the end, rewind and go to last literals */
					op = token;
					goto _last_literals;
				}

				/* Encode Offset */
				if (maybe_extMem) {   /* static test */
					debug (6) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `             with offset=%u  (ext if > %d)`, offset, cast(int)(ip - cast(const (ubyte)*)(source)));
					}

					assert(offset <= .MAX_DISTANCE && offset > 0);
					lz4_lib.lz4common.writeLE16(op, cast(ushort)(offset));
					op += 2;
				} else {
					debug (6) {
						lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `             with offset=%u  (same segment)`, cast(uint)(ip - match));
					}

					assert(ip - match <= .MAX_DISTANCE);
					lz4_lib.lz4common.writeLE16(op, cast(ushort)(ip - match));
					op += 2;
				}

				/* Encode MatchLength */
				{
					size_t matchCode;

					if ((dictDirective == .dict_directive.usingExtDict || dictDirective == .dict_directive.usingDictCtx) && (lowLimit == dictionary)) {
						const (ubyte)* limit = ip + (dictEnd - match);
						assert(dictEnd > match);

						if (limit > matchlimit) {
							limit = matchlimit;
						}

						matchCode = .LZ4_count(ip + .MINMATCH, match + .MINMATCH, limit);
						ip += .MINMATCH + matchCode;

						if (ip == limit) {
							const size_t more = .LZ4_count(limit, cast(const (ubyte)*)(source), matchlimit);
							matchCode += more;
							ip += more;
						}

						debug (6) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `             with matchLength=%u starting in extDict`, matchCode + .MINMATCH);
						}
					} else {
						matchCode = .LZ4_count(ip + .MINMATCH, match + .MINMATCH, matchlimit);
						ip += .MINMATCH + matchCode;

						debug(6) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `             with matchLength=%u`, matchCode + .MINMATCH);
						}
					}

					if ((outputLimited) && (lz4_lib.lz4common.unlikely(op + (1 + .LASTLITERALS) + (matchCode >> 8) > olimit))) {
						if (outputLimited == .limitedOutput_directive.limitedOutput) {
							goto _failure;
						}

						if (outputLimited == .limitedOutput_directive.fillOutput) {
							/* Match description too long : reduce it */
							size_t newMatchCode = 15 /* in token */ - 1 /* to avoid needing a zero byte */ + (cast(size_t)(olimit - op) - 2 - 1 - .LASTLITERALS) * 255;
							ip -= matchCode - newMatchCode;
							matchCode = newMatchCode;
						}
					}

					if (matchCode >= .ML_MASK) {
						*token += .ML_MASK;
						matchCode -= .ML_MASK;
						lz4_lib.lz4common.write32(op, 0xFFFFFFFF);

						while (matchCode >= 4 * 255) {
							op += 4;
							lz4_lib.lz4common.write32(op, 0xFFFFFFFF);
							matchCode -= 4 * 255;
						}

						op += matchCode / 255;
						*op++ = cast(ubyte)(matchCode % 255);
					} else {
						*token += cast(ubyte)(matchCode);
					}
				}

				anchor = ip;

				/* Test end of chunk */
				if (ip >= mflimitPlusOne) {
					break;
				}

				/* Fill table */
				.LZ4_putPosition(ip - 2, &this.hashTable, tableType, base);

				/* Test next position */
				if (tableType == .tableType_t.byPtr) {
					match = .LZ4_getPosition(ip, &this.hashTable, tableType, base);
					.LZ4_putPosition(ip, &this.hashTable, tableType, base);

					if ((match + .MAX_DISTANCE >= ip) && (lz4_lib.lz4common.read32(match) == lz4_lib.lz4common.read32(ip))) {
						token = op++;
						*token = 0;
						goto _next_match;
					}
				} else {   /* tableType_t.byU32, tableType_t.byU16 */

					const uint h = .LZ4_hashPosition(ip, tableType);
					const uint current = cast(uint)(ip - base);
					uint matchIndex = .LZ4_getIndexOnHash(h, &this.hashTable, tableType);
					assert(matchIndex < current);

					if (dictDirective == .dict_directive.usingDictCtx) {
						if (matchIndex < startIndex) {
							/* there was no match, try the dictionary */
							matchIndex = .LZ4_getIndexOnHash(h, &((*dictCtx).hashTable), .tableType_t.byU32);
							match = dictBase + matchIndex;

							/* required for match length counter */
							lowLimit = dictionary;

							matchIndex += dictDelta;
						} else {
							match = base + matchIndex;

							/* required for match length counter */
							lowLimit = cast(const (ubyte)*)(source);
						}
					} else if (dictDirective == .dict_directive.usingExtDict) {
						if (matchIndex < startIndex) {
							match = dictBase + matchIndex;

							/* required for match length counter */
							lowLimit = dictionary;
						} else {
							match = base + matchIndex;

							/* required for match length counter */
							lowLimit = cast(const (ubyte)*)(source);
						}
					} else {   /* single memory segment */
						match = base + matchIndex;
					}

					.LZ4_putIndexOnHash(current, h, &this.hashTable, tableType);
					assert(matchIndex < current);

					if (((dictIssue == .dictIssue_directive.dictSmall) ? (matchIndex >= prefixIdxLimit) : (1)) && ((tableType == .tableType_t.byU16) ? (1) : (matchIndex + .MAX_DISTANCE >= current)) && (lz4_lib.lz4common.read32(match) == lz4_lib.lz4common.read32(ip))) {
						token = op++;
						*token = 0;

						if (maybe_extMem) {
							offset = current - matchIndex;
						}

						debug (6) {
							lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `seq.start:%d, literals=%u, match.start:%d`, cast(int)(anchor - cast(const (ubyte)*)(source)), 0, cast(int)(ip - cast(const (ubyte)*)(source)));
						}

						goto _next_match;
					}
				}

				/* Prepare next loop */
				forwardH = .LZ4_hashPosition(++ip, tableType);
			}

		_last_literals:
			/* Encode Last Literals */
			{
				size_t lastRun = cast(size_t)(iend - anchor);

				if ((outputLimited) &&  /* Check output buffer overflow */
						(op + lastRun + 1 + ((lastRun + 255 - .RUN_MASK) / 255) > olimit)) {
					if (outputLimited == .limitedOutput_directive.fillOutput) {
						/* adapt lastRun to fill 'dst' */
						lastRun = (olimit - op) - 1;
						lastRun -= (lastRun + 240) / 255;
					}

					if (outputLimited == .limitedOutput_directive.limitedOutput) {
						goto _failure;
					}
				}

				if (lastRun >= .RUN_MASK) {
					size_t accumulator = lastRun - .RUN_MASK;
					*op++ = .RUN_MASK << .ML_BITS;

					for (; accumulator >= 255 ; accumulator -= 255) {
						*op++ = 255;
					}

					*op++ = cast(ubyte)(accumulator);
				} else {
					*op++ = cast(ubyte)(lastRun << .ML_BITS);
				}

				core.stdc.string.memcpy(op, anchor, lastRun);
				ip = anchor + lastRun;
				op += lastRun;
			}

			if (outputLimited == .limitedOutput_directive.fillOutput) {
				*inputConsumed = cast(int)((cast(const (char)*)(ip)) - source);
			}

			debug (5) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_generic: compressed %d bytes into %d bytes`, inputSize, cast(int)((cast(char*)(op)) - dest));
			}

			result = cast(int)((cast(char*)(op)) - dest);

			return result;
		_failure:
			/* Mark stream as having dirty context, so, it has to be fully reset */
			this.dirty = 1;

			return 0;
		}

	pure nothrow @nogc
	package void renormDictT(const int nextSize)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			if (this.currentOffset + nextSize > 0x80000000) {   /* potential ptrdiff_t overflow (32-bits mode) */
				/* rescale hash table */
				const uint delta = this.currentOffset - lz4_lib.lz4common.KB!(uint, 64);
				const (ubyte)* dictEnd = this.dictionary + this.dictSize;

				debug(4) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `renormDictT`);
				}

				for (size_t i = 0; i < LZ4_HASH_SIZE_U32; i++) {
					if (this.hashTable[i] < delta) {
						this.hashTable[i] = 0;
					} else {
						this.hashTable[i] -= delta;
					}
				}

				this.currentOffset = lz4_lib.lz4common.KB!(uint, 64);

				if (this.dictSize > lz4_lib.lz4common.KB!(uint, 64)) {
					this.dictSize = lz4_lib.lz4common.KB!(uint, 64);
				}

				this.dictionary = dictEnd - this.dictSize;
			}
		}
}

package struct LZ4_streamDecode_t_internal
{
	const (ubyte)* externalDict;
	const (ubyte)* prefixEnd;
	size_t extDictSize;
	size_t prefixSize;
}

/**
 * LZ4_stream_t :
 *  information structure to track an LZ4 stream.
 *  init this structure with resetStream() before first use.
 *  note : only use in association with static linking !
 *         this definition is not API/ABI safe,
 *         it may change in a future version !
 */
package enum LZ4_STREAMSIZE_U64 = (1 << (.LZ4_MEMORY_USAGE - 3)) + 4 + (((void*).sizeof == 16) ? (4) : (0));
package enum LZ4_STREAMSIZE = .LZ4_STREAMSIZE_U64 * ulong.sizeof;

public union LZ4_stream_t
{
public:
	ulong[.LZ4_STREAMSIZE_U64] table;

package:
	.LZ4_stream_t_internal internal_donotuse;

	/**
	 * compress_fast_extState() :
	 *  Same as compress_fast(), using an externally allocated memory space for its state.
	 *  Use LZ4_sizeofState() to know how much memory must be allocated,
	 *  and allocate it on 8-bytes boundaries (using `malloc()` typically).
	 *  Then, provide this buffer as `void* state` to compression function.
	 */
	 //LZ4LIB_API
	pure nothrow @nogc
	public int compress_fast_extState(const char* source, const char* dest, const int inputSize, const int maxOutputSize, int acceleration)

		in
		{
		}

		do
		{
			if (acceleration < 1) {
				acceleration = .ACCELERATION_DEFAULT;
			}

			this.resetStream();

			if (maxOutputSize >= .LZ4_compressBound(inputSize)) {
				if (inputSize < .LZ4_64Klimit) {
					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, 0, .limitedOutput_directive.notLimited, .tableType_t.byU16, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				} else {
					const .tableType_t tableType = ((((void*).sizeof) == 4) && (cast(size_t)(source) > .MAX_DISTANCE)) ? (.tableType_t.byPtr) : (.tableType_t.byU32);

					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, 0, .limitedOutput_directive.notLimited, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				}
			} else {
				if (inputSize < .LZ4_64Klimit) {
					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, .tableType_t.byU16, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				} else {
					const .tableType_t tableType = ((((void*).sizeof) == 4) && (cast(size_t)(source) > .MAX_DISTANCE)) ? (.tableType_t.byPtr) : (.tableType_t.byU32);

					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				}
			}
		}

	/**
	 * compress_fast_extState_fastReset() :
	 *  A variant of compress_fast_extState().
	 *
	 *  Using this variant avoids an expensive initialization step.
	 *  It is only safe to call if the state buffer is known to be correctly initialized already
	 *  (see above comment on resetStream_fast() for a definition of "correctly initialized").
	 *  From a high level, the difference is that
	 *  this function initializes the provided state with a call to something like resetStream_fast()
	 *  while compress_fast_extState() starts with a call to resetStream().
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public int compress_fast_extState_fastReset(const (char)* src, const char* dst, const int srcSize, const int dstCapacity, int acceleration)

		in
		{
			assert(src != null);
		}

		do
		{
			if (acceleration < 1) {
				acceleration = .ACCELERATION_DEFAULT;
			}

			if (dstCapacity >= .LZ4_compressBound(srcSize)) {
				if (srcSize < .LZ4_64Klimit) {
					const .tableType_t tableType = .tableType_t.byU16;
					this.internal_donotuse.prepareTable(srcSize, tableType);

					if (this.internal_donotuse.currentOffset) {
						return this.internal_donotuse.compress_generic(src, dst, srcSize, null, 0, .limitedOutput_directive.notLimited, tableType, .dict_directive.noDict, .dictIssue_directive.dictSmall, acceleration);
					} else {
						return this.internal_donotuse.compress_generic(src, dst, srcSize, null, 0, .limitedOutput_directive.notLimited, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
					}
				} else {
					const .tableType_t tableType = ((((void*).sizeof) == 4) && (cast(size_t)(src) > .MAX_DISTANCE)) ? (.tableType_t.byPtr) : (.tableType_t.byU32);
					this.internal_donotuse.prepareTable(srcSize, tableType);

					return this.internal_donotuse.compress_generic(src, dst, srcSize, null, 0, .limitedOutput_directive.notLimited, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				}
			} else {
				if (srcSize < .LZ4_64Klimit) {
					const .tableType_t tableType = .tableType_t.byU16;
					this.internal_donotuse.prepareTable(srcSize, tableType);

					if (this.internal_donotuse.currentOffset) {
						return this.internal_donotuse.compress_generic(src, dst, srcSize, null, dstCapacity, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.noDict, .dictIssue_directive.dictSmall, acceleration);
					} else {
						return this.internal_donotuse.compress_generic(src, dst, srcSize, null, dstCapacity, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
					}
				} else {
					const .tableType_t tableType = ((((void*).sizeof) == 4) && (cast(size_t)(src) > .MAX_DISTANCE)) ? (.tableType_t.byPtr) : (.tableType_t.byU32);
					this.internal_donotuse.prepareTable(srcSize, tableType);

					return this.internal_donotuse.compress_generic(src, dst, srcSize, null, dstCapacity, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
				}
			}
		}

	/*
	 * Note!: This function leaves the stream in an unclean/broken state!
	 * It is not safe to subsequently use the same state with a _fastReset() or
	 * _continue() call without resetting it.
	 */
	pure nothrow @nogc
	package int compress_destSize_extState(const (char)* src, const char* dst, ref int srcSizePtr, const int targetDstSize)

		in
		{
			assert(src != null);
		}

		do
		{
			this.resetStream();

			if (targetDstSize >= .LZ4_compressBound(srcSizePtr)) {  /* compression success is guaranteed */
				return this.compress_fast_extState(src, dst, srcSizePtr, targetDstSize, 1);
			} else {
				if (srcSizePtr < .LZ4_64Klimit) {
					return this.internal_donotuse.compress_generic(src, dst, srcSizePtr, &srcSizePtr, targetDstSize, .limitedOutput_directive.fillOutput, .tableType_t.byU16, .dict_directive.noDict, .dictIssue_directive.noDictIssue, 1);
				} else {
					const .tableType_t addrMode = ((((void*).sizeof) == 4) && (cast(size_t)(src) > .MAX_DISTANCE)) ? (.tableType_t.byPtr) : (.tableType_t.byU32);

					return this.internal_donotuse.compress_generic(src, dst, srcSizePtr, &srcSizePtr, targetDstSize, .limitedOutput_directive.fillOutput, addrMode, .dict_directive.noDict, .dictIssue_directive.noDictIssue, 1);
				}
			}
		}

	/**
	 * resetStream() :
	 *  An LZ4_stream_t structure can be allocated once and re-used multiple times.
	 *  Use this function to start compressing a new stream.
	 */
	//LZ4LIB_API
	pure nothrow @safe @nogc
	public void resetStream()

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			debug (5) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `resetStream (ctx:%X)`, &this);
			}

			this = .LZ4_stream_t.init;
		}

	/**
	 * resetStream_fast() :
	 *  Use this to prepare a context for a new chain of calls to a streaming API
	 *  (e.g., LZ4_compress_fast_continue()).
	 *
	 *  Note:
	 *  To stay on the safe side, when LZ4_stream_t is used for the first time,
	 *  it should be either created using LZ4_createStream() or
	 *  initialized using LZ4_resetStream().
	 *
	 *  Note:
	 *  Using this in advance of a non-streaming-compression function is redundant,
	 *  since they all perform their own custom reset internally.
	 *
	 *  Differences from resetStream():
	 *  When an LZ4_stream_t is known to be in an internally coherent state,
	 *  it will be prepared for a new compression with almost no work.
	 *  Otherwise, it will fall back to the full, expensive reset.
	 *
	 *  LZ4_streams are guaranteed to be in a valid state when:
	 *  - returned from LZ4_createStream()
	 *  - reset by LZ4_resetStream()
	 *  - memset(stream, 0, sizeof(LZ4_stream_t)), though this is discouraged
	 *  - the stream was in a valid state and was reset by resetStream_fast()
	 *  - the stream was in a valid state and was then used in any compression call
	 *    that returned success
	 *  - the stream was in an indeterminate state and was used in a compression
	 *    call that fully reset the state (e.g., compress_fast_extState()) and
	 *    that returned success
	 *
	 *  Note:
	 *  A stream that was used in a compression call that did not return success
	 *  (e.g., compress_fast_continue()), can still be passed to this function,
	 *  however, it's history is not preserved because of previous compression
	 *  failure.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public void resetStream_fast()

		in
		{
		}

		do
		{
			this.internal_donotuse.prepareTable(0, .tableType_t.byU32);
		}

	/**
	 * loadDict() :
	 *  Use this function to load a static dictionary into LZ4_stream_t.
	 *  Any previous data will be forgotten, only 'dictionary' will remain in memory.
	 *  Loading a size of 0 is allowed, and is the same as reset.
	 *
	 * Returns: dictionary size, in bytes (necessarily <= 64 KB)
	 */
	//LZ4LIB_API
	pure nothrow @nogc
	public int loadDict(const (char)* dictionary, const int dictSize)

		in
		{
			assert(dictionary != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			const .tableType_t tableType = .tableType_t.byU32;
			const (ubyte)* p = cast(const (ubyte)*)(dictionary);
			const ubyte* dictEnd = p + dictSize;
			const (ubyte)* base;

			debug (4) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `loadDict (%d bytes from %X into %X)`, dictSize, dictionary, &this);
			}

			/*
			 * It's necessary to reset the context,
			 * and not just continue it with prepareTable()
			 * to avoid any risk of generating overflowing matchIndex
			 * when compressing using this dictionary
			 */
			this.resetStream();

			/*
			 * We always increment the offset by 64 KB, since, if the dict is longer,
			 * we truncate it to the last 64k, and if it's shorter, we still want to
			 * advance by a whole window length so we can provide the guarantee that
			 * there are only valid offsets in the window, which allows an optimization
			 * in compress_fast_continue() where it uses dictIssue_directive.noDictIssue even when the
			 * dictionary isn't a full 64k.
			 */

			if ((dictEnd - p) > lz4_lib.lz4common.KB!(uint, 64)) {
				p = dictEnd - lz4_lib.lz4common.KB!(uint, 64);
			}

			base = dictEnd - lz4_lib.lz4common.KB!(uint, 64) - this.internal_donotuse.currentOffset;
			this.internal_donotuse.dictionary = p;
			this.internal_donotuse.dictSize = cast(uint)(dictEnd - p);
			this.internal_donotuse.currentOffset += lz4_lib.lz4common.KB!(uint, 64);
			this.internal_donotuse.tableType = tableType;

			if (dictSize < cast(int)(size_t.sizeof)) {
				return 0;
			}

			while (p <= dictEnd - size_t.sizeof) {
				.LZ4_putPosition(p, &(this.internal_donotuse.hashTable), tableType, base);
				p += 3;
			}

			return this.internal_donotuse.dictSize;
		}

	/**
	 * attach_dictionary() :
	 *  This is an experimental API that allows
	 *  efficient use of a static dictionary many times.
	 *
	 *  Rather than re-loading the dictionary buffer into a working context before
	 *  each compression, or copying a pre-loaded dictionary's LZ4_stream_t into a
	 *  working LZ4_stream_t, this function introduces a no-copy setup mechanism,
	 *  in which the working stream references the dictionary stream in-place.
	 *
	 *  Several assumptions are made about the state of the dictionary stream.
	 *  Currently, only streams which have been prepared by loadDict() should
	 *  be expected to work.
	 *
	 *  Alternatively, the provided dictionaryStream may be NULL,
	 *  in which case any existing dictionary stream is unset.
	 *
	 *  If a dictionary is provided, it replaces any pre-existing stream history.
	 *  The dictionary contents are the only history that can be referenced and
	 *  logically immediately precede the data compressed in the first subsequent
	 *  compression call.
	 *
	 *  The dictionary will only remain attached to the working stream through the
	 *  first compression call, at the end of which it is cleared. The dictionary
	 *  stream (and source buffer) must remain in-place / accessible / unchanged
	 *  through the completion of the first compression call on the stream.
	 */
	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public void attach_dictionary(const .LZ4_stream_t* dictionaryStream)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			/*
			 * Calling LZ4_resetStream_fast() here makes sure that changes will not be
			 * erased by subsequent calls to LZ4_resetStream_fast() in case stream was
			 * marked as having dirty context, e.g. requiring full reset.
			 */
			this.resetStream_fast();

			if (dictionaryStream != null) {
				/*
				 * If the current offset is zero, we will never look in the
				 * external dictionary context, since there is no value a table
				 * entry can take that indicate a miss. In that case, we need
				 * to bump the offset to something non-zero.
				 */
				if (this.internal_donotuse.currentOffset == 0) {
					this.internal_donotuse.currentOffset = lz4_lib.lz4common.KB!(uint, 64);
				}

				this.internal_donotuse.dictCtx = cast(.LZ4_stream_t_internal*)(&((*dictionaryStream).internal_donotuse));
			} else {
				this.internal_donotuse.dictCtx = null;
			}
		}

	//LZ4LIB_STATIC_API
	pure nothrow @nogc
	public void attach_dictionary(const ref .LZ4_stream_t dictionaryStream)

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			if (this.internal_donotuse.currentOffset == 0) {
				this.internal_donotuse.currentOffset = lz4_lib.lz4common.KB!(uint, 64);
			}

			this.internal_donotuse.dictCtx = cast(.LZ4_stream_t_internal*)(&dictionaryStream.internal_donotuse);
		}

	/**
	 * compress_fast_continue() :
	 *  Compress 'src' content using data from previously compressed blocks, for better compression ratio.
	 * 'dst' buffer must be already allocated.
	 *  If dstCapacity >= LZ4_compressBound(srcSize), compression is guaranteed to succeed, and runs faster.
	 *
	 *  Note 1 : Each invocation to compress_fast_continue() generates a new block.
	 *           Each block has precise boundaries.
	 *           Each block must be decompressed separately, calling LZ4_decompress_*() with relevant metadata.
	 *           It's not possible to append blocks together and expect a single invocation of LZ4_decompress_*() to decompress them together.
	 *
	 *  Note 2 : The previous 64KB of source data is __assumed__ to remain present, unmodified, at same address in memory !
	 *
	 *  Note 3 : When input is structured as a double-buffer, each buffer can have any size, including < 64 KB.
	 *           Make sure that buffers are separated, by at least one byte.
	 *           This construction ensures that each block only depends on previous block.
	 *
	 *  Note 4 : If input buffer is a ring-buffer, it can have any size, including < 64 KB.
	 *
	 *  Note 5 : After an error, the stream status is undefined (invalid), it can only be reset or freed.
	 */
	//LZ4LIB_API
	pure nothrow @nogc @system
	public int compress_fast_continue(const char* source, const char* dest, const int inputSize, const int maxOutputSize, int acceleration)

		in
		{
			assert(source != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			const .tableType_t tableType = .tableType_t.byU32;
			const (ubyte)* dictEnd = this.internal_donotuse.dictionary + this.internal_donotuse.dictSize;

			debug (5) {
				lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_fast_continue (inputSize=%d)`, inputSize);
			}

			if (this.internal_donotuse.dirty) {
				/* Uninitialized structure detected */
				return 0;
			}

			/* avoid index overflow */
			this.internal_donotuse.renormDictT(inputSize);

			if (acceleration < 1) {
				acceleration = .ACCELERATION_DEFAULT;
			}

			/* invalidate tiny dictionaries */
			if ((this.internal_donotuse.dictSize - 1 < 4 - 1) && (dictEnd != cast(const (ubyte)*)(source))) {
				debug (5) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `compress_fast_continue: dictSize(%u) at addr:%X is too small`, this.internal_donotuse.dictSize, this.internal_donotuse.dictionary);
				}

				this.internal_donotuse.dictSize = 0;
				this.internal_donotuse.dictionary = cast(const (ubyte)*)(source);
				dictEnd = cast(const (ubyte)*)(source);
			}

			/* Check overlapping input/dictionary space */
			{
				const (ubyte)* sourceEnd = cast(const (ubyte)*)(source) + inputSize;

				if ((sourceEnd > this.internal_donotuse.dictionary) && (sourceEnd < dictEnd)) {
					this.internal_donotuse.dictSize = cast(uint)(dictEnd - sourceEnd);

					if (this.internal_donotuse.dictSize > lz4_lib.lz4common.KB!(uint, 64)) {
						this.internal_donotuse.dictSize = lz4_lib.lz4common.KB!(uint, 64);
					}

					if (this.internal_donotuse.dictSize < 4) {
						this.internal_donotuse.dictSize = 0;
					}

					this.internal_donotuse.dictionary = dictEnd - this.internal_donotuse.dictSize;
				}
			}

			/* prefix mode : source data follows dictionary */
			if (dictEnd == cast(const (ubyte)*)(source)) {
				if ((this.internal_donotuse.dictSize < lz4_lib.lz4common.KB!(uint, 64)) && (this.internal_donotuse.dictSize < this.internal_donotuse.currentOffset)) {
					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.withPrefix64k, .dictIssue_directive.dictSmall, acceleration);
				} else {
					return this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.withPrefix64k, .dictIssue_directive.noDictIssue, acceleration);
				}
			}

			/* external dictionary mode */
			{
				int result;

				if (this.internal_donotuse.dictCtx) {
					/*
					 * We depend here on the fact that dictCtx'es (produced by
					 * loadDict) guarantee that their tables contain no references
					 * to offsets between (*dictCtx).currentOffset - 64 KB and
					 * (*dictCtx).currentOffset - (*dictCtx).dictSize. This makes it safe
					 * to use dictIssue_directive.noDictIssue even when the dict isn't a full 64 KB.
					 */
					if (inputSize > lz4_lib.lz4common.KB!(int, 4)) {
						/*
						 * For compressing large blobs, it is faster to pay the setup
						 * cost to copy the dictionary's tables into the active context,
						 * so that the compression loop is only looking into one table.
						 */
						core.stdc.string.memcpy(&this.internal_donotuse, this.internal_donotuse.dictCtx, .LZ4_stream_t.sizeof);
						result = this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.usingExtDict, .dictIssue_directive.noDictIssue, acceleration);
					} else {
						result = this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.usingDictCtx, .dictIssue_directive.noDictIssue, acceleration);
					}
				} else {
					if ((this.internal_donotuse.dictSize < lz4_lib.lz4common.KB!(uint, 64)) && (this.internal_donotuse.dictSize < this.internal_donotuse.currentOffset)) {
						result = this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.usingExtDict, .dictIssue_directive.dictSmall, acceleration);
					} else {
						result = this.internal_donotuse.compress_generic(source, dest, inputSize, null, maxOutputSize, .limitedOutput_directive.limitedOutput, tableType, .dict_directive.usingExtDict, .dictIssue_directive.noDictIssue, acceleration);
					}
				}

				this.internal_donotuse.dictionary = cast(const (ubyte)*)(source);
				this.internal_donotuse.dictSize = cast(uint)(inputSize);

				return result;
			}
		}

	/* Hidden debug function, to force-test external dictionary mode */
	pure nothrow @nogc
	int compress_forceExtDict(const char* source, const char* dest, const int srcSize)

		in
		{
			assert(source != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			int result;

			this.internal_donotuse.renormDictT(srcSize);

			if ((this.internal_donotuse.dictSize < lz4_lib.lz4common.KB!(uint, 64)) && (this.internal_donotuse.dictSize < this.internal_donotuse.currentOffset)) {
				result = this.internal_donotuse.compress_generic(source, dest, srcSize, null, 0, .limitedOutput_directive.notLimited, .tableType_t.byU32, .dict_directive.usingExtDict, .dictIssue_directive.dictSmall, 1);
			} else {
				result = this.internal_donotuse.compress_generic(source, dest, srcSize, null, 0, .limitedOutput_directive.notLimited, .tableType_t.byU32, .dict_directive.usingExtDict, .dictIssue_directive.noDictIssue, 1);
			}

			this.internal_donotuse.dictionary = cast(const (ubyte)*)(source);
			this.internal_donotuse.dictSize = cast(uint)(srcSize);

			return result;
		}

	/**
	 * saveDict() :
	 *  If last 64KB data cannot be guaranteed to remain available at its current memory location,
	 *  save it into a safer place (char* safeBuffer).
	 *  This is schematically equivalent to a memcpy() followed by loadDict(),
	 *  but is much faster, because saveDict() doesn't need to rebuild tables.
	 *
	 * Returns: saved dictionary size in bytes (necessarily <= maxDictSize), or 0 if error.
	 */
	/**
	 * saveDict() :
	 *  If previously compressed data block is not guaranteed to remain available at its memory location,
	 *  save it into a safer place (char* safeBuffer).
	 *  Note : you don't need to call loadDict() afterwards,
	 *         dictionary is immediately usable, you can therefore call compress_fast_continue().
	 *  Return : saved dictionary size in bytes (necessarily <= dictSize), or 0 if error.
	 */
	//LZ4LIB_API
	pure nothrow @nogc @system
	public int saveDict(char* safeBuffer, int dictSize)

		in
		{
			assert(safeBuffer != null);
		}

		do
		{
			static import core.stdc.string;
			static import lz4_lib.lz4common;

			const ubyte* previousDictEnd = this.internal_donotuse.dictionary + this.internal_donotuse.dictSize;

			if (cast(uint)(dictSize) > lz4_lib.lz4common.KB!(uint, 64)) {
				/* useless to define a dictionary > 64 KB */
				dictSize = lz4_lib.lz4common.KB!(uint, 64);
			}

			if (cast(uint)(dictSize) > this.internal_donotuse.dictSize) {
				dictSize = this.internal_donotuse.dictSize;
			}

			core.stdc.string.memmove(safeBuffer, previousDictEnd - dictSize, dictSize);

			this.internal_donotuse.dictionary = cast(const (ubyte)*)(safeBuffer);
			this.internal_donotuse.dictSize = cast(uint)(dictSize);

			return dictSize;
		}
}

/**
 * LZ4_streamDecode_t :
 *  information structure to track an LZ4 stream during decompression.
 *  init this structure  using setStreamDecode() before first use.
 *  note : only use in association with static linking !
 *         this definition is not API/ABI safe,
 *         and may change in a future version !
 */
package enum LZ4_STREAMDECODESIZE_U64 = 4 + (((void*).sizeof == 16) ? (2) : (0));
package enum LZ4_STREAMDECODESIZE = .LZ4_STREAMDECODESIZE_U64 * ulong.sizeof;

public union LZ4_streamDecode_t
{
public:
	ulong[.LZ4_STREAMDECODESIZE_U64] table;

package:
	.LZ4_streamDecode_t_internal internal_donotuse;

	/**
	 * setStreamDecode() :
	 *  An LZ4_streamDecode_t context can be allocated once and re-used multiple times.
	 *  Use this function to start decompression of a new stream of blocks.
	 *  A dictionary can optionally be set. Use null or size 0 for a reset order.
	 *  Dictionary is presumed stable : it must remain accessible and unmodified during next decompression.
	 *
	 * Returns: 1 if OK, 0 if error
	 */
	/**
	 * setStreamDecode() :
	 *  Use this function to instruct where to find the dictionary.
	 *  This function is not necessary if previous data is still available where it was decoded.
	 *  Loading a size of 0 is allowed (same effect as no dictionary).
	 *
	 * Returns: 1 if OK, 0 if error
	 */
	//LZ4LIB_API
	pure nothrow @nogc
	public int setStreamDecode(const char* dictionary, const int dictSize)

		in
		{
			assert(dictionary != null);
		}

		do
		{
			this.internal_donotuse.prefixSize = cast(size_t)(dictSize);
			this.internal_donotuse.prefixEnd = cast(const ubyte*)(dictionary) + dictSize;
			this.internal_donotuse.externalDict = null;
			this.internal_donotuse.extDictSize  = 0;

			return 1;
		}

	/**
	 *_continue() :
	 *  These decoding functions allow decompression of multiple blocks in "streaming" mode.
	 *  Previously decoded blocks must still be available at the memory position where they were decoded.
	 *  If it's not possible, save the relevant part of decoded data into a safe buffer,
	 *  and indicate where it stands using setStreamDecode()
	 */
	/**
	 * LZ4_decompress_*_continue() :
	 *  These decoding functions allow decompression of consecutive blocks in "streaming" mode.
	 *  A block is an unsplittable entity, it must be presented entirely to a decompression function.
	 *  Decompression functions only accepts one block at a time.
	 *  The last 64KB of previously decoded data *must* remain available and unmodified at the memory position where they were decoded.
	 *  If less than 64KB of data has been decoded, all the data must be present.
	 *
	 *  Special : if decompression side sets a ring buffer, it must respect one of the following conditions :
	 *  - Decompression buffer size is _at least_ LZ4_decoderRingBufferSize(maxBlockSize).
	 *    maxBlockSize is the maximum size of any single block. It can have any value > 16 bytes.
	 *    In which case, encoding and decoding buffers do not need to be synchronized.
	 *    Actually, data can be produced by any source compliant with LZ4 format specification, and respecting maxBlockSize.
	 *  - Synchronized mode :
	 *    Decompression buffer size is _exactly_ the same as compression buffer size,
	 *    and follows exactly same update rule (block boundaries at same positions),
	 *    and decoding function is provided with exact decompressed size of each block (exception for last block of the stream),
	 *    _then_ decoding & encoding ring buffer can have any size, including small ones ( < 64 KB).
	 *  - Decompression buffer is larger than encoding buffer, by a minimum of maxBlockSize more bytes.
	 *    In which case, encoding and decoding buffers do not need to be synchronized,
	 *    and encoding ring buffer can have any size, including small ones ( < 64 KB).
	 *
	 *  Whenever these conditions are not possible,
	 *  save the last 64KB of decoded data into a safe buffer where it can't be modified during decompression,
	 *  then indicate where this data is saved using setStreamDecode(), before decompressing next block.
	 */
	//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
	//LZ4LIB_API
	pragma(inline, true)
	pure nothrow @nogc
	public int decompress_safe_continue(const char* source, const char* dest, const int compressedSize, const int maxOutputSize)

		in
		{
			assert(dest != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			int result;

			if (this.internal_donotuse.prefixSize == 0) {
				/* The first call, no dictionary yet. */
				assert(this.internal_donotuse.extDictSize == 0);
				result = .LZ4_decompress_safe(source, dest, compressedSize, maxOutputSize);

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize = result;
				this.internal_donotuse.prefixEnd = cast(ubyte*)(dest) + result;
			} else if (this.internal_donotuse.prefixEnd == cast(ubyte*)(dest)) {
				/* They're rolling the current segment. */
				if (this.internal_donotuse.prefixSize >= lz4_lib.lz4common.KB!(size_t, 64) - 1) {
					result = .LZ4_decompress_safe_withPrefix64k(source, dest, compressedSize, maxOutputSize);
				} else if (this.internal_donotuse.extDictSize == 0) {
					result = .LZ4_decompress_safe_withSmallPrefix(source, dest, compressedSize, maxOutputSize, this.internal_donotuse.prefixSize);
				} else {
					result = .LZ4_decompress_safe_doubleDict(source, dest, compressedSize, maxOutputSize, this.internal_donotuse.prefixSize, this.internal_donotuse.externalDict, this.internal_donotuse.extDictSize);
				}

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize += result;
				this.internal_donotuse.prefixEnd += result;
			} else {
				/* The buffer wraps around, or they're switching to another buffer. */
				this.internal_donotuse.extDictSize = this.internal_donotuse.prefixSize;
				this.internal_donotuse.externalDict = this.internal_donotuse.prefixEnd - this.internal_donotuse.extDictSize;
				result = .LZ4_decompress_safe_forceExtDict(source, dest, compressedSize, maxOutputSize, this.internal_donotuse.externalDict, this.internal_donotuse.extDictSize);

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize = result;
				this.internal_donotuse.prefixEnd = cast(ubyte*)(dest) + result;
			}

			return result;
		}

	//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
	//LZ4LIB_API
	pragma(inline, true)
	pure nothrow @nogc
	public int decompress_fast_continue(const char* source, const char* dest, const int originalSize)

		in
		{
			assert(dest != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			int result;

			if (this.internal_donotuse.prefixSize == 0) {
				assert(this.internal_donotuse.extDictSize == 0);
				result = .LZ4_decompress_fast(source, dest, originalSize);

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize = originalSize;
				this.internal_donotuse.prefixEnd = cast(ubyte*)(dest) + originalSize;
			} else if (this.internal_donotuse.prefixEnd == cast(ubyte*)(dest)) {
				if (this.internal_donotuse.prefixSize >= lz4_lib.lz4common.KB!(size_t, 64) - 1 || this.internal_donotuse.extDictSize == 0) {
					result = .LZ4_decompress_fast(source, dest, originalSize);
				} else {
					result = .LZ4_decompress_fast_doubleDict(source, dest, originalSize, this.internal_donotuse.prefixSize, this.internal_donotuse.externalDict, this.internal_donotuse.extDictSize);
				}

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize += originalSize;
				this.internal_donotuse.prefixEnd += originalSize;
			} else {
				this.internal_donotuse.extDictSize = this.internal_donotuse.prefixSize;
				this.internal_donotuse.externalDict = this.internal_donotuse.prefixEnd - this.internal_donotuse.extDictSize;
				result = .LZ4_decompress_fast_extDict(source, dest, originalSize, this.internal_donotuse.externalDict, this.internal_donotuse.extDictSize);

				if (result <= 0) {
					return result;
				}

				this.internal_donotuse.prefixSize = originalSize;
				this.internal_donotuse.prefixEnd = cast(ubyte*)(dest) + originalSize;
			}

			return result;
		}
}

package alias LZ4_stream_u = .LZ4_stream_t;
package alias LZ4_streamDecode_u = .LZ4_streamDecode_t;

/*-************************************
*  Common Constants
**************************************/
enum MINMATCH = 4;

enum WILDCOPYLENGTH = 8;

/**
 * see ../doc/lz4_Block_format.md#parsing-restrictions
 */
enum LASTLITERALS = 5;

/**
 * see ../doc/lz4_Block_format.md#parsing-restrictions
 */
enum MFLIMIT = 12;

/**
 * ensure it's possible to write 2 x wildcopyLength without overflowing output buffer
 */
enum MATCH_SAFEGUARD_DISTANCE = (2 * .WILDCOPYLENGTH) - .MINMATCH;

enum FASTLOOP_SAFE_DISTANCE = 64;

enum int LZ4_minLength = .MFLIMIT + 1;

enum MAXD_LOG = 16;
enum MAX_DISTANCE = (1 << .MAXD_LOG) - 1;

enum ML_BITS = 4;
enum ML_MASK = (1u << .ML_BITS) - 1;
enum RUN_BITS = 8 - .ML_BITS;
enum RUN_MASK = (1u << .RUN_BITS) - 1;

/*-************************************
*  Error detection
**************************************/

/*-************************************
*  Common functions
**************************************/
version (LittleEndian) {
	version (X86_64) {
		//#if defined(_MSC_VER) && defined(_WIN64) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package uint NbCommonBytes(size_t val)
		//	{
		//		//	ulong r = 0;
		//		//	_BitScanForward64(&r, cast(ulong)(val));
		//		//	return cast(int)(r >> 3);
		//	}
		//#elif (defined(__clang__) || (defined(__GNUC__) && (__GNUC__>=3))) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package uint NbCommonBytes(size_t val)
		//	{
		//		return (__builtin_ctzll(cast(ulong)(val)) >> 3);
		//	}
		//#else
			static immutable size_t[64] DeBruijnBytePos =
			[
				0, 0, 0, 0, 0, 1, 1, 2,
				0, 3, 1, 3, 1, 4, 2, 7,
				0, 2, 3, 6, 1, 5, 3, 5,
				1, 3, 4, 4, 2, 5, 6, 7,
				7, 0, 1, 2, 3, 3, 4, 6,
				2, 6, 5, 5, 3, 4, 5, 6,
				7, 1, 2, 4, 6, 4, 4, 5,
				7, 2, 6, 5, 7, 6, 7, 7,
			];

			pure nothrow @safe @nogc
			package size_t NbCommonBytes(const size_t val)

				in
				{
				}

				do
				{

					return .DeBruijnBytePos[(cast(ulong)((val & -(cast(long)(val))) * 0x0218A392CDABBD3FUL)) >> 58];
				}
		//#endif
	} else {
		static assert(size_t.sizeof == 4);

		//#if defined(_MSC_VER) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package uint NbCommonBytes(size_t val)
		//	{
		//		ulong r;
		//		_BitScanForward(&r, cast(uint)(val));
		//		return cast(int)(r >> 3);
		//	}
		//#elif (defined(__clang__) || (defined(__GNUC__) && (__GNUC__>=3))) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package uint NbCommonBytes(size_t val)
		//	{
		//		return (__builtin_ctz(cast(uint)(val)) >> 3);
		//	}
		//#else
			static immutable size_t[32] DeBruijnBytePos =
			[
				0, 0, 3, 0, 3, 1, 3, 0,
				3, 2, 2, 1, 3, 2, 0, 1,
				3, 3, 1, 2, 2, 2, 2, 0,
				3, 1, 2, 0, 1, 0, 1, 1,
			];

			pure nothrow @safe @nogc
			package size_t NbCommonBytes(const size_t val)

				in
				{
				}

				do
				{

					return .DeBruijnBytePos[(cast(uint)((val & -(cast(int)(val))) * 0x077CB531u)) >> 27];
				}
		//#endif
	}
} else version (BigEndian) {
	version (X86_64) {
		//#if defined(_MSC_VER) && defined(_WIN64) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package size_t NbCommonBytes(size_t val)
		//	{
		//		ulong r = 0;
		//		_BitScanReverse64(&r, val);
		//		return cast(uint)(r >> 3);
		//	}
		//#elif (defined(__clang__) || (defined(__GNUC__) && (__GNUC__>=3))) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package size_t NbCommonBytes(size_t val)
		//	{
		//		return (__builtin_clzll(cast(ulong)(val)) >> 3);
		//	}
		//#else
			pure nothrow @safe @nogc
			package size_t NbCommonBytes(size_t val)

				in
				{
				}

				do
				{
					/*
					 * 32 on 64 bits (goal), 16 on 32 bits.
					 *  Just to avoid some static analyzer complaining about shift by 32 on 32-bits target.
					 *  Note that this code path is never triggered in 32-bits mode.
					 */
					enum uint by32 = val.sizeof * 4;

					uint r;

					if (!(val >> by32)) {
						r = 4;
					} else {
						r = 0;
						val >>= by32;
					}

					if (!(val >> 16)) {
						r += 2;
						val >>= 8;
					} else {
						val >>= 24;
					}

					r += (!val);

					return r;
				}
		//#endif
	} else {
		static assert(size_t.sizeof == 4);

		//#if defined(_MSC_VER) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package size_t NbCommonBytes(size_t val)
		//	{
		//		ulong r = 0;
		//		_BitScanReverse(&r, cast(ulong)(val));
		//		return cast(uint)(r >> 3);
		//	}
		//#elif (defined(__clang__) || (defined(__GNUC__) && (__GNUC__>=3))) && !defined(LZ4_FORCE_SW_BITCOUNT)
		//	package size_t NbCommonBytes(size_t val)
		//	{
		//		return (__builtin_clz(cast(uint)(val)) >> 3);
		//	}
		//#else
			pure nothrow @safe @nogc
			package size_t NbCommonBytes(size_t val)

				in
				{
				}

				do
				{
					uint r;

					if (!(val >> 16)) {
						r = 2;
						val >>= 8;
					} else {
						r = 0;
						val >>= 24;
					}

					r += (!val);

					return r;
				}
		//#endif
	}
} else {
	static assert(0);
}

pragma(inline, true)
pure nothrow @nogc
size_t LZ4_count(const (ubyte)* pIn, const (ubyte)* pMatch, const ubyte* pInLimit)

	in
	{
		assert(pIn != null);
		assert(pMatch != null);
		assert(pInLimit != null);
		static assert(size_t.max >= uint.max);
	}

	do
	{
		static import lz4_lib.lz4common;

		const ubyte* pStart = pIn;

		if (lz4_lib.lz4common.likely(pIn < pInLimit - (size_t.sizeof - 1))) {
			const size_t diff = lz4_lib.lz4common.read_ARCH(pMatch) ^ lz4_lib.lz4common.read_ARCH(pIn);

			if (!diff) {
				pIn += size_t.sizeof;
				pMatch += size_t.sizeof;
			} else {
				return .NbCommonBytes(diff);
			}
		}

		while (lz4_lib.lz4common.likely(pIn < pInLimit - (size_t.sizeof - 1))) {
			const size_t diff = lz4_lib.lz4common.read_ARCH(pMatch) ^ lz4_lib.lz4common.read_ARCH(pIn);

			if (!diff) {
				pIn += size_t.sizeof;
				pMatch += size_t.sizeof;
				continue;
			}

			pIn += .NbCommonBytes(diff);

			return cast(size_t)(pIn - pStart);
		}

		static if (size_t.sizeof == 8) {
			if ((pIn < (pInLimit - 3)) && (lz4_lib.lz4common.read32(pMatch) == lz4_lib.lz4common.read32(pIn))) {
				pIn += 4;
				pMatch += 4;
			}
		}

		if ((pIn < (pInLimit - 1)) && (lz4_lib.lz4common.read16(pMatch) == lz4_lib.lz4common.read16(pIn))) {
			pIn += 2;
			pMatch += 2;
		}

		if ((pIn < pInLimit) && (*pMatch == *pIn)) {
			pIn++;
		}

		return cast(size_t)(pIn - pStart);
	}

/*-************************************
*  Local Constants
**************************************/
enum int LZ4_64Klimit = lz4_lib.lz4common.KB!(int, 64) + (.MFLIMIT - 1);

/**
 * Increase this value ==> compression run slower on incompressible data
 */
enum uint LZ4_skipTrigger = 6;

/*-************************************
*  Local Structures and types
**************************************/
enum limitedOutput_directive
{
	noLimit = 0,
	notLimited = 1,
	limitedOutput = 2,
	fillOutput = 3,
	limitedDestSize = 4,
}

enum tableType_t
{
	clearedTable = 0,
	byPtr,
	byU32,
	byU16,
}

/**
 * This enum distinguishes several different modes of accessing previous
 * content in the stream.
 *
 * - noDict        : There is no preceding content.
 * - withPrefix64k : Table entries up to (*ctx).dictSize before the current blob
 *                   blob being compressed are valid and refer to the preceding
 *                   content (of length (*ctx).dictSize), which is available
 *                   contiguously preceding in memory the content currently
 *                   being compressed.
 * - usingExtDict  : Like withPrefix64k, but the preceding content is somewhere
 *                   else in memory, starting at (*ctx).dictionary with length
 *                   (*ctx).dictSize.
 * - usingDictCtx  : Like usingExtDict, but everything concerning the preceding
 *                   content is in a separate context, pointed to by
 *                   (*ctx).dictCtx. (*ctx).dictionary, (*ctx).dictSize, and table
 *                   entries in the current context that refer to positions
 *                   preceding the beginning of the current compression are
 *                   ignored. Instead, ctx->dictCtx->dictionary and ctx->dictCtx
 *                   ->dictSize describe the location and size of the preceding
 *                   content, and matches are found by looking in the ctx
 *                   ->dictCtx->hashTable.
 */
enum dict_directive
{
	noDict = 0,
	withPrefix64k,
	usingExtDict,
	usingDictCtx,
}

enum dictIssue_directive
{
	noDictIssue = 0,
	dictSmall,
}

/*-************************************
*  Local Utils
**************************************/
/**
 * library version number; useful to check dll version
 */
//LZ4LIB_API
pure nothrow @safe @nogc
public int LZ4_versionNumber()

	do
	{
		return .LZ4_VERSION_NUMBER;
	}

/**
 * library version string; useful to check dll version
 */
//LZ4LIB_API
pure nothrow @safe @nogc
public string LZ4_versionString()

	do
	{
		return .LZ4_VERSION_STRING;
	}

/**
 * 2_113_929_216 bytes
 */
enum LZ4_MAX_INPUT_SIZE = 0x7E000000;

/**
 * LZ4_compressBound() :
 *  Provides the maximum size that LZ4 compression may output in a "worst case" scenario (input data not compressible)
 *  This function is primarily useful for memory allocation purposes (destination buffer size).
 *  Macro LZ4_COMPRESSBOUND() is also provided for compilation-time evaluation (stack memory allocation for example).
 *  Note that LZ4_compress_default() compresses faster when dstCapacity is >= LZ4_compressBound(srcSize)
 *      inputSize  : max supported value is LZ4_MAX_INPUT_SIZE
 *      return : maximum output size in a "worst case" scenario
 *            or 0, if input size is incorrect (too large or negative)
 */
//LZ4LIB_API
pure nothrow @safe @nogc
public size_t LZ4_compressBound(const size_t isize)

	in
	{
		if (isize <= .LZ4_MAX_INPUT_SIZE && (isize > 271)) {
			assert((size_t.max - 16 - (isize / 255)) >= isize);
		}
	}

	do
	{
		return (isize > .LZ4_MAX_INPUT_SIZE) ? (0) : (isize + (isize / 255) + 16);
	}

//LZ4LIB_API
pure nothrow @safe @nogc
public size_t LZ4_sizeofState()

	in
	{
	}

	do
	{
		return .LZ4_STREAMSIZE;
	}

/*-************************************
*  Internal Definitions used in Tests
**************************************/
/*
pure nothrow @nogc
int compress_forceExtDict(const char* source, const char* dest, const int srcSize);
*/

pragma(inline, true)
pure nothrow @nogc
int LZ4_decompress_safe_forceExtDict(const char* source, const char* dest, const int compressedSize, const int maxOutputSize, const void* dictStart, const size_t dictSize);

/*-******************************
*  Compression functions
********************************/
pure nothrow @safe @nogc
package uint LZ4_hash4(const uint sequence, const .tableType_t tableType)

	in
	{
	}

	do
	{
		if (tableType == .tableType_t.byU16) {
			return ((sequence * 2654435761u) >> ((.MINMATCH * 8) - (.LZ4_HASHLOG + 1)));
		} else {
			return ((sequence * 2654435761u) >> ((.MINMATCH * 8) - .LZ4_HASHLOG));
		}
	}

pure nothrow @safe @nogc
package uint LZ4_hash5(const ulong sequence, const .tableType_t tableType)

	in
	{
	}

	do
	{
		const uint hashLog = (tableType == .tableType_t.byU16) ? (.LZ4_HASHLOG + 1) : (.LZ4_HASHLOG);

		version (LittleEndian) {
			enum ulong prime5bytes = 889523592379UL;

			return cast(uint)(((sequence << 24) * prime5bytes) >> (64 - hashLog));
		} else {
			enum ulong prime8bytes = 11400714785074694791UL;

			return cast(uint)(((sequence >> 24) * prime8bytes) >> (64 - hashLog));
		}
	}

pragma(inline, true)
pure nothrow @nogc
uint LZ4_hashPosition(const void* p, const .tableType_t tableType)

	in
	{
		assert(p != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		static if (size_t.sizeof == 8) {
			if (tableType != .tableType_t.byU16) {
				return .LZ4_hash5(lz4_lib.lz4common.read_ARCH(p), tableType);
			}
		}

		return .LZ4_hash4(lz4_lib.lz4common.read32(p), tableType);
	}

pure nothrow @nogc
package void LZ4_putIndexOnHash(const uint idx, const uint h, void* tableBase, const .tableType_t tableType)

	in
	{
		switch (tableType) {
			case .tableType_t.byU32:
			case .tableType_t.byU16:
				break;

			default:
				assert(0);
		}
	}

	do
	{
		switch (tableType) {
			case .tableType_t.byU32:
				{
					uint* hashTable = cast(uint*)(tableBase);
					hashTable[h] = idx;

					return;
				}

			case .tableType_t.byU16:
				{
					ushort* hashTable = cast(ushort*)(tableBase);
					assert(idx < 65536);
					hashTable[h] = cast(ushort)(idx);

					return;
				}

			//case .tableType_t.clearedTable:
			//case .tableType_t.byPtr:
			default:
				return;
		}
	}

pure nothrow @nogc
package void LZ4_putPositionOnHash(const ubyte* p, const uint h, void* tableBase, const .tableType_t tableType, const ubyte* srcBase)

	in
	{
		assert(p != null);
		assert(tableBase != null);
		assert(srcBase != null);

		switch (tableType) {
			case .tableType_t.byPtr:
			case .tableType_t.byU32:
			case .tableType_t.byU16:
				break;

			default:
				assert(0);
		}
	}

	do
	{
		switch (tableType) {
			case .tableType_t.byPtr:
				{
					const (ubyte)** hashTable = cast(const (ubyte)**)(tableBase);
					hashTable[h] = p;

					return;
				}

			case .tableType_t.byU32:
				{
					uint* hashTable = cast(uint*)(tableBase);
					hashTable[h] = cast(uint)(p - srcBase);

					return;
				}

			case .tableType_t.byU16:
				{
					ushort* hashTable = cast(ushort*)(tableBase);
					hashTable[h] = cast(ushort)(p - srcBase);

					return;
				}

			//case .tableType_t.clearedTable:
			default:
				return;
		}
	}

pragma(inline, true)
pure nothrow @nogc
void LZ4_putPosition(const ubyte* p, void* tableBase, const .tableType_t tableType, const ubyte* srcBase)

	in
	{
		assert(p != null);
		assert(tableBase != null);
		assert(srcBase != null);
	}

	do
	{
		const uint h = .LZ4_hashPosition(p, tableType);
		.LZ4_putPositionOnHash(p, h, tableBase, tableType, srcBase);
	}

/**
 * LZ4_getIndexOnHash() :
 * Index of match position registered in hash table.
 * hash position must be calculated by using base+index, or dictBase+index.
 * Assumption 1 : only valid if tableType == tableType_t.byU32 or tableType_t.byU16.
 * Assumption 2 : h is presumed valid (within limits of hash table)
 */
pure nothrow @nogc
package uint LZ4_getIndexOnHash(const uint h, const void* tableBase, const .tableType_t tableType)

	in
	{
		assert(tableBase != null);

		switch (tableType) {
			case .tableType_t.byU32:
			case .tableType_t.byU16:
				break;

			default:
				assert(0);
		}
	}

	do
	{
		static assert(.LZ4_MEMORY_USAGE > 2);

		switch (tableType) {
			case .tableType_t.byU32:
				const uint* hashTable = cast(const uint*)(tableBase);
				assert(h < (1u << (.LZ4_MEMORY_USAGE - 2)));

				return hashTable[h];

			case .tableType_t.byU16:
				const ushort* hashTable = cast(const ushort*)(tableBase);
				assert(h < (1u << (.LZ4_MEMORY_USAGE - 1)));

				return hashTable[h];

			default:
				/* forbidden case */
				return 0;
		}
	}

pure nothrow @nogc
package const (ubyte)* LZ4_getPositionOnHash(const uint h, const void* tableBase, const .tableType_t tableType, const ubyte* srcBase)

	in
	{
		assert(tableBase != null);
		assert(srcBase != null);
	}

	do
	{
		if (tableType == .tableType_t.byPtr) {
			const ubyte** hashTable = cast(const ubyte**)(tableBase);

			return hashTable[h];
		}

		if (tableType == .tableType_t.byU32) {
			const uint* hashTable = cast(const uint*)(tableBase);

			return hashTable[h] + srcBase;
		}

		{
			/* default, to ensure a return */
			const ushort* hashTable = cast(const ushort*)(tableBase);

			return hashTable[h] + srcBase;
		}
	}

pragma(inline, true)
pure nothrow @nogc
const (ubyte)* LZ4_getPosition(const ubyte* p, const void* tableBase, const .tableType_t tableType, const ubyte* srcBase)

	in
	{
		assert(p != null);
		assert(tableBase != null);
		assert(srcBase != null);
	}

	do
	{
		const uint h = .LZ4_hashPosition(p, tableType);

		return .LZ4_getPositionOnHash(h, tableBase, tableType, srcBase);
	}

/**
 * LZ4_compress_fast() :
 *  Same as LZ4_compress_default(), but allows selection of "acceleration" factor.
 *  The larger the acceleration value, the faster the algorithm, but also the lesser the compression.
 *  It's a trade-off. It can be fine tuned, with each successive value providing roughly +~3% to speed.
 *  An acceleration value of "1" is the same as regular LZ4_compress_default()
 *  Values <= 0 will be replaced by ACCELERATION_DEFAULT (currently == 1, see lz4.c).
 */
static if (LZ4_HEAPMODE) {
	//LZ4LIB_API
	nothrow @nogc @system
	public int LZ4_compress_fast(const char* source, const char* dest, const int inputSize, const int maxOutputSize, const int acceleration)

		in
		{
		}

		do
		{
			static import core.stdc.stdlib;

			/* malloc-calloc always properly aligned */
			.LZ4_stream_t* ctxPtr = cast(.LZ4_stream_t*)(core.stdc.stdlib.malloc(.LZ4_stream_t.sizeof));

			if (ctxPtr == null) {
				return 0;
			}

			scope (exit) {
				core.stdc.stdlib.free(ctxPtr);
				ctxPtr = null;
			}

			return (*ctxPtr).compress_fast_extState(source, dest, inputSize, maxOutputSize, acceleration);
		}
} else {
	//LZ4LIB_API
	pure nothrow @nogc
	public int LZ4_compress_fast(const char* source, const char* dest, const int inputSize, const int maxOutputSize, const int acceleration)

		in
		{
		}

		do
		{
			.LZ4_stream_t ctx;

			return ctx.compress_fast_extState(source, dest, inputSize, maxOutputSize, acceleration);
		}
}

/**
 * LZ4_compress_default() :
 *  Compresses 'srcSize' bytes from buffer 'src'
 *  into already allocated 'dst' buffer of size 'dstCapacity'.
 *  Compression is guaranteed to succeed if 'dstCapacity' >= LZ4_compressBound(srcSize).
 *  It also runs faster, so it's a recommended setting.
 *  If the function cannot compress 'src' into a more limited 'dst' budget,
 *  compression stops *immediately*, and the function result is zero.
 *  In which case, 'dst' content is undefined (invalid).
 *      srcSize : max supported value is LZ4_MAX_INPUT_SIZE.
 *      dstCapacity : size of buffer 'dst' (which must be already allocated)
 *     @return  : the number of bytes written into buffer 'dst' (necessarily <= dstCapacity)
 *                or 0 if compression fails
 *  Note : This function is protected against buffer overflow scenarios (never writes outside 'dst' buffer, nor read outside 'source' buffer).
 */
//LZ4LIB_API
pure nothrow @nogc
public int LZ4_compress_default(const char* src, const char* dst, const int srcSize, const int maxOutputSize)

	in
	{
	}

	do
	{
		return .LZ4_compress_fast(src, dst, srcSize, maxOutputSize, 1);
	}

//test
pure nothrow
public size_t LZ4_compress_default(const ref char[] src, ref char[] outbuf)
{
	int output_size = cast(int)(.LZ4_compressBound(src.length));

	if (outbuf.length != output_size) {
		outbuf = new char[output_size];
	}

	return .LZ4_compress_fast(cast(const char*)(&src[0]), cast(char*)(&outbuf[0]), cast(int)(src.length), output_size, 1);
}

/* hidden debug function */
/**
 * strangely enough, gcc generates faster code when this function is uncommented, even if unused
 */
pure nothrow @nogc
int LZ4_compress_fast_force(const char* src, const char* dst, const int srcSize, const int dstCapacity, const int acceleration)

	in
	{
	}

	do
	{
		.LZ4_stream_t ctx;
		ctx.resetStream();

		if (srcSize < .LZ4_64Klimit) {
			return ctx.internal_donotuse.compress_generic(src, dst, srcSize, null, dstCapacity, .limitedOutput_directive.limitedOutput, .tableType_t.byU16, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
		} else {
			const tableType_t addrMode = (((void*).sizeof) > 4) ? (.tableType_t.byU32) : (.tableType_t.byPtr);

			return ctx.internal_donotuse.compress_generic(src, dst, srcSize, null, dstCapacity, .limitedOutput_directive.limitedOutput, addrMode, .dict_directive.noDict, .dictIssue_directive.noDictIssue, acceleration);
		}
	}

/**
 * LZ4_compress_destSize() :
 *  Reverse the logic : compresses as much data as possible from 'src' buffer
 *  into already allocated buffer 'dst', of size >= 'targetDestSize'.
 *  This function either compresses the entire 'src' content into 'dst' if it's large enough,
 *  or fill 'dst' buffer completely with as much data as possible from 'src'.
 *  note: acceleration parameter is fixed to "default".
 *
 * *srcSizePtr : will be modified to indicate how many bytes where read from 'src' to fill 'dst'.
 *               New value is necessarily <= input value.
 *
 * Returns: Nb bytes written into 'dst' (necessarily <= targetDestSize) or 0 if compression fails.
 */
static if (LZ4_HEAPMODE) {
	//LZ4LIB_API
	nothrow @nogc @system
	public int LZ4_compress_destSize(const (char)* src, const char* dst, ref int srcSizePtr, const int targetDstSize)

		in
		{
		}

		do
		{
			static import core.stdc.stdlib;

			/* malloc-calloc always properly aligned */
			.LZ4_stream_t* ctx = cast(.LZ4_stream_t*)(core.stdc.stdlib.malloc(.LZ4_stream_t.sizeof));

			if (ctx == null) {
				return 0;
			}

			scope (exit) {
				core.stdc.stdlib.free(ctx);
				ctx = null;
			}

			return (*ctx).compress_destSize_extState(src, dst, srcSizePtr, targetDstSize);
		}
} else {
	//LZ4LIB_API
	pure nothrow @nogc
	public int LZ4_compress_destSize(const (char)* src, const char* dst, ref int srcSizePtr, const int targetDstSize)

		in
		{
		}

		do
		{
			.LZ4_stream_t ctxBody;

			return ctxBody.compress_destSize_extState(src, dst, srcSizePtr, targetDstSize);
		}
}

/*-******************************
*  Streaming functions
********************************/

/*-*******************************
 *  Decompression functions
 ********************************/

enum endCondition_directive
{
	endOnOutputSize = 0,
	endOnInputSize = 1,
}

enum earlyEnd_directive
{
	decode_full_block = 0,
	partial_decode = 1,
}

enum variable_length_error
{
	loop_error = -2,
	initial_error = -1,
	ok = 0,
}

/* Read the variable-length literal or match length.
 *
 * ip - pointer to use as input.
 * lencheck - end ip.  Return an error if ip advances >= lencheck.
 * loop_check - check ip >= lencheck in body of loop.  Returns loop_error if so.
 * initial_check - check ip >= lencheck before start of loop.  Returns initial_error if so.
 * error (output) - error code.  Should be set to 0 before call.
 */
pragma(inline, true)
pure nothrow @nogc
uint read_variable_length(ref const (ubyte)* ip, const ubyte* lencheck, int loop_check, int initial_check, ref variable_length_error error)

	do
	{
		uint length = 0;
		uint s;

		if (initial_check && lz4_lib.lz4common.unlikely(ip >= lencheck)) {
			/* overflow detection */
			error = .variable_length_error.initial_error;

			return length;
		}

		do {
			s = *ip;
			ip++;
			length += s;

			if (loop_check && lz4_lib.lz4common.unlikely(ip >= lencheck)) {
				/* overflow detection */
				error = .variable_length_error.loop_error;

				return length;
			}
		} while (s == 255);

		return length;
	}

/**
 * LZ4_decompress_generic() :
 *  This generic decompression function covers all use cases.
 *  It shall be instantiated several times, using different sets of directives.
 *  Note that it is important for performance that this function really get inlined,
 *  in order to remove useless branches during compilation optimization.
 *
 * Params:
 *      src = 
 *      dst = 
 *      srcSize = 
 *      outputSize = If endOnInput==endCondition_directive.endOnInputSize, this value is `dstCapacity`
 *      endOnInput = endCondition_directive.endOnOutputSize, endCondition_directive.endOnInputSize
 *      partialDecoding = full, partial
 *      dict = dict_directive.noDict, dict_directive.withPrefix64k, dict_directive.usingExtDict
 *      lowPrefix = always <= dst, == dst when no prefix
 *      dictStart = only if dict==dict_directive.usingExtDict
 *      dictSize = note : = 0 if dict_directive.noDict
 *
 * ToDo: dictStart
 */
pragma(inline, true)
pure nothrow @nogc @system
int LZ4_decompress_generic(const char* src, const char* dst, const int srcSize, const int outputSize, .endCondition_directive endOnInput, .earlyEnd_directive partialDecoding, .dict_directive dict, const ubyte* lowPrefix, const ubyte* dictStart, const size_t dictSize)

	in
	{
		assert(src != null);
		assert(dst != null);
		assert(lowPrefix != null);
		//assert(dictStart != null);
	}

	do
	{
		static import core.stdc.string;
		static import lz4_lib.lz4common;

		{
			if (src == null) {
				return -1;
			}

			{
				const (ubyte)* ip = cast(const (ubyte)*)(src);
				const ubyte* iend = ip + srcSize;

				ubyte* op = cast(ubyte*)(dst);
				const ubyte* oend = op + outputSize;
				ubyte* cpy;

				const ubyte* dictEnd = (dictStart == null) ? (null) : (dictStart + dictSize);

				const int safeDecode = (endOnInput == .endCondition_directive.endOnInputSize);
				const int checkOffset = (safeDecode) && (dictSize < lz4_lib.lz4common.KB!(int, 64));

				/* Set up the "end" pointers for the shortcut. */
				const ubyte* shortiend = iend - ((endOnInput) ? (14) : (8)) /*maxLL*/ - 2 /*offset*/;
				const ubyte* shortoend = oend - ((endOnInput) ? (14) : (8)) /*maxLL*/ - 18 /*maxML*/;

				const (ubyte)* match;
				size_t offset;
				uint token;
				size_t length;

				debug (5) {
					lz4_lib.lz4common.DEBUGLOG(__FILE__, __LINE__, `LZ4_decompress_generic (srcSize:%d, dstSize:%d)`, srcSize, outputSize);
				}

				/* Special cases */
				assert(lowPrefix <= op);

				if ((endOnInput) && (lz4_lib.lz4common.unlikely(outputSize == 0))) {
					/* Empty output buffer */
					return (((srcSize == 1) && (*ip == 0)) ? (0) : (-1));
				}

				if ((!endOnInput) && (lz4_lib.lz4common.unlikely(outputSize == 0))) {
					return ((*ip == 0) ? (1) : (-1));
				}

				if ((endOnInput) && lz4_lib.lz4common.unlikely(srcSize == 0)) {
					return -1;
				}

				/* Currently the fast loop shows a regression on qualcomm arm chips. */
				static if (lz4_lib.lz4common.i386_or_x86_64) {
					if ((oend - op) < .FASTLOOP_SAFE_DISTANCE) {
						goto safe_decode;
					}

					/* Fast loop : decode sequences as long as output < iend-.FASTLOOP_SAFE_DISTANCE */
					while (1) {
						/* Main fastloop assertion: We can always wildcopy .FASTLOOP_SAFE_DISTANCE */
						assert(oend - op >= .FASTLOOP_SAFE_DISTANCE);

						token = *ip++;
						length = token >> .ML_BITS;  /* literal length */

						assert(!endOnInput || ip <= iend); /* ip < iend before the increment */

						/* decode literal length */
						if (length == .RUN_MASK) {
							.variable_length_error error = .variable_length_error.ok;
							length += .read_variable_length(ip, iend - .RUN_MASK, endOnInput, endOnInput, error);

							if (error == .variable_length_error.initial_error) {
								goto _output_error;
							}

							if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(op) + length < cast(size_t)(op))) {
								/* overflow detection */
								goto _output_error;
							}

							if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(ip) + length < cast(size_t)(ip))) {
								/* overflow detection */
								goto _output_error;
							}

							/* copy literals */
							cpy = op + length;
							static assert(.MFLIMIT >= .WILDCOPYLENGTH);

							if (((endOnInput) && ((cpy > oend - .FASTLOOP_SAFE_DISTANCE) || (ip + length > iend - (2 + 1 + .LASTLITERALS)))) || ((!endOnInput) && (cpy > oend - .FASTLOOP_SAFE_DISTANCE)) ) {
								goto safe_literal_copy;
							}

							lz4_lib.lz4common.wildCopy32(op, ip, cpy);
							ip += length;
							op = cpy;
						} else {
							cpy = op + length;

							/* We don't need to check oend, since we check it once for each loop below */
							if ( ((endOnInput) && (ip + 16 > iend - (2 + 1 + .LASTLITERALS)))) {
								goto safe_literal_copy;
							}

							/* Literals can only be 14, but hope compilers optimize if we copy by a register size */
							core.stdc.string.memcpy(op, ip, 16);
							ip += length;
							op = cpy;
						}

						/* get offset */
						offset = lz4_lib.lz4common.readLE16(ip);
						ip += 2;
						match = op - offset;

						/* get matchlength */
						length = token & .ML_MASK;

						if ((checkOffset) && (lz4_lib.lz4common.unlikely(match + dictSize < lowPrefix))) {
							goto _output_error;    /* Error : offset outside buffers */
						}

						if (length == .ML_MASK) {
							.variable_length_error error = .variable_length_error.ok;
							length += .read_variable_length(ip, iend - .LASTLITERALS + 1, endOnInput, 0, error);

							if (error != .variable_length_error.ok) {
								goto _output_error;
							}

							if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(op) + length < cast(size_t)(op))) {
								/* overflow detection */
								goto _output_error;
							}

							length += .MINMATCH;

							if (op + length >= oend - .FASTLOOP_SAFE_DISTANCE) {
								goto safe_match_copy;
							}
						} else {
							length += .MINMATCH;

							if (op + length >= oend - .FASTLOOP_SAFE_DISTANCE) {
								goto safe_match_copy;
							}

							/* Fastpath check: Avoids a branch in wildCopy32 if true */
							if (!(dict == .dict_directive.usingExtDict) || (match >= lowPrefix)) {
								if (offset >= 8) {
									core.stdc.string.memcpy(op, match, 8);
									core.stdc.string.memcpy(op + 8, match + 8, 8);
									core.stdc.string.memcpy(op + 16, match + 16, 2);
									op += length;
									continue;
								}
							}
						}

						/* match starting within external dictionary */
						if ((dict == .dict_directive.usingExtDict) && (match < lowPrefix)) {
							if (lz4_lib.lz4common.unlikely(op + length > oend - .LASTLITERALS)) {
								if (partialDecoding) {
									length = lz4_lib.lz4common.MIN(length, cast(size_t)(oend - op));
								} else {
									/* doesn't respect parsing restriction */
									goto _output_error;
								}
							}

							if (length <= cast(size_t)(lowPrefix - match)) {
								/* match fits entirely within external dictionary : just copy */
								core.stdc.string.memmove(op, dictEnd - (lowPrefix - match), length);
								op += length;
							} else {
								/* match stretches into both external dictionary and current block */
								const size_t copySize = cast(size_t)(lowPrefix - match);
								const size_t restSize = length - copySize;
								core.stdc.string.memcpy(op, dictEnd - copySize, copySize);
								op += copySize;

								if (restSize > cast(size_t)(op - lowPrefix)) {  /* overlap copy */
									const ubyte* endOfMatch = op + restSize;
									const (ubyte)* copyFrom = lowPrefix;

									while (op < endOfMatch) {
										*op++ = *copyFrom++;
									}
								} else {
									core.stdc.string.memcpy(op, lowPrefix, restSize);
									op += restSize;
								}
							}

							continue;
						}

						/* copy match within block */
						cpy = op + length;

						/* partialDecoding : may not respect endBlock parsing restrictions */
						assert(op <= oend);

						if (lz4_lib.lz4common.unlikely(offset < 16)) {
							lz4_lib.lz4common.memcpy_using_offset(op, match, cpy, offset);
						} else {
							lz4_lib.lz4common.wildCopy32(op, match, cpy);
						}

						op = cpy;   /* wildcopy correction */
					}
			safe_decode:
				}

				/* Main Loop : decode remaining sequences where output < .FASTLOOP_SAFE_DISTANCE */
				while (1) {
					token = *ip++;

					/* literal length */
					length = token >> .ML_BITS;

					/* ip < iend before the increment */
					assert(!endOnInput || ip <= iend);

					/*
					 * A two-stage shortcut for the most common case:
					 * 1) If the literal length is 0..14, and there is enough space,
					 * enter the shortcut and copy 16 bytes on behalf of the literals
					 * (in the fast mode, only 8 bytes can be safely copied this way).
					 * 2) Further if the match length is 4..18, copy 18 bytes in a similar
					 * manner; but we ensure that there's enough space in the output for
					 * those 18 bytes earlier, upon entering the shortcut (in other words,
					 * there is a combined check for both stages).
					 */
					/* strictly "less than" on input, to re-enter the loop with at least one byte */
					if (((endOnInput) ? (length != .RUN_MASK) : (length <= 8)) && lz4_lib.lz4common.likely(((endOnInput) ? (ip < shortiend) : (1)) & (op <= shortoend))) {
						/* Copy the literals */
						core.stdc.string.memcpy(op, ip, ((endOnInput) ? (16) : (8)));
						op += length;
						ip += length;

						/*
						 * The second stage: prepare for match copying, decode full info.
						 * If it doesn't work out, the info won't be wasted.
						 */

						/* match length */
						length = token & .ML_MASK;

						offset = lz4_lib.lz4common.readLE16(ip);
						ip += 2;
						match = op - offset;

						/* check overflow */
						assert(match <= op);

						/* Do not deal with overlapping matches. */
						if ((length != .ML_MASK) && (offset >= 8) && (dict == .dict_directive.withPrefix64k || match >= lowPrefix)) {
							/* Copy the match. */
							core.stdc.string.memcpy(op + 0, match + 0, 8);
							core.stdc.string.memcpy(op + 8, match + 8, 8);
							core.stdc.string.memcpy(op + 16, match + 16, 2);
							op += length + .MINMATCH;
							/* Both stages worked, load the next token. */
							continue;
						}

						/*
						 * The second stage didn't work out, but the info is ready.
						 * Propel it right to the point of match copying.
						 */
						goto _copy_match;
					}

					/* decode literal length */
					if (length == .RUN_MASK) {
						.variable_length_error error = .variable_length_error.ok;
						length += .read_variable_length(ip, iend - .RUN_MASK, endOnInput, endOnInput, error);


						if (error == .variable_length_error.initial_error) {
							goto _output_error;
						}

						if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(op) + length < cast(size_t)(op))) {
							goto _output_error;    /* overflow detection */
						}

						if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(ip) + length < cast(size_t)(ip))) {
							goto _output_error;    /* overflow detection */
						}
					}

					/* copy literals */
					cpy = op + length;

					//static if (lz4_lib.lz4common.i386_or_x86_64) {
			safe_literal_copy:
					//}

					static assert(.MFLIMIT >= .WILDCOPYLENGTH);

					if (((endOnInput) && ((cpy > (oend - .MFLIMIT)) || (ip + length > iend - (2 + 1 + .LASTLITERALS)))) || ((!endOnInput) && (cpy > oend - .WILDCOPYLENGTH))) {
						if (partialDecoding) {
							if (cpy > oend) {
								/* Partial decoding : stop in the middle of literal segment */
								cpy = cast(ubyte*)(oend);
								length = oend - op;
							}

							if ((endOnInput) && (ip + length > iend)) {
								/* Error : read attempt beyond end of input buffer */
								goto _output_error;
							}
						} else {
							if ((!endOnInput) && (cpy != oend)) {
								/* Error : block decoding must stop exactly there */
								goto _output_error;
							}

							if ((endOnInput) && ((ip + length != iend) || (cpy > oend))) {
								/* Error : input must be consumed */
								goto _output_error;
							}
						}

						core.stdc.string.memcpy(op, ip, length);
						ip += length;
						op += length;

						if (!partialDecoding || (cpy == oend)) {
							/* Necessarily EOF, due to parsing restrictions */
							break;
						}
					} else {
						/* may overwrite up to WILDCOPYLENGTH beyond cpy */
						lz4_lib.lz4common.wildCopy(op, ip, cpy);
						ip += length;
						op = cpy;
					}

					/* get offset */
					offset = lz4_lib.lz4common.readLE16(ip);
					ip += 2;
					match = op - offset;

					/* get matchlength */
					length = token & .ML_MASK;

			_copy_match:

					if ((checkOffset) && (lz4_lib.lz4common.unlikely(match + dictSize < lowPrefix))) {
						/* Error : offset outside buffers */
						goto _output_error;
					}

					if (!partialDecoding) {
						assert(oend > op);
						assert((oend - op) >= 4);

						/* silence an msan warning when offset==0; costs <1%; */
						lz4_lib.lz4common.write32(op, 0);
					}   /* note : when partialDecoding, there is no guarantee that at least 4 bytes remain available in output buffer */

					if (length == .ML_MASK) {
						.variable_length_error error = .variable_length_error.ok;
						length += .read_variable_length(ip, iend - .LASTLITERALS + 1, endOnInput, 0, error);

						if (error != .variable_length_error.ok) {
							goto _output_error;
						}

						if ((safeDecode) && lz4_lib.lz4common.unlikely(cast(size_t)(op) + length < cast(size_t)(op))) {
							goto _output_error;    /* overflow detection */
						}
					}

					length += .MINMATCH;

					static if (lz4_lib.lz4common.i386_or_x86_64) {
			safe_match_copy:
					}

					/* match starting within external dictionary */
					if ((dict == .dict_directive.usingExtDict) && (match < lowPrefix)) {
						if (lz4_lib.lz4common.unlikely(op + length > oend - .LASTLITERALS)) {
							if (partialDecoding) {
								length = lz4_lib.lz4common.MIN(length, cast(size_t)(oend - op));
							} else {
								/* doesn't respect parsing restriction */
								goto _output_error;
							}
						}

						if (length <= cast(size_t)(lowPrefix - match)) {
							/* match fits entirely within external dictionary : just copy */
							core.stdc.string.memmove(op, dictEnd - (lowPrefix - match), length);
							op += length;
						} else {
							/* match stretches into both external dictionary and current block */
							const size_t copySize = cast(size_t)(lowPrefix - match);
							const size_t restSize = length - copySize;
							core.stdc.string.memcpy(op, dictEnd - copySize, copySize);
							op += copySize;

							if (restSize > cast(size_t)(op - lowPrefix)) { /* overlap copy */
								ubyte* endOfMatch = op + restSize;
								ubyte* copyFrom = cast(ubyte*)(lowPrefix);

								while (op < endOfMatch) {
									*op++ = *copyFrom++;
								}
							} else {
								core.stdc.string.memcpy(op, lowPrefix, restSize);
								op += restSize;
							}
						}

						continue;
					}

					/* copy match within block */
					cpy = op + length;

					/* partialDecoding : may not respect endBlock parsing restrictions */
					assert(op <= oend);

					if (partialDecoding && (cpy > (oend - .MATCH_SAFEGUARD_DISTANCE))) {
						const size_t mlen = lz4_lib.lz4common.MIN(length, cast(size_t)(oend - op));
						const ubyte* matchEnd = match + mlen;
						const ubyte* copyEnd = op + mlen;

						if (matchEnd > op) {   /* overlap copy */
							while (op < copyEnd) {
								*op++ = *match++;
							}
						} else {
							core.stdc.string.memcpy(op, match, mlen);
						}

						op = cast(ubyte*)(copyEnd);

						if (op == oend) {
							break;
						}

						continue;
					}

					if (lz4_lib.lz4common.unlikely(offset < 8)) {
						op[0] = match[0];
						op[1] = match[1];
						op[2] = match[2];
						op[3] = match[3];
						match += lz4_lib.lz4common.inc32table[offset];
						core.stdc.string.memcpy(op + 4, match, 4);
						match -= lz4_lib.lz4common.dec64table[offset];
					} else {
						core.stdc.string.memcpy(op, match, 8);
						match += 8;
					}

					op += 8;

					if (lz4_lib.lz4common.unlikely(cpy > oend - .MATCH_SAFEGUARD_DISTANCE)) {
						const ubyte* oCopyLimit = oend - (.WILDCOPYLENGTH - 1);

						if (cpy > oend - .LASTLITERALS) {
							/* Error : last LASTLITERALS bytes must be literals (uncompressed) */
							goto _output_error;
						}

						if (op < oCopyLimit) {
							lz4_lib.lz4common.wildCopy(op, match, oCopyLimit);
							match += oCopyLimit - op;
							op = cast(ubyte*)(oCopyLimit);
						}

						while (op < cpy) {
							*op++ = *match++;
						}
					} else {
						core.stdc.string.memcpy(op, match, 8);

						if (length > 16) {
							lz4_lib.lz4common.wildCopy(op + 8, match + 8, cpy);
						}
					}

					/* wildcopy correction */
					op = cpy;
				}

				/* end of decoding */
				if (endOnInput) {
					/* Nb of output bytes decoded */
					return cast(int)((cast(char*)(op)) - dst);
				} else {
					/* Nb of input bytes read */
					return cast(int)((cast(const (char)*)(ip)) - src);
				}

				/* Overflow error detected */
			_output_error:
				return cast(int)(-((cast(const (char)*)(ip)) - src)) - 1;
			}
		}
	}

/*===== Instantiate the API decoding functions. =====*/

/**
 * LZ4_decompress_safe() :
 *  compressedSize : is the exact complete size of the compressed block.
 *  dstCapacity : is the size of destination buffer, which must be already allocated.
 * @return : the number of bytes decompressed into destination buffer (necessarily <= dstCapacity)
 *           If destination buffer is not large enough, decoding will stop and output an error code (negative value).
 *           If the source stream is detected malformed, the function will stop decoding and return a negative result.
 *  Note : This function is protected against malicious data packets (never writes outside 'dst' buffer, nor read outside 'source' buffer).
 */
//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
//LZ4LIB_API
pragma(inline, true)
pure nothrow @nogc
public int LZ4_decompress_safe(const char* source, const char* dest, const int compressedSize, const int maxDecompressedSize)

	in
	{
		assert(dest != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, compressedSize, maxDecompressedSize, .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.noDict, cast(ubyte*)(dest), null, 0);
	}

//test
pragma(inline, true)
pure
public int LZ4_decompress_safe(const char[] source, ref char[] dest, const int compressedSize)

	in
	{
	}

	do
	{
		return .LZ4_decompress_generic(cast(const char*)(&source[0]), cast(char*)(&dest[0]), compressedSize, cast(int)(source.length), .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.noDict, cast(ubyte*)(dest), null, 0);
	}

/**
 * LZ4_decompress_safe_partial() :
 *  Decompress an LZ4 compressed block, of size 'srcSize' at position 'src',
 *  into destination buffer 'dst' of size 'dstCapacity'.
 *  Up to 'targetOutputSize' bytes will be decoded.
 *  The function stops decoding on reaching this objective,
 *  which can boost performance when only the beginning of a block is required.
 *
 * @return : the number of bytes decoded in `dst` (necessarily <= dstCapacity)
 *           If source stream is detected malformed, function returns a negative result.
 *
 *  Note : @return can be < targetOutputSize, if compressed block contains less data.
 *
 *  Note 2 : this function features 2 parameters, targetOutputSize and dstCapacity,
 *           and expects targetOutputSize <= dstCapacity.
 *           It effectively stops decoding on reaching targetOutputSize,
 *           so dstCapacity is kind of redundant.
 *           This is because in a previous version of this function,
 *           decoding operation would not "break" a sequence in the middle.
 *           As a consequence, there was no guarantee that decoding would stop at exactly targetOutputSize,
 *           it could write more bytes, though only up to dstCapacity.
 *           Some "margin" used to be required for this operation to work properly.
 *           This is no longer necessary.
 *           The function nonetheless keeps its signature, in an effort to not break API.
 */
//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
//LZ4LIB_API
pragma(inline, true)
pure nothrow @nogc
public int LZ4_decompress_safe_partial(const char* src, const char* dst, const int compressedSize, const int targetOutputSize, int dstCapacity)

	in
	{
		assert(dst != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		dstCapacity = lz4_lib.lz4common.MIN(targetOutputSize, dstCapacity);

		return .LZ4_decompress_generic(src, dst, compressedSize, dstCapacity, .endCondition_directive.endOnInputSize, .earlyEnd_directive.partial_decode, .dict_directive.noDict, cast(ubyte*)(dst), null, 0);
	}

/**
 * LZ4_decompress_fast() : **unsafe!**
 *  This function used to be a bit faster than LZ4_decompress_safe(),
 *  though situation has changed in recent versions,
 *  and now `LZ4_decompress_safe()` can be as fast and sometimes faster than `LZ4_decompress_fast()`.
 *  Moreover, LZ4_decompress_fast() is not protected vs malformed input, as it doesn't perform full validation of compressed data.
 *  As a consequence, this function is no longer recommended, and may be deprecated in future versions.
 *  It's last remaining specificity is that it can decompress data without knowing its compressed size.
 *
 *  originalSize : is the uncompressed size to regenerate.
 *                 `dst` must be already allocated, its size must be >= 'originalSize' bytes.
 * @return : number of bytes read from source buffer (== compressed size).
 *           If the source stream is detected malformed, the function stops decoding and returns a negative result.
 *  note : This function requires uncompressed originalSize to be known in advance.
 *         The function never writes past the output buffer.
 *         However, since it doesn't know its 'src' size, it may read past the intended input.
 *         Also, because match offsets are not validated during decoding,
 *         reads from 'src' may underflow.
 *         Use this function in trusted environment **only**.
 */
//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
//LZ4LIB_API
pragma(inline, true)
pure nothrow @nogc
public int LZ4_decompress_fast(const char* source, const char* dest, const int originalSize)

	in
	{
		assert(dest != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		return .LZ4_decompress_generic(source, dest, 0, originalSize, .endCondition_directive.endOnOutputSize, .earlyEnd_directive.decode_full_block, .dict_directive.withPrefix64k, cast(ubyte*)(dest) - lz4_lib.lz4common.KB!(uint, 64), null, 0);
	}

/*===== Instantiate a few more decoding cases, used more than once. =====*/

/**
 * Deprecated: use LZ4_decompress_safe_usingDict() instead
 */
//LZ4_FORCE_O2_INLINE_GCC_PPC64LE /* Exported, an obsolete API function. */
//LZ4LIB_API
pragma(inline, true)
//deprecated
pure nothrow @nogc
public int LZ4_decompress_safe_withPrefix64k(const char* source, const char* dest, const int compressedSize, const int maxOutputSize)

	in
	{
		assert(dest != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		return .LZ4_decompress_generic(source, dest, compressedSize, maxOutputSize, .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.withPrefix64k, cast(ubyte*)(dest) - lz4_lib.lz4common.KB!(uint, 64), null, 0);
	}

/* Another obsolete API function, paired with the previous one. */
/**
 * Deprecated: use LZ4_decompress_fast_usingDict() instead
 */
//LZ4LIB_API
//deprecated
pure nothrow @nogc
public int LZ4_decompress_fast_withPrefix64k(const char* source, const char* dest, const int originalSize)

	in
	{
	}

	do
	{
		/*
		 * LZ4_decompress_fast doesn't validate match offsets,
		 * and thus serves well with any prefixed dictionary.
		 */
		return .LZ4_decompress_fast(source, dest, originalSize);
	}

//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
pragma(inline, true)
pure nothrow @nogc
package int LZ4_decompress_safe_withSmallPrefix(const char* source, const char* dest, const int compressedSize, const int maxOutputSize, const size_t prefixSize)

	in
	{
		assert(dest != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, compressedSize, maxOutputSize, .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.noDict, cast(ubyte*)(dest) - prefixSize, null, 0);
	}

//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
pragma(inline, true)
pure nothrow @nogc
int LZ4_decompress_safe_forceExtDict(const char* source, const char* dest, const int compressedSize, const int maxOutputSize, const void* dictStart, const size_t dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, compressedSize, maxOutputSize, .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.usingExtDict, cast(ubyte*)(dest), cast(const (ubyte)*)(dictStart), dictSize);
	}

//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
pragma(inline, true)
pure nothrow @nogc
package int LZ4_decompress_fast_extDict(const char* source, const char* dest, const int originalSize, const void* dictStart, const size_t dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, 0, originalSize, .endCondition_directive.endOnOutputSize, .earlyEnd_directive.decode_full_block, .dict_directive.usingExtDict, cast(ubyte*)(dest), cast(const (ubyte)*)(dictStart), dictSize);
	}

/*
 * The "double dictionary" mode, for use with e.g. ring buffers: the first part
 * of the dictionary is passed as prefix, and the second via dictStart + dictSize.
 * These routines are used only once, in LZ4_decompress_*_continue().
 */
pragma(inline, true)
pure nothrow @nogc
int LZ4_decompress_safe_doubleDict(const char* source, const char* dest, const int compressedSize, const int maxOutputSize, const size_t prefixSize, const void* dictStart, const size_t dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, compressedSize, maxOutputSize, .endCondition_directive.endOnInputSize, .earlyEnd_directive.decode_full_block, .dict_directive.usingExtDict, cast(ubyte*)(dest) - prefixSize, cast(const (ubyte)*)(dictStart), dictSize);
	}

pragma(inline, true)
pure nothrow @nogc
int LZ4_decompress_fast_doubleDict(const char* source, const char* dest, const int originalSize, const size_t prefixSize, const void* dictStart, const size_t dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		return .LZ4_decompress_generic(source, dest, 0, originalSize, .endCondition_directive.endOnOutputSize, .earlyEnd_directive.decode_full_block, .dict_directive.usingExtDict, cast(ubyte*)(dest) - prefixSize, cast(const (ubyte)*)(dictStart), dictSize);
	}

/*===== streaming decompression functions =====*/
/**
 * for static allocation; maxBlockSize presumed valid
 */
pragma(inline, true)
pure nothrow @safe @nogc
package int LZ4_DECODER_RING_BUFFER_SIZE(const int maxBlockSize)

	in
	{
	}

	do
	{
		return 65536 + 14 + maxBlockSize;
	}

/**
 * LZ4_decoderRingBufferSize() : v1.8.2+
 *  Note : in a ring buffer scenario (optional),
 *  blocks are presumed decompressed next to each other
 *  up to the moment there is not enough remaining space for next block (remainingSize < maxBlockSize),
 *  at which stage it resumes from beginning of ring buffer.
 *  When setting such a ring buffer for streaming decompression,
 *  provides the minimum size of this ring buffer
 *  to be compatible with any source respecting maxBlockSize condition.
 *
 * Returns: minimum ring buffer size, or 0 if there is an error (invalid maxBlockSize).
 */
/**
 * LZ4_decoderRingBufferSize() :
 *  when setting a ring buffer for streaming decompression (optional scenario),
 *  provides the minimum size of this ring buffer
 *  to be compatible with any source respecting maxBlockSize condition.
 *  Note : in a ring buffer scenario,
 *  blocks are presumed decompressed next to each other.
 *  When not enough space remains for next block (remainingSize < maxBlockSize),
 *  decoding resumes from beginning of ring buffer.
 *
 * Returns: minimum ring buffer size, or 0 if there is an error (invalid maxBlockSize).
 */
//LZ4LIB_API
pure nothrow @nogc
public int LZ4_decoderRingBufferSize(int maxBlockSize)

	in
	{
	}

	do
	{
		if (maxBlockSize < 0) {
			return 0;
		}

		if (maxBlockSize > .LZ4_MAX_INPUT_SIZE) {
			return 0;
		}

		if (maxBlockSize < 16) {
			maxBlockSize = 16;
		}

		return .LZ4_DECODER_RING_BUFFER_SIZE(maxBlockSize);
	}

/**
 * Advanced decoding functions :
 *_usingDict() :
 *  These decoding functions work the same as "_continue" ones,
 *  the dictionary must be explicitly provided within parameters
 */

/**
 * LZ4_decompress_*_usingDict() :
 *  These decoding functions work the same as
 *  a combination of setStreamDecode() followed by LZ4_decompress_*_continue()
 *  They are stand-alone, and don't need an LZ4_streamDecode_t structure.
 *  Dictionary is presumed stable : it must remain accessible and unmodified during decompression.
 *  Performance tip : Decompression speed can be substantially increased
 *                    when dst == dictStart + dictSize.
 */
//LZ4LIB_API
pure nothrow @nogc
public int LZ4_decompress_safe_usingDict(const char* source, const char* dest, const int compressedSize, const int maxOutputSize, const char* dictStart, const int dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		static import lz4_lib.lz4common;

		if (dictSize == 0) {
			return .LZ4_decompress_safe(source, dest, compressedSize, maxOutputSize);
		}

		if (dictStart + dictSize == dest) {
			if (dictSize >= lz4_lib.lz4common.KB!(int, 64) - 1) {
				return .LZ4_decompress_safe_withPrefix64k(source, dest, compressedSize, maxOutputSize);
			}

			return .LZ4_decompress_safe_withSmallPrefix(source, dest, compressedSize, maxOutputSize, dictSize);
		}

		return .LZ4_decompress_safe_forceExtDict(source, dest, compressedSize, maxOutputSize, dictStart, dictSize);
	}

//LZ4LIB_API
pure nothrow @nogc
public int LZ4_decompress_fast_usingDict(const char* source, const char* dest, const int originalSize, const char* dictStart, const int dictSize)

	in
	{
		assert(dest != null);
		assert(dictStart != null);
	}

	do
	{
		if (dictSize == 0 || dictStart + dictSize == dest) {
			return .LZ4_decompress_fast(source, dest, originalSize);
		}

		return .LZ4_decompress_fast_extDict(source, dest, originalSize, dictStart, dictSize);
	}
