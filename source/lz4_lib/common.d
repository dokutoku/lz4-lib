/**
 * Common Utility
 *
 * License: BSD 2-Clause License
 */
module lz4_lib.lz4common;


private static import std.traits;

version (X86) {
	enum i386_or_x86_64 = true;
} else version (X86_64) {
	enum i386_or_x86_64 = true;
} else {
	enum i386_or_x86_64 = false;
}

pragma(inline, true):

debug {
	pragma(inline, false)
	nothrow @trusted
	void DEBUGLOG(S, A...)(immutable string file_name, immutable S line_number, immutable string message, A arguments)
		if (std.traits.isIntegral!(S))

		in
		{
			assert(line_number >= 1);
		}

		do
		{
			static import std.stdio;

			try {
				std.stdio.stderr.write(file_name, `: `);

				try {
					static if (arguments.length == 0) {
						std.stdio.stderr.writeln(message);
					} else {
						std.stdio.stderr.writefln(message, arguments);
					}
				} catch (Exception e2) {
					std.stdio.stderr.writeln(file_name, ` line `, line_number, ` DEBUGLOG Error: `, e2.msg);
				}
			} catch (Exception e1) {
			}
		}
}

pure nothrow @safe @nogc
S KB_func(S)(const S KB_size)
	if (std.traits.isIntegral!(S))

	in
	{
		assert(KB_size >= 1);
		assert((cast(ulong)(KB_size) * (1 << 10)) <= S.max);
	}

	do
	{
		return KB_size * (1 << 10);
	}

pure nothrow @safe @nogc
S MB_func(S)(const S MB_size)
	if (std.traits.isIntegral!(S))

	in
	{
		assert(MB_size >= 1);
		assert((cast(ulong)(MB_size) * (1 << 20)) <= S.max);
	}

	do
	{
		return MB_size * (1 << 20);
	}

pure nothrow @safe @nogc
S GB_func(S)(const S GB_size)
	if (std.traits.isIntegral!(S))

	in
	{
		assert(GB_size >= 1);
		assert((cast(ulong)(GB_size) * (1 << 30)) <= S.max);
	}

	do
	{
		return GB_size * (1 << 30);
	}

template KB(S, S KB_size)
	if (std.traits.isIntegral!(S))
	{
		enum KB = KB_func!(S)(KB_size);
	}

template MB(S, S MB_size)
	if (std.traits.isIntegral!(S))
	{
		enum MB = MB_func!(S)(MB_size);
	}

template GB(S, S GB_size)
	if (std.traits.isIntegral!(S))
	{
		enum GB = GB_func!(S)(GB_size);
	}

pure nothrow @safe @nogc
S MIN(S)(const S a, const S b)
	if (std.traits.isIntegral!(S))

	do
	{
		return (a < b) ? (a) : (b);
	}

pure nothrow @safe @nogc
S MAX(S)(const S a, const S b)
	if (std.traits.isIntegral!(S))

	do
	{
		return (a > b) ? (a) : (b);
	}

pure nothrow @safe @nogc
bool likely(const bool temp)

	do
	{
		return (temp != false) ? (true) : (false);
	}

pure nothrow @safe @nogc
bool unlikely(const bool temp)

	do
	{
		return (temp == false) ? (false) : (true);
	}

pure nothrow @safe @nogc
S rotl32(S)(const S x, const S r)
	if (std.traits.isIntegral!(S))

	do
	{
		return (x << r) | (x >> (32 - r));
	}

pure nothrow @safe @nogc
S rotl64(S)(const S x, const S r)
	if (std.traits.isIntegral!(S))

	do
	{
		return (x << r) | (x >> (64 - r));
	}

/*-************************************
*  Reading and writing into memory
**************************************/
/**
 * LZ4_FORCE_MEMORY_ACCESS
 * By default, access to unaligned memory is controlled by `memcpy()`, which is safe and portable.
 * Unfortunately, on some target/compiler combinations, the generated assembly is sub-optimal.
 * The below switch allow to select different access method for improved performance.
 * Method 0 (default) : use `memcpy()`. Safe and portable.
 * Method 1 : `__packed` statement. It depends on compiler extension (ie, not portable).
 *            This method is safe if your compiler supports it, and *generally* as fast or faster than `memcpy`.
 * Method 2 : direct access. This method is portable but violate C standard.
 *            It can generate buggy code on targets which assembly generation depends on alignment.
 *            But in some circumstances, it's the only known way to get the most performance (ie GCC + ARMv6)
 * See https://fastcompression.blogspot.fr/2015/08/accessing-unaligned-memory.html for details.
 * Prefer these methods in priority order (0 > 1 > 2)
 */
/**
 * XXH_FORCE_MEMORY_ACCESS :
 * By default, access to unaligned memory is controlled by `memcpy()`, which is safe and portable.
 * Unfortunately, on some target/compiler combinations, the generated assembly is sub-optimal.
 * The below switch allow to select different access method for improved performance.
 * Method 0 (default) : use `memcpy()`. Safe and portable.
 * Method 1 : `__packed` statement. It depends on compiler extension (ie, not portable).
 *            This method is safe if your compiler supports it, and *generally* as fast or faster than `memcpy`.
 * Method 2 : direct access. This method doesn't depend on compiler but violate C standard.
 *            It can generate buggy code on targets which do not support unaligned memory accesses.
 *            But in some circumstances, it's the only known way to get the most performance (ie GCC + ARMv6)
 * See http://stackoverflow.com/a/32095106/646947 for details.
 * Prefer these methods in priority order (0 > 1 > 2)
 */
/* can be defined externally */
static if (!__traits(compiles, .LZ4_FORCE_MEMORY_ACCESS)) {
	//#if defined(__GNUC__) && (defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || defined(__ARM_ARCH_6K__) || defined(__ARM_ARCH_6Z__) || defined(__ARM_ARCH_6ZK__) || defined(__ARM_ARCH_6T2__))
	//	enum LZ4_FORCE_MEMORY_ACCESS = 2;
	//#elif (defined(__INTEL_COMPILER) && !defined(_WIN32)) || defined(__GNUC__)
	//	enum LZ4_FORCE_MEMORY_ACCESS = 1;
	//} else {
		enum LZ4_FORCE_MEMORY_ACCESS = 0;
	//}
}

static if (.LZ4_FORCE_MEMORY_ACCESS == 2) {
	/* lie to the compiler about data alignment; use with caution */
	/* Force direct memory access. Only works on CPU which support unaligned memory access in hardware */

	pure nothrow @nogc
	ushort read16(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			return *(cast(const ushort*)(memPtr));
		}

	pure nothrow @nogc
	uint read32(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			return *(cast(const uint*)(memPtr));
		}

	pure nothrow @nogc
	ulong read64(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			return *(cast(const ulong*)(memPtr));
		}

	pure nothrow @nogc
	size_t read_ARCH(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			return *(cast(const size_t*)(memPtr));
		}

	pure nothrow @nogc
	void write16(void* memPtr, const ushort value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			*(cast(ushort*)(memPtr)) = value;
		}

	pure nothrow @nogc
	void write32(void* memPtr, const uint value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			*(cast(uint*)(memPtr)) = value;
		}
} else static if (.LZ4_FORCE_MEMORY_ACCESS == 1) {
	/* __pack instructions are safer, but compiler specific, hence potentially problematic for some compilers */
	/* currently only defined for gcc and icc */
	align (1)
	private union unalign
	{
		ushort u16;
		uint u32;
		size_t uArch;
	}

	align (1)
	private union unalign64
	{
		uint u32;
		ulong u64;
	}

	pure nothrow @nogc
	ushort read16(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			return (*(cast(const unalign*)(ptr))).u16;
		}

	pure nothrow @nogc
	uint read32(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			return (*(cast(const unalign*)(ptr))).u32;
		}

	pure nothrow @nogc
	ulong read64(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			return (*(cast(const unalign64*)(ptr))).u64;
		}

	pure nothrow @nogc
	size_t read_ARCH(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			return (*(cast(const unalign*)(ptr))).uArch;
		}

	pure nothrow @nogc
	void write16(void* memPtr, const ushort value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			(*(cast(const unalign*)(ptr))).u16 = value;
		}

	pure nothrow @nogc
	void write32(void* memPtr, const uint value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			(*(cast(const unalign*)(ptr))).u32 = value;
		}
} else {
	/* safe and portable access through memcpy() */
	/*
	 * portable and safe solution. Generally efficient.
	 * see : http://stackoverflow.com/a/32095106/646947
	 */

	pure nothrow @nogc @system
	ushort read16(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			ushort val;
			core.stdc.string.memcpy(&val, memPtr, val.sizeof);

			return val;
		}

	pure nothrow @nogc @system
	uint read32(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			uint val;
			core.stdc.string.memcpy(&val, memPtr, val.sizeof);

			return val;
		}

	pure nothrow @nogc @system
	ulong read64(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			ulong val;
			core.stdc.string.memcpy(&val, memPtr, val.sizeof);

			return val;
		}

	pure nothrow @nogc @system
	size_t read_ARCH(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			size_t val;
			core.stdc.string.memcpy(&val, memPtr, val.sizeof);

			return val;
		}

	pure nothrow @nogc @system
	void write16(void* memPtr, const ushort value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			core.stdc.string.memcpy(memPtr, &value, value.sizeof);
		}

	pure nothrow @nogc @system
	void write32(void* memPtr, const uint value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			static import core.stdc.string;

			core.stdc.string.memcpy(memPtr, &value, value.sizeof);
		}
}

version (LittleEndian) {
	pure nothrow @nogc
	ushort readLE16(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			return .read16(memPtr);
		}

	pure nothrow @nogc
	void writeLE16(void* memPtr, const ushort value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			.write16(memPtr, value);
		}

	pure nothrow @nogc
	uint readBE32(const void* ptr)

		in
		{
		}

		do
		{
			static import std.bitmanip;

			return std.bitmanip.swapEndian!(uint)(.read32(ptr));
		}

	pure nothrow @nogc
	ulong readBE64(const void* ptr)

		in
		{
		}

		do
		{
			static import std.bitmanip;

			return std.bitmanip.swapEndian!(ulong)(.read64(ptr));
		}
} else version (BigEndian) {
	pure nothrow @nogc
	ushort readLE16(const void* memPtr)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			const ubyte* p = cast(const ubyte*)(memPtr);

			return cast(ushort)(cast(ushort)(p[0]) + (p[1] << 8));
		}

	pure nothrow @nogc
	void writeLE16(void* memPtr, const ushort value)

		in
		{
			assert(memPtr != null);
		}

		do
		{
			ubyte* p = cast(ubyte*)(memPtr);
			p[0] = cast(ubyte)(value);
			p[1] = cast(ubyte)(value >> 8);
		}

	pure nothrow @nogc
	uint readBE32(const void* ptr)

		in
		{
		}

		do
		{
			return .read32(ptr);
		}

	pure nothrow @nogc
	ulong readBE64(const void* ptr)

		in
		{
		}

		do
		{
			return .read64(ptr);
		}
} else {
	static assert(0);
}

/**
 * customized variant of memcpy, which can overwrite up to 8 bytes beyond dstEnd
 */
//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
pragma(inline, true)
pure nothrow @nogc @system
void wildCopy(void* dstPtr, const (void)* srcPtr, const void* dstEnd)

	in
	{
		assert(dstPtr != null);
		assert(srcPtr != null);
		assert(dstEnd != null);
	}

	do
	{
		static import core.stdc.string;

		ubyte* d = cast(ubyte*)(dstPtr);
		const (ubyte)* s = cast(const (ubyte)*)(srcPtr);
		const ubyte* e = cast(const ubyte*)(dstEnd);

		do {
			core.stdc.string.memcpy(d, s, 8);
			d += 8;
			s += 8;
		} while (d < e);
	}

static immutable uint[8] inc32table = [0, 1, 2, 1, 0, 4, 4, 4];
static immutable int[8] dec64table = [0, 0, 0, -1, -4, 1, 2, 3];

static if (.i386_or_x86_64) {
	//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
	pragma(inline, true)
	pure nothrow @nogc
	void memcpy_using_offset_base(ubyte* dstPtr, const (ubyte)* srcPtr, ubyte* dstEnd, const size_t offset)

		do
		{
			static import core.stdc.string;

			if (offset < 8) {
				dstPtr[0] = srcPtr[0];
				dstPtr[1] = srcPtr[1];
				dstPtr[2] = srcPtr[2];
				dstPtr[3] = srcPtr[3];
				srcPtr += .inc32table[offset];
				core.stdc.string.memcpy(dstPtr + 4, srcPtr, 4);
				srcPtr -= .dec64table[offset];
				dstPtr += 8;
			} else {
				core.stdc.string.memcpy(dstPtr, srcPtr, 8);
				dstPtr += 8;
				srcPtr += 8;
			}

			.wildCopy(dstPtr, srcPtr, dstEnd);
		}

	/* customized variant of memcpy, which can overwrite up to 32 bytes beyond dstEnd */
	//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
	pragma(inline, true)
	pure nothrow @nogc
	void wildCopy32(void* dstPtr, const void* srcPtr, void* dstEnd)

		do
		{
			static import core.stdc.string;

			ubyte* d = cast(ubyte*)(dstPtr);
			const (ubyte)* s = cast(const ubyte*)(srcPtr);
			const ubyte* e = cast(ubyte*)(dstEnd);

			do {
				core.stdc.string.memcpy(d, s, 16);
				core.stdc.string.memcpy(d + 16, s + 16, 16);
				d += 32;
				s += 32;
			} while (d < e);
		}

	//LZ4_FORCE_O2_INLINE_GCC_PPC64LE
	pragma(inline, true)
	pure nothrow @nogc
	void memcpy_using_offset(ubyte* dstPtr, const ubyte* srcPtr, ubyte* dstEnd, const size_t offset)

		do
		{
			static import core.stdc.string;

			ubyte[8] v;

			switch (offset) {
				case 1:
					core.stdc.string.memset(&v[0], *srcPtr, 8);
					goto copy_loop;

				case 2:
					core.stdc.string.memcpy(&v[0], srcPtr, 2);
					core.stdc.string.memcpy(&v[2], srcPtr, 2);
					core.stdc.string.memcpy(&v[4], &v[0], 4);
					goto copy_loop;

				case 4:
					core.stdc.string.memcpy(&v[0], srcPtr, 4);
					core.stdc.string.memcpy(&v[4], srcPtr, 4);
					goto copy_loop;

				case 3:
				case 5:
				case 6:
				case 7:
				case 8:
				default:
					.memcpy_using_offset_base(dstPtr, srcPtr, dstEnd, offset);

					return;
			}

		copy_loop:
			core.stdc.string.memcpy(dstPtr, &v[0], 8);
			dstPtr += 8;

			while (dstPtr < dstEnd) {
				core.stdc.string.memcpy(dstPtr, &v[0], 8);
				dstPtr += 8;
			}
		}
}
