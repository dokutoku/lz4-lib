/**
 * Notice extracted from xxHash homepage :
 *
 * xxHash is an extremely fast Hash algorithm, running at RAM speed limits.
 * It also successfully passes all tests from the SMHasher suite.
 *
 * Comparison (single thread, Windows Seven 32 bits, using SMHasher on a Core 2 Duo @3GHz)
 *
 * Name            Speed       Q.Score   Author
 * xxHash          5.4 GB/s     10
 * CrapWow         3.2 GB/s      2       Andrew
 * MumurHash 3a    2.7 GB/s     10       Austin Appleby
 * SpookyHash      2.0 GB/s     10       Bob Jenkins
 * SBox            1.4 GB/s      9       Bret Mulvey
 * Lookup3         1.2 GB/s      9       Bob Jenkins
 * SuperFastHash   1.2 GB/s      1       Paul Hsieh
 * CityHash64      1.05 GB/s    10       Pike & Alakuijala
 * FNV             0.55 GB/s     5       Fowler, Noll, Vo
 * CRC32           0.43 GB/s     9
 * MD5-32          0.33 GB/s    10       Ronald L. Rivest
 * SHA1-32         0.28 GB/s    10
 *
 * Q.Score is a measure of quality of the hash function.
 * It depends on successfully passing SMHasher test set.
 * 10 is a perfect score.
 *
 * A 64-bit version, named XXH64, is available since r35.
 * It offers much better speed, but for 64-bit applications only.
 * Name     Speed on 64 bits    Speed on 32 bits
 * XXH64       13.8 GB/s            1.9 GB/s
 * XXH32        6.8 GB/s            6.0 GB/s
 *
 * License: BSD 2-Clause License
 */
/*
*  xxHash - Fast Hash algorithm
*  Copyright (C) 2012-2016, Yann Collet
*
*  BSD 2-Clause License (http://www.opensource.org/licenses/bsd-license.php)
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions are
*  met:
*
*  * Redistributions of source code must retain the above copyright
*  notice, this list of conditions and the following disclaimer.
*  * Redistributions in binary form must reproduce the above
*  copyright notice, this list of conditions and the following disclaimer
*  in the documentation and/or other materials provided with the
*  distribution.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  You can contact the author at :
*  - xxHash homepage: http://www.xxhash.com
*  - xxHash source repository : https://github.com/Cyan4973/xxHash
*/
module lz4_lib.xxhash;


/*-*************************************
*  Dependency
***************************************/
private static import core.stdc.stdlib;
private static import core.stdc.string;
private static import std.bitmanip;
private static import lz4_lib.lz4common;

/*-****************************
*  Definitions
******************************/

/*-*************************************
*  Version
***************************************/
enum XXH_VERSION_MAJOR = 0;
enum XXH_VERSION_MINOR = 6;
enum XXH_VERSION_RELEASE = 5;
enum XXH_VERSION_NUMBER = (XXH_VERSION_MAJOR * 100 * 100) + (XXH_VERSION_MINOR * 100) + XXH_VERSION_RELEASE;

/*-*************************************
*  Tuning parameters
***************************************/
/**
 * XXH_ACCEPT_NULL_INPUT_POINTER :
 * If input pointer is NULL, xxHash default behavior is to dereference it, triggering a segfault.
 * When this macro is enabled, xxHash actively checks input for null pointer.
 * It it is, result for null input pointers is the same as a null-length input.
 */
/* can be defined externally */
static if (!__traits(compiles, .XXH_ACCEPT_NULL_INPUT_POINTER)) {
	enum XXH_ACCEPT_NULL_INPUT_POINTER = false;
}

/**
 * XXH_FORCE_ALIGN_CHECK :
 * This is a minor performance trick, only useful with lots of very small keys.
 * It means : check for aligned/unaligned input.
 * The check costs one initial branch per hash;
 * set it to 0 when the input is guaranteed to be aligned,
 * or when alignment doesn't matter for performance.
 */
/* can be defined externally */
static if (!__traits(compiles, .XXH_FORCE_ALIGN_CHECK)) {
	//#if defined(__i386) || defined(_M_IX86) || defined(__x86_64__) || defined(_M_X64)
		enum bool XXH_FORCE_ALIGN_CHECK = false;
	//} else {
	//	enum bool XXH_FORCE_ALIGN_CHECK = true;
	//}
}

/*-*************************************
*  Compiler Specific Options
***************************************/

/*-*************************************
*  Basic Types
***************************************/

/*-****************************************
*  Compiler-specific Functions and Macros
******************************************/

/*-*************************************
*  Architecture Macros
***************************************/

/*-***************************
*  Memory reads
*****************************/
enum XXH_alignment : bool
{
	aligned = true,
	unaligned = false,
}

version (LittleEndian) {
	pragma(inline, true)
	pure nothrow @nogc
	private uint readLE32_align(.XXH_alignment XXH_align)(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			static if (!XXH_align) {
				return lz4_lib.lz4common.read32(ptr);
			} else {
				return *(cast(const uint*)(ptr));
			}
		}

	pragma(inline, true)
	pure nothrow @nogc
	private ulong readLE64_align(.XXH_alignment XXH_align)(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			static import lz4_lib.lz4common;

			static if (!XXH_align) {
				return lz4_lib.lz4common.read64(ptr);
			} else {
				return *(cast(const ulong*)(ptr));
			}
		}
} else version (BigEndian) {
	pragma(inline, true)
	pure nothrow @nogc
	private uint readLE32_align(.XXH_alignment XXH_align)(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			static import std.bitmanip;
			static import lz4_lib.lz4common;

			static if (!XXH_align) {
				return std.bitmanip.swapEndian!(uint)(lz4_lib.lz4common.read32(ptr));
			} else {
				return std.bitmanip.swapEndian!(uint)(*(cast(const uint*)(ptr)));
			}
		}

	pragma(inline, true)
	pure nothrow @nogc
	private ulong readLE64_align(.XXH_alignment XXH_align)(const void* ptr)

		in
		{
			assert(ptr != null);
		}

		do
		{
			static import std.bitmanip;
			static import lz4_lib.lz4common;

			static if (!XXH_align) {
				return std.bitmanip.swapEndian!(ulong)(lz4_lib.lz4common.read64(ptr));
			} else {
				return std.bitmanip.swapEndian!(ulong)(*(cast(const ulong*)(ptr)));
			}
		}
} else {
	static assert(0);
}

/*-*************************************
*  Macros
***************************************/
//XXH_PUBLIC_API
pragma(inline, true)
pure nothrow @safe @nogc
public uint XXH_versionNumber()

	in
	{
	}

	do
	{
		return .XXH_VERSION_NUMBER;
	}

/* ================================================================================================
   This section contains declarations which are not guaranteed to remain stable.
   They may change in future versions, becoming incompatible with a different version of the library.
   These declarations should only be used with static linking.
   Never use them in association with dynamic linking !
=================================================================================================== */

/*
 * These definitions are only present to allow
 *  static allocation of XXH state, on stack or in a struct for example.
 *  Never **ever** use members directly.
 */

struct XXH32_state_t
{
	uint total_len_32;
	uint large_len;
	uint v1;
	uint v2;
	uint v3;
	uint v4;
	uint[4] mem32;
	uint memsize;

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @safe @nogc
	public void copyState(const ref .XXH32_state_t srcState)

		in
		{
		}

		do
		{
			this = srcState;
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @safe @nogc
	public void reset(const uint seed)

		in
		{
		}

		do
		{
			/* using a local state to memcpy() in order to avoid strict-aliasing warnings */
			.XXH32_state_t state;

			state = .XXH32_state_t.init;

			state.v1 = seed + .PRIME32_1 + .PRIME32_2;
			state.v2 = seed + .PRIME32_2;
			state.v3 = seed + 0;
			state.v4 = seed - .PRIME32_1;
			this = state;
		}

	pragma(inline, true)
	pure nothrow @nogc @system
	package bool update_endian(const void* input, const size_t len)

		in
		{
		}

		do
		{
			static import core.stdc.string;

			static if (.XXH_ACCEPT_NULL_INPUT_POINTER) {
				if (input == null) {
					return true;
				}
			} else {
				if (input == null) {
					return false;
				}
			}

			{
				const (ubyte)* p = cast(const ubyte*)(input);
				const ubyte* bEnd = p + len;

				this.total_len_32 += cast(uint)(len);
				this.large_len |= (len >= 16) | (this.total_len_32 >= 16);

				/* fill in tmp buffer */
				if ((this.memsize + len) < 16) {
					core.stdc.string.memcpy(cast(ubyte*)(this.mem32[0]) + this.memsize, input, len);
					this.memsize += cast(uint)(len);

					return true;
				}

				/* some data left from previous update */
				if (this.memsize) {
					core.stdc.string.memcpy(cast(ubyte*)(this.mem32[0]) + this.memsize, input, 16 - this.memsize);

					{
						this.v1 = .XXH32_round(this.v1, .readLE32_align!(.XXH_alignment.unaligned)(&this.mem32[0]));
						this.v2 = .XXH32_round(this.v2, .readLE32_align!(.XXH_alignment.unaligned)(&this.mem32[1]));
						this.v3 = .XXH32_round(this.v3, .readLE32_align!(.XXH_alignment.unaligned)(&this.mem32[2]));
						this.v4 = .XXH32_round(this.v4, .readLE32_align!(.XXH_alignment.unaligned)(&this.mem32[3]));
					}

					p += 16 - this.memsize;
					this.memsize = 0;
				}

				if (p <= bEnd - 16) {
					const ubyte* limit = bEnd - 16;
					uint v1 = this.v1;
					uint v2 = this.v2;
					uint v3 = this.v3;
					uint v4 = this.v4;

					do {
						v1 = .XXH32_round(v1, .readLE32_align!(.XXH_alignment.unaligned)(p));
						p += 4;
						v2 = .XXH32_round(v2, .readLE32_align!(.XXH_alignment.unaligned)(p));
						p += 4;
						v3 = .XXH32_round(v3, .readLE32_align!(.XXH_alignment.unaligned)(p));
						p += 4;
						v4 = .XXH32_round(v4, .readLE32_align!(.XXH_alignment.unaligned)(p));
						p += 4;
					} while (p <= limit);

					this.v1 = v1;
					this.v2 = v2;
					this.v3 = v3;
					this.v4 = v4;
				}

				if (p < bEnd) {
					core.stdc.string.memcpy(&this.mem32[0], p, cast(size_t)(bEnd - p));
					this.memsize = cast(uint)(bEnd - p);
				}
			}

			return true;
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @nogc
	public bool update(const void* input, const size_t len)

		in
		{
		}

		do
		{
			return this.update_endian(input, len);
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @nogc
	public uint digest_endian()

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			uint h32;

			if (this.large_len) {
				h32 = lz4_lib.lz4common.rotl32(this.v1, 1) + lz4_lib.lz4common.rotl32(this.v2, 7) + lz4_lib.lz4common.rotl32(this.v3, 12) + lz4_lib.lz4common.rotl32(this.v4, 18);
			} else {
				h32 = this.v3 /* seed */ + .PRIME32_5;
			}

			h32 += this.total_len_32;

			return .XXH32_finalize!(.XXH_alignment.aligned)(h32, &this.mem32[0], this.memsize);
		}
}

struct XXH64_state_t
{
	ulong total_len;
	ulong v1;
	ulong v2;
	ulong v3;
	ulong v4;
	ulong[4] mem64;
	uint memsize;

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @safe @nogc
	public void copyState(const ref .XXH64_state_t srcState)

		in
		{
		}

		do
		{
			this = srcState;
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @safe @nogc
	public void reset(const ulong seed)

		in
		{
		}

		do
		{
			.XXH64_state_t state;

			state = .XXH64_state_t.init;

			state.v1 = seed + .PRIME64_1 + .PRIME64_2;
			state.v2 = seed + .PRIME64_2;
			state.v3 = seed + 0;
			state.v4 = seed - .PRIME64_1;
			this = state;
		}

	pragma(inline, true)
	pure nothrow @nogc @system
	package bool update_endian(const void* input, const size_t len)

		in
		{
		}

		do
		{
			static import core.stdc.string;

			static if (.XXH_ACCEPT_NULL_INPUT_POINTER) {
				if (input == null) {
					return true;
				}
			} else {
				if (input == null) {
					return false;
				}
			}

			{
				const (ubyte)* p = cast(const ubyte*)(input);
				const ubyte* bEnd = p + len;

				this.total_len += len;

				/* fill in tmp buffer */
				if (this.memsize + len < 32) {
					core.stdc.string.memcpy((cast(ubyte*)(this.mem64[0])) + this.memsize, input, len);
					this.memsize += cast(uint)(len);

					return true;
				}

				/* tmp buffer is full */
				if (this.memsize) {
					core.stdc.string.memcpy((cast(ubyte*)(this.mem64[0])) + this.memsize, input, 32 - this.memsize);
					this.v1 = .XXH64_round(this.v1, .readLE64_align!(.XXH_alignment.unaligned)(&this.mem64[0]));
					this.v2 = .XXH64_round(this.v2, .readLE64_align!(.XXH_alignment.unaligned)(&this.mem64[1]));
					this.v3 = .XXH64_round(this.v3, .readLE64_align!(.XXH_alignment.unaligned)(&this.mem64[2]));
					this.v4 = .XXH64_round(this.v4, .readLE64_align!(.XXH_alignment.unaligned)(&this.mem64[3]));
					p += 32 - this.memsize;
					this.memsize = 0;
				}

				if (p + 32 <= bEnd) {
					const ubyte* limit = bEnd - 32;
					ulong v1 = this.v1;
					ulong v2 = this.v2;
					ulong v3 = this.v3;
					ulong v4 = this.v4;

					do {
						v1 = .XXH64_round(v1, .readLE64_align!(.XXH_alignment.unaligned)(p));
						p += 8;
						v2 = .XXH64_round(v2, .readLE64_align!(.XXH_alignment.unaligned)(p));
						p += 8;
						v3 = .XXH64_round(v3, .readLE64_align!(.XXH_alignment.unaligned)(p));
						p += 8;
						v4 = .XXH64_round(v4, .readLE64_align!(.XXH_alignment.unaligned)(p));
						p += 8;
					} while (p <= limit);

					this.v1 = v1;
					this.v2 = v2;
					this.v3 = v3;
					this.v4 = v4;
				}

				if (p < bEnd) {
					core.stdc.string.memcpy(&this.mem64[0], p, cast(size_t)(bEnd - p));
					this.memsize = cast(uint)(bEnd - p);
				}
			}

			return true;
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @nogc
	public bool update(const void* input, const size_t len)

		in
		{
		}

		do
		{
			return this.update_endian(input, len);
		}

	//XXH_PUBLIC_API
	pragma(inline, true)
	pure nothrow @nogc
	public ulong digest_endian()

		in
		{
		}

		do
		{
			static import lz4_lib.lz4common;

			ulong h64;

			if (this.total_len >= 32) {
				const ulong v1 = this.v1;
				const ulong v2 = this.v2;
				const ulong v3 = this.v3;
				const ulong v4 = this.v4;

				h64 = lz4_lib.lz4common.rotl64(v1, 1) + lz4_lib.lz4common.rotl64(v2, 7) + lz4_lib.lz4common.rotl64(v3, 12) + lz4_lib.lz4common.rotl64(v4, 18);
				h64 = .XXH64_mergeRound(h64, v1);
				h64 = .XXH64_mergeRound(h64, v2);
				h64 = .XXH64_mergeRound(h64, v3);
				h64 = .XXH64_mergeRound(h64, v4);
			} else {
				h64 = this.v3 /* seed */ + .PRIME64_5;
			}

			h64 += cast(ulong)(this.total_len);

			return .XXH64_finalize!(.XXH_alignment.aligned)(h64, &this.mem64[0], cast(size_t)(this.total_len));
		}
}

package alias XXH32_state_s = .XXH32_state_t;
package alias XXH64_state_s = .XXH64_state_t;

/*-*******************************************************************
*  32-bit hash functions
*********************************************************************/
enum uint PRIME32_1 = 2654435761u;
enum uint PRIME32_2 = 2246822519u;
enum uint PRIME32_3 = 3266489917u;
enum uint PRIME32_4 = 668265263u;
enum uint PRIME32_5 = 374761393u;

pure nothrow @safe @nogc
package uint XXH32_round(uint seed, const uint input)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4common;

		seed += input * .PRIME32_2;
		seed = lz4_lib.lz4common.rotl32(seed, 13);
		seed *= .PRIME32_1;

		return seed;
	}

/**
 * mix all bits
 */
pure nothrow @safe @nogc
package uint XXH32_avalanche(uint h32)

	do
	{
		h32 ^= h32 >> 15;
		h32 *= .PRIME32_2;
		h32 ^= h32 >> 13;
		h32 *= .PRIME32_3;
		h32 ^= h32 >> 16;

		return h32;
	}

pure nothrow @nogc
package uint XXH32_finalize(.XXH_alignment XXH_align)(uint h32, const void* ptr, const size_t len)

	in
	{
		switch (len & 15) {
			case 0: .. case 15:
				break;

			default:
				assert(0);
		}
	}

	do
	{
		static import lz4_lib.lz4common;

		const (ubyte)* p = cast(const (ubyte)*)(ptr);

		pragma(inline, true)
		pure nothrow @nogc
		void PROCESS1()

			do
			{
				h32 += (*p++) * .PRIME32_5;
				h32 = lz4_lib.lz4common.rotl32(h32, 11) * .PRIME32_1;
			}

		pragma(inline, true)
		pure nothrow @nogc
		void PROCESS4()

			do
			{
				h32 += .readLE32_align!(XXH_align)(p) * .PRIME32_3;
				p +=4;
				h32 = lz4_lib.lz4common.rotl32(h32, 17) * .PRIME32_4;
			}

		switch (len & 15) { /* or switch(bEnd - p) */
			case 12:
				PROCESS4();
				goto case;

			case 8:
				PROCESS4();
				goto case;

			case 4:
				PROCESS4();

				return .XXH32_avalanche(h32);

			case 13:
				PROCESS4();
				goto case;

			case 9:
				PROCESS4();
				goto case;

			case 5:
				PROCESS4();
				PROCESS1();

				return .XXH32_avalanche(h32);

			case 14:
				PROCESS4();
				goto case;

			case 10:
				PROCESS4();
				goto case;

			case 6:
				PROCESS4();
				PROCESS1();
				PROCESS1();

				return .XXH32_avalanche(h32);

			case 15:
				PROCESS4();
				goto case;

			case 11:
				PROCESS4();
				goto case;

			case 7:
				PROCESS4();
				goto case;

			case 3:
				PROCESS1();
				goto case;

			case 2:
				PROCESS1();
				goto case;

			case 1:
				PROCESS1();
				goto case;

			case 0:
				return .XXH32_avalanche(h32);

			default:
				return h32;
		}
	}

pragma(inline, true)
pure nothrow @nogc
uint XXH32_endian_align(.XXH_alignment XXH_align)(const void* input, size_t len, const uint seed)

	in
	{
		static if (!.XXH_ACCEPT_NULL_INPUT_POINTER) {
			assert(input != null);
		}
	}

	do
	{
		static import lz4_lib.lz4common;

		const (ubyte)* p = cast(const (ubyte)*)(input);
		const (ubyte)* bEnd = p + len;
		uint h32;

		static if (.XXH_ACCEPT_NULL_INPUT_POINTER) {
			if (p == null) {
				len = 0;
				p = cast(const (ubyte)*)(cast(size_t)(16));
				bEnd = p;
			}
		}

		if (len >= 16) {
			const ubyte* limit = bEnd - 15;
			uint v1 = seed + .PRIME32_1 + .PRIME32_2;
			uint v2 = seed + .PRIME32_2;
			uint v3 = seed + 0;
			uint v4 = seed - .PRIME32_1;

			do {
				v1 = .XXH32_round(v1, .readLE32_align!(XXH_align)(p));
				p += 4;
				v2 = .XXH32_round(v2, .readLE32_align!(XXH_align)(p));
				p += 4;
				v3 = .XXH32_round(v3, .readLE32_align!(XXH_align)(p));
				p += 4;
				v4 = .XXH32_round(v4, .readLE32_align!(XXH_align)(p));
				p += 4;
			} while (p < limit);

			h32 = lz4_lib.lz4common.rotl32(v1, 1) + lz4_lib.lz4common.rotl32(v2, 7) + lz4_lib.lz4common.rotl32(v3, 12) + lz4_lib.lz4common.rotl32(v4, 18);
		} else {
			h32 = seed + .PRIME32_5;
		}

		h32 += cast(uint)(len);

		return .XXH32_finalize!(XXH_align)(h32, p, len & 15);
	}

/**
 * XXH32() :
 *  Calculate the 32-bit hash of sequence "length" bytes stored at memory address "input".
 *  The memory between input & input+length must be valid (allocated and read-accessible).
 *  "seed" can be used to alter the result predictably.
 *  Speed on Core 2 Duo @ 3 GHz (single thread, SMHasher benchmark) : 5.4 GB/s
 */
//XXH_PUBLIC_API
pragma(inline, true)
pure nothrow @nogc
public uint XXH32(const void* input, const size_t len, const uint seed)

	in
	{
		assert(input != null);
	}

	do
	{
		static if (.XXH_FORCE_ALIGN_CHECK) {
			/* Input is 4-bytes aligned, leverage the speed benefit */
			if (((cast(size_t)(input)) & 3) == 0) {
				return .XXH32_endian_align!(.XXH_alignment.aligned)(input, len, seed);
			}
		}

		return .XXH32_endian_align!(.XXH_alignment.unaligned)(input, len, seed);
	}

/*
 * Streaming functions generate the xxHash of an input provided in multiple segments.
 * Note that, for small input, they are slower than single-call functions, due to state management.
 * For small inputs, prefer `XXH32()` and `XXH64()`, which are better optimized.
 *
 * XXH state must first be allocated, using XXH*_createState() .
 *
 * Start a new hash by initializing state with a seed, using XXH*_reset().
 *
 * Then, feed the hash state by calling XXH*_update() as many times as necessary.
 * The function returns an error code, with 0 meaning OK, and any other value meaning there is an error.
 *
 * Finally, a hash value can be produced anytime, by using XXH*_digest().
 * This function returns the nn-bits hash as an int or long long.
 *
 * It's still possible to continue inserting input into the hash state after a digest,
 * and generate some new hashes later on, by calling again XXH*_digest().
 *
 * When done, free XXH state space if it was allocated dynamically.
 */

/*======   Canonical representation   ======*/

/*
 * Default result type for XXH functions are primitive unsigned 32 and 64 bits.
 * The canonical representation uses human-readable write convention, aka big-endian (large digits first).
 * These functions allow transformation of hash result into and from its canonical format.
 * This way, hash values can be written into a file / memory, and remain comparable on different systems and programs.
 */
/*-*******************************************************************
*  64-bit hash functions
*********************************************************************/
/*======   Memory access   ======*/

/*======   xxh64   ======*/

enum ulong PRIME64_1 = 11400714785074694791UL;
enum ulong PRIME64_2 = 14029467366897019727UL;
enum ulong PRIME64_3 = 1609587929392839161UL;
enum ulong PRIME64_4 = 9650029242287828579UL;
enum ulong PRIME64_5 = 2870177450012600261UL;

pure nothrow @safe @nogc
package ulong XXH64_round(ulong acc, ulong input)

	in
	{
	}

	do
	{
		static import lz4_lib.lz4common;

		acc += input * .PRIME64_2;
		acc = lz4_lib.lz4common.rotl64(acc, 31);
		acc *= .PRIME64_1;

		return acc;
	}

pure nothrow @safe @nogc
package ulong XXH64_mergeRound(ulong acc, ulong val)

	in
	{
	}

	do
	{
		val = .XXH64_round(0, val);
		acc ^= val;
		acc = acc * .PRIME64_1 + .PRIME64_4;

		return acc;
	}

pure nothrow @safe @nogc
package ulong XXH64_avalanche(ulong h64)

	do
	{
		h64 ^= h64 >> 33;
		h64 *= .PRIME64_2;
		h64 ^= h64 >> 29;
		h64 *= .PRIME64_3;
		h64 ^= h64 >> 32;

		return h64;
	}

pure nothrow @nogc
package ulong XXH64_finalize(.XXH_alignment XXH_align)(ulong h64, const void* ptr, const size_t len)

	in
	{
		switch (len & 31) {
			case 0: .. case 31:
				break;

			default:
				assert(0);
		}
	}

	do
	{
		static import lz4_lib.lz4common;

		const (ubyte)* p = cast(const (ubyte)*)(ptr);

		pragma(inline, true)
		pure nothrow @nogc
		void PROCESS1_64()

			do
			{
				h64 ^= (*p++) * .PRIME64_5;
				h64 = lz4_lib.lz4common.rotl64(h64, 11) * .PRIME64_1;
			}

		pragma(inline, true)
		pure nothrow @nogc
		void PROCESS4_64()

			do
			{
				h64 ^= cast(ulong)(.readLE32_align!(XXH_align)(p)) * .PRIME64_1;
				p += 4;
				h64 = lz4_lib.lz4common.rotl64(h64, 23) * .PRIME64_2 + .PRIME64_3;
			}

		pragma(inline, true)
		pure nothrow @nogc
		void PROCESS8_64()

			do
			{
				const ulong k1 = .XXH64_round(0, .readLE64_align!(XXH_align)(p));
				p += 8;
				h64 ^= k1;
				h64 = lz4_lib.lz4common.rotl64(h64,27) * .PRIME64_1 + .PRIME64_4;
			}

		switch (len & 31) {
			case 24:
				PROCESS8_64();
				goto case;

			case 16:
				PROCESS8_64();
				goto case;

			case 8:
				PROCESS8_64();

				return .XXH64_avalanche(h64);

			case 28:
				PROCESS8_64();
				goto case;

			case 20:
				PROCESS8_64();
				goto case;

			case 12:
				PROCESS8_64();
				goto case;

			case 4:
				PROCESS4_64();

				return .XXH64_avalanche(h64);

			case 25:
				PROCESS8_64();
				goto case;

			case 17:
				PROCESS8_64();
				goto case;

			case 9:
				PROCESS8_64();
				PROCESS1_64();

				return .XXH64_avalanche(h64);

			case 29:
				PROCESS8_64();
				goto case;

			case 21:
				PROCESS8_64();
				goto case;

			case 13:
				PROCESS8_64();
				goto case;

			case 5:
				PROCESS4_64();
				PROCESS1_64();

				return .XXH64_avalanche(h64);

			case 26:
				PROCESS8_64();
				goto case;

			case 18:
				PROCESS8_64();
				goto case;

			case 10:
				PROCESS8_64();
				PROCESS1_64();
				PROCESS1_64();

				return .XXH64_avalanche(h64);

			case 30:
				PROCESS8_64();
				goto case;

			case 22:
				PROCESS8_64();
				goto case;

			case 14:
				PROCESS8_64();
				goto case;

			case 6:
				PROCESS4_64();
				PROCESS1_64();
				PROCESS1_64();

				return .XXH64_avalanche(h64);

			case 27:
				PROCESS8_64();
				goto case;

			case 19:
				PROCESS8_64();
				goto case;

			case 11:
				PROCESS8_64();
				PROCESS1_64();
				PROCESS1_64();
				PROCESS1_64();

				return .XXH64_avalanche(h64);

			case 31:
				PROCESS8_64();
				goto case;

			case 23:
				PROCESS8_64();
				goto case;

			case 15:
				PROCESS8_64();
				goto case;

			case 7:
				PROCESS4_64();
				goto case;

			case 3:
				PROCESS1_64();
				goto case;

			case 2:
				PROCESS1_64();
				goto case;

			case 1:
				PROCESS1_64();
				goto case;

			case 0:
				return .XXH64_avalanche(h64);

			default:
				/* impossible to reach */
				/* unreachable, but some compilers complain without it */
				return 0;
		}
	}

pragma(inline, true)
pure nothrow @nogc
ulong XXH64_endian_align(.XXH_alignment XXH_align)(const void* input, size_t len, const ulong seed)

	in
	{
		static if (!.XXH_ACCEPT_NULL_INPUT_POINTER) {
			assert(input != null);
		}
	}

	do
	{
		static import lz4_lib.lz4common;

		const (ubyte)* p = cast(const (ubyte)*)(input);
		const ubyte* bEnd = p + len;
		ulong h64;

		static if (.XXH_ACCEPT_NULL_INPUT_POINTER) {
			if (p == null) {
				len = 0;
				bEnd = p = cast(const (ubyte)*)(cast(size_t)(32));
			}
		}

		if (len >= 32) {
			const ubyte* limit = bEnd - 32;
			ulong v1 = seed + .PRIME64_1 + .PRIME64_2;
			ulong v2 = seed + .PRIME64_2;
			ulong v3 = seed + 0;
			ulong v4 = seed - .PRIME64_1;

			do {
				v1 = .XXH64_round(v1, .readLE64_align!(XXH_align)(p));
				p += 8;
				v2 = .XXH64_round(v2, .readLE64_align!(XXH_align)(p));
				p += 8;
				v3 = .XXH64_round(v3, .readLE64_align!(XXH_align)(p));
				p += 8;
				v4 = .XXH64_round(v4, .readLE64_align!(XXH_align)(p));
				p += 8;
			} while (p <= limit);

			h64 = lz4_lib.lz4common.rotl64(v1, 1) + lz4_lib.lz4common.rotl64(v2, 7) + lz4_lib.lz4common.rotl64(v3, 12) + lz4_lib.lz4common.rotl64(v4, 18);
			h64 = .XXH64_mergeRound(h64, v1);
			h64 = .XXH64_mergeRound(h64, v2);
			h64 = .XXH64_mergeRound(h64, v3);
			h64 = .XXH64_mergeRound(h64, v4);
		} else {
			h64 = seed + .PRIME64_5;
		}

		h64 += cast(ulong)(len);

		return .XXH64_finalize!(XXH_align)(h64, p, len);
	}

/**
 * XXH64() :
 *  Calculate the 64-bit hash of sequence of length "len" stored at memory address "input".
 *  "seed" can be used to alter the result predictably.
 *  This function runs faster on 64-bit systems, but slower on 32-bit systems (see benchmark).
 */
//XXH_PUBLIC_API
pragma(inline, true)
pure nothrow @nogc
public ulong XXH64(const void* input, const size_t len, const ulong seed)

	in
	{
		assert(input != null);
	}

	do
	{
		static if (.XXH_FORCE_ALIGN_CHECK) {
			if (((cast(size_t)(input)) & 7) == 0) { /* Input is aligned, let's leverage the speed advantage */
				return .XXH64_endian_align!(.XXH_alignment.aligned)(input, len, seed);
			}
		}

		return .XXH64_endian_align!(.XXH_alignment.unaligned)(input, len, seed);
	}

/*======   Hash Streaming   ======*/

/*======   Canonical representation   ======*/
